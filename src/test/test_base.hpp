#pragma once

#include <ast/rules.hpp>
#include <gtest/gtest.h>
#include <peglib.h>

namespace bpy::testing {
class BythonTestBase : public ::testing::Test {
  public:
	explicit BythonTestBase(ast::RuleName ruleName);

	void SetUp() final;
	void TearDown() final;

	ast::RuleName GetRuleName();

  protected:
	peg::parser* parser = nullptr;
	ast::RuleName rule_name;
};

class BythonProgrammTest : public BythonTestBase {
  public:
	BythonProgrammTest() : BythonTestBase{bpy::ast::RuleName::Program}
	{
	}

  protected:
	auto test_program(const std::string&) -> void;
};
} // namespace bpy::testing
