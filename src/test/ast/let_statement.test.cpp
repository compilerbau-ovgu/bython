#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/statement/let_statement.hpp"

namespace bpy::ast {

class LetStatementASTTest : public testing::BythonTestBase {
  public:
	LetStatementASTTest() : BythonTestBase{RuleName::LetStatement}
	{
	}
};

TEST_F(LetStatementASTTest, CreateNullType)
{
	// integer
	std::shared_ptr<peg::Ast> letstate;
	EXPECT_TRUE(parser->parse("let a = {}", letstate));
	const auto statemen_test = let_statement::create(letstate);

	EXPECT_EQ(statemen_test->rule_name, GetRuleName());
}

TEST_F(LetStatementASTTest, CreateWithType)
{
	// integer
	std::shared_ptr<peg::Ast> letstate;
	EXPECT_TRUE(parser->parse("let a : Int = {}", letstate));
	const auto statemen_test = let_statement::create(letstate);
	EXPECT_TRUE(statemen_test->has_type_hint());

	EXPECT_EQ(statemen_test->rule_name, GetRuleName());
}

TEST_F(LetStatementASTTest, GetterVarName)
{
	std::shared_ptr<peg::Ast> getter_var_name;
	EXPECT_TRUE(parser->parse("let leer = {}", getter_var_name));
	const auto statement_getter =
		let_statement::create(getter_var_name)->get_var_name();
	EXPECT_STREQ(statement_getter.c_str(), "leer");
}

TEST_F(LetStatementASTTest, GetterTypeNameTrue)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(parser->parse("let a : Int = {}", getter_type_name));
	const auto statement_getter =
		let_statement::create(getter_type_name)->get_type_name();
	EXPECT_STREQ(statement_getter.c_str(), "Int");
}

TEST_F(LetStatementASTTest, GetterTypeNameFalse)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(parser->parse("let a = {}", getter_type_name));
	const auto statement_getter =
		let_statement::create(getter_type_name)->get_type_name();
	EXPECT_STREQ(statement_getter.c_str(), "");
}

// false Test

TEST_F(LetStatementASTTest, FalseTestMissLet1)
{
	std::shared_ptr<peg::Ast> missing_let;
	EXPECT_FALSE(parser->parse("a : Int = {}", missing_let));
}

TEST_F(LetStatementASTTest, FalseTestMissLet2)
{
	std::shared_ptr<peg::Ast> missing_let;
	EXPECT_FALSE(parser->parse("a = {}", missing_let));
}

TEST_F(LetStatementASTTest, FalseTestDoubleColon1)
{
	std::shared_ptr<peg::Ast> double_colon;
	EXPECT_FALSE(parser->parse("let a :: Int = {}", double_colon));
}

TEST_F(LetStatementASTTest, FalseTestDoubleColon2)
{
	std::shared_ptr<peg::Ast> double_colon;
	EXPECT_FALSE(parser->parse("let a :: = {}", double_colon));
}

TEST_F(LetStatementASTTest, FalseTestMissExpression)
{
	std::shared_ptr<peg::Ast> missing_espression;
	EXPECT_FALSE(parser->parse("let a : Int = ", missing_espression));
}

TEST_F(LetStatementASTTest, FalseTestMissExpression2)
{
	std::shared_ptr<peg::Ast> missing_expression;
	EXPECT_FALSE(parser->parse("let a = ", missing_expression));
}

TEST_F(LetStatementASTTest, FalseTestMissType)
{
	std::shared_ptr<peg::Ast> missing_type;
	EXPECT_FALSE(parser->parse("let a :  = {}", missing_type));
}

TEST_F(LetStatementASTTest, FalseTestMissVariableName)
{
	std::shared_ptr<peg::Ast> missing_vn;
	EXPECT_FALSE(parser->parse("let : Int = {}", missing_vn));
}

} // namespace bpy::ast
