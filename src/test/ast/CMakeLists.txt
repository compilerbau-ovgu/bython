set(BYTHON_TEST_AST_SOURCE_FILES

    ${CMAKE_CURRENT_SOURCE_DIR}/integer.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/constructor.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/function_name.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/floating_point.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/numeric.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/string.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/identifier.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/type_name.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/variable_name.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/constant.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/return_type.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/boolean_constant.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/block_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/statement.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/callargs.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/lambda_expression.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/else_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/if_expression.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/func_decl_arg.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/func_decl_args.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/function.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/none_arithmetic_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/call_expression.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/comparison.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/none_boolean_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/first_class_arithmetic_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/arithmetic_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/first_class_boolean_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/boolean_expression.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/record_member.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/record.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/if_statement.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/let_statement.test.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/else_statement.test.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/program.test.cpp
    PARENT_SCOPE
)
