#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/block_expression.hpp"
#include "ast/expression/expression.hpp"

namespace bpy::ast {

class ExpressionASTTest : public testing::BythonTestBase {
  public:
	ExpressionASTTest() : BythonTestBase{RuleName::Expression}
	{
	}
};

/*TEST_F(ExpressionASTTest, CreateLambdaExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("Void -> {}", is_identifier));
	const auto expression = expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<lambda_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}*/

TEST_F(ExpressionASTTest, CreateBlockExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("{}", is_identifier));
	const auto expression = expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<block_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ExpressionASTTest, CreateArithmeticExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("a+a*b", is_identifier));
	const auto expression = expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<arithmetic_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
// Boolean Expression does not match in Expression
TEST_F(ExpressionASTTest, CreateBooleanExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("true || false", is_identifier));
	const auto expression = expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<boolean_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ExpressionASTTest, CreateStringConstant)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("'Hello World!'", is_identifier));
	const auto expression = expression::create(is_identifier);
	EXPECT_NO_THROW(std::get<std::shared_ptr<bstring>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
} // namespace bpy::ast
