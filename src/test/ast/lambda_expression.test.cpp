#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/lambda_expression.hpp"

namespace bpy::ast {

class LambdaExpressionASTTest : public testing::BythonTestBase {
  public:
	LambdaExpressionASTTest() : BythonTestBase{RuleName::LambdaExpression}
	{
	}
};

TEST_F(LambdaExpressionASTTest, CreateNullType)
{
	// integer
	std::shared_ptr<peg::Ast> lambda;
	EXPECT_TRUE(parser->parse("Void -> {}", lambda));
	const auto lambda_expression_test = lambda_expression::create(lambda);

	EXPECT_EQ(lambda_expression_test->rule_name, GetRuleName());
}

TEST_F(LambdaExpressionASTTest, CreateWithType)
{
	// integer
	std::shared_ptr<peg::Ast> lambda;
	EXPECT_TRUE(parser->parse("Void -> Int {}", lambda));
	const auto lambda_expression_test = lambda_expression::create(lambda);
	EXPECT_TRUE(lambda_expression_test->has_type());

	EXPECT_EQ(lambda_expression_test->rule_name, GetRuleName());
}

TEST_F(LambdaExpressionASTTest, GetterReturnTypeTrue)
{
	std::shared_ptr<peg::Ast> getter_ret_type;
	EXPECT_TRUE(parser->parse("Void -> Int {}", getter_ret_type));
	const auto lambda_expr = lambda_expression::create(getter_ret_type);
	EXPECT_TRUE(lambda_expr->has_type());

	const auto lambda_getter = lambda_expr->get_type();
	EXPECT_STREQ(lambda_getter.c_str(), "Int");
}

TEST_F(LambdaExpressionASTTest, GetterReturnTypeFalse)
{
	std::shared_ptr<peg::Ast> getter_ret_type;
	EXPECT_TRUE(parser->parse("Void -> {}", getter_ret_type));
	const auto lambda_expr = lambda_expression::create(getter_ret_type);
	EXPECT_FALSE(lambda_expr->has_type());
}
// false Test

TEST_F(LambdaExpressionASTTest, FalseTestMissArrow1)
{
	std::shared_ptr<peg::Ast> miss_arrow;
	EXPECT_FALSE(parser->parse("Void {}", miss_arrow));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissArrow2)
{
	std::shared_ptr<peg::Ast> miss_arrow;
	EXPECT_FALSE(parser->parse("Void Int {}", miss_arrow));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissArrow3)
{
	std::shared_ptr<peg::Ast> miss_arrow;
	EXPECT_FALSE(parser->parse("A : Int, B : Float {}", miss_arrow));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissArrow4)
{
	std::shared_ptr<peg::Ast> miss_arrow;
	EXPECT_FALSE(parser->parse("A : Int, B : Float Int {}", miss_arrow));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissFuncDeclArgs1)
{
	std::shared_ptr<peg::Ast> miss_fda;
	EXPECT_FALSE(parser->parse("-> Int {}", miss_fda));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissFuncDeclArgs2)
{
	std::shared_ptr<peg::Ast> miss_fda;
	EXPECT_FALSE(parser->parse("-> {}", miss_fda));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissBLock1)
{
	std::shared_ptr<peg::Ast> miss_fda;
	EXPECT_FALSE(parser->parse("Void -> Int ", miss_fda));
}

TEST_F(LambdaExpressionASTTest, FalseTestMissBlock2)
{
	std::shared_ptr<peg::Ast> miss_fda;
	EXPECT_FALSE(parser->parse("A : Int, B : Float Int -> ", miss_fda));
}

TEST_F(LambdaExpressionASTTest, FalseTestWrongRT)
{
	std::shared_ptr<peg::Ast> wrong_rt;
	EXPECT_FALSE((parser->parse("Void -> 5ummand {}", wrong_rt)));
}

} // namespace bpy::ast
