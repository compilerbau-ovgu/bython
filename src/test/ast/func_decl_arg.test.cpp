#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/func_decl_arg.hpp"

namespace bpy::ast {

class FuncDeclArgASTTest : public testing::BythonTestBase {
  public:
	FuncDeclArgASTTest() : BythonTestBase{RuleName::FuncDeclArg}
	{
	}
};

TEST_F(FuncDeclArgASTTest, Create)
{
	// signless
	std::shared_ptr<peg::Ast> arg;
	EXPECT_TRUE(parser->parse("phonebook: HashMap[Key, Value]", arg));
	const auto arg_ast = func_decl_arg::create(arg);

	EXPECT_STREQ(arg_ast->get_var_name().c_str(), "phonebook");
	EXPECT_STREQ(arg_ast->get_type()->get_name().c_str(), "HashMap");

	// funktionier get_type_name hier: wegen subtypes
	EXPECT_STREQ(arg_ast->get_type()->get_subtype(0)->get_name().c_str(),
				 "Key");
	EXPECT_STREQ(arg_ast->get_type()->get_subtype(1)->get_name().c_str(),
				 "Value");

	EXPECT_EQ(arg_ast->rule_name, GetRuleName());
}

TEST_F(FuncDeclArgASTTest, GetterVarName)
{
	std::shared_ptr<peg::Ast> getter_var_name;
	EXPECT_TRUE(
		parser->parse("phonebook: HashMap[Key, Value]", getter_var_name));
	const auto func_getter =
		func_decl_arg::create(getter_var_name)->get_var_name();
	EXPECT_STREQ(func_getter.c_str(), "phonebook");
}

TEST_F(FuncDeclArgASTTest, GetterTypeName)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(
		parser->parse("phonebook: HashMap[Key, Value]", getter_type_name));
	const auto func_getter =
		func_decl_arg::create(getter_type_name)->get_type();
	EXPECT_STREQ(func_getter->get_name().c_str(), "HashMap");
}

// false Test

TEST_F(FuncDeclArgASTTest, FalseTestMissColon)
{
	std::shared_ptr<peg::Ast> miss_colon;
	EXPECT_FALSE(parser->parse("Human HashMap[Key, Value]", miss_colon));
}

TEST_F(FuncDeclArgASTTest, FalseTestDoubleColon)
{
	std::shared_ptr<peg::Ast> double_colon;
	EXPECT_FALSE(parser->parse("Human:: HashMap[Key, Value]", double_colon));
}

TEST_F(FuncDeclArgASTTest, FalseTestMissVN)
{
	std::shared_ptr<peg::Ast> miss_vn;
	EXPECT_FALSE(parser->parse(": HashMap[Key, Value]", miss_vn));
}

TEST_F(FuncDeclArgASTTest, FalseTestDoubleVN)
{
	std::shared_ptr<peg::Ast> double_vn;
	EXPECT_FALSE(parser->parse("Human Mensch: HashMap[Key, Value]", double_vn));
}

TEST_F(FuncDeclArgASTTest, FalseTestMissTN)
{
	std::shared_ptr<peg::Ast> miss_tn;
	EXPECT_FALSE(parser->parse("Human: ", miss_tn));
}

TEST_F(FuncDeclArgASTTest, FalseTestDoubleTN)
{
	std::shared_ptr<peg::Ast> double_tn;
	EXPECT_FALSE(parser->parse("Human: HashMap[Key, Value] HashMap[Key, Value]",
							   double_tn));
}

} // namespace bpy::ast