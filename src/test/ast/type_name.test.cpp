#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/type_name.hpp"

namespace bpy::ast {

class TypeNameASTTest : public testing::BythonTestBase {
  public:
	TypeNameASTTest() : BythonTestBase{RuleName::TypeName}
	{
	}
};

template <int N>
static auto depends_on_all_names(const type_name::instance& tn,
								 const std::string (&names)[N]) -> void
{
	const auto tn_deps = tn->dependencies();
	ASSERT_FALSE(tn_deps.empty());

	ASSERT_EQ(std::size(tn->get_subtypes()), std::size(tn_deps));

	for (std::size_t i = 0; i < N; ++i) {
		// Match subtype
		const auto& subtype = tn->get_subtype(i);
		EXPECT_STREQ(subtype->get_name().c_str(), names[i].c_str());

		// Make sure subtype is empty
		EXPECT_FALSE(subtype->has_subtypes());

		const auto locate_it = std::find_if(
			std::begin(tn_deps),
			std::end(tn_deps),
			[&](const auto& dependency) {
				// Can only be a type_name
				const auto tn =
					std::dynamic_pointer_cast<type_name>(dependency);
				return tn->get_name() == names[i];
			});

		// Make sure subtype is a dependency
		EXPECT_NE(locate_it, std::end(tn_deps));
	}
}

TEST_F(TypeNameASTTest, Create)
{
	std::shared_ptr<peg::Ast> simple;
	EXPECT_TRUE(parser->parse("PeRsOn", simple));
	const auto simple_typename = type_name::create(simple);

	EXPECT_STREQ(simple_typename->get_name().c_str(), "PeRsOn");
	// has no dependencies
	ASSERT_TRUE(simple_typename->dependencies().empty());

	// Subtype depth of 1
	std::shared_ptr<peg::Ast> usedIdentifier;
	EXPECT_TRUE(parser->parse("BytH1[BytHon2, Bython3]", usedIdentifier));
	const auto subtypes_used_identifier = type_name::create(usedIdentifier);

	// Match outermost type
	EXPECT_STREQ(subtypes_used_identifier->get_name().c_str(), "BytH1");

	// Match inner types
	const std::string subtypes_used_identifier_dep_names[] = {"BytHon2",
															  "Bython3"};
	depends_on_all_names(subtypes_used_identifier,
						 subtypes_used_identifier_dep_names);

	// Subtype depth of 2
	std::shared_ptr<peg::Ast> list_in_list;
	EXPECT_TRUE(parser->parse("BytH7[BytHon8, Bython9[BytH10]]", list_in_list));
	const auto list_in_list_tn = type_name::create(list_in_list);

	EXPECT_STREQ(list_in_list_tn->get_name().c_str(), "BytH7");

	// First subtype has only a name, no subtypes of its own
	const auto& first_subtype = list_in_list_tn->get_subtype(0);
	EXPECT_STREQ(first_subtype->get_name().c_str(), "BytHon8");
	EXPECT_FALSE(first_subtype->has_subtypes());

	// Second subtype has a name, and one subtype of its own
	const auto& second_subtype = list_in_list_tn->get_subtype(1);
	EXPECT_STREQ(second_subtype->get_name().c_str(), "Bython9");

	const std::string DNS[] = {"BytH10"};
	depends_on_all_names(second_subtype, DNS);

	EXPECT_STREQ(second_subtype->get_subtype(0)->get_name().c_str(), "BytH10");
	// No dependencies
	EXPECT_TRUE(second_subtype->get_subtype(0)->dependencies().empty());

	EXPECT_EQ(list_in_list_tn->rule_name, GetRuleName());

	// false tests
	std::shared_ptr<peg::Ast> noIdentifier;
	EXPECT_FALSE(parser->parse("[BytHon8, Bython9]", noIdentifier));

	std::shared_ptr<peg::Ast> noComma;
	EXPECT_FALSE(parser->parse("BytH7[BytHon8; Bython9]", noComma));

	std::shared_ptr<peg::Ast> noSquareBrackets;
	EXPECT_FALSE(parser->parse("BytH7(BytHon8, Bython9)", noSquareBrackets));
}

// false Test

TEST_F(TypeNameASTTest, FalseTestMissingIdentifier1)
{
	std::shared_ptr<peg::Ast> miss_ident;
	EXPECT_FALSE(parser->parse("[Int, Float]", miss_ident));
}

TEST_F(TypeNameASTTest, FalseTestMissingIdentifier2)
{
	std::shared_ptr<peg::Ast> miss_ident;
	EXPECT_FALSE(parser->parse("Int[, Float]", miss_ident));
}

TEST_F(TypeNameASTTest, FalseTestMissingIdentifier3)
{
	std::shared_ptr<peg::Ast> miss_ident;
	EXPECT_FALSE(parser->parse("Int[Float, ]", miss_ident));
}

TEST_F(TypeNameASTTest, FalseTestNoComma1)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("Int[Int. Float]", no_comma));
}

TEST_F(TypeNameASTTest, FalseTestNoComma2)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("Int[Int; Float]", no_comma));
}

TEST_F(TypeNameASTTest, FalseTestNoComma3)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("Int[Int: Float]", no_comma));
}

TEST_F(TypeNameASTTest, FalseTestWrongBrackets1)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("Int(Int, Float)", wrong_brackets));
}

TEST_F(TypeNameASTTest, FalseTestWrongBrackets2)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("Int{Int, Float}", wrong_brackets));
}

} // namespace bpy::ast
