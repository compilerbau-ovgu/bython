#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/return_type.hpp"

namespace bpy::ast {

class ReturnTypeASTTest : public testing::BythonTestBase {
  public:
	ReturnTypeASTTest() : BythonTestBase{RuleName::ReturnType}
	{
	}
};

TEST_F(ReturnTypeASTTest, Create)
{
	std::shared_ptr<peg::Ast> is_type;
	EXPECT_TRUE(parser->parse("String[Int, Int]", is_type));
	const auto rt_is_type = return_type::create(is_type);
	EXPECT_STREQ(rt_is_type->get_name().c_str(), "String");

	EXPECT_EQ(rt_is_type->rule_name, GetRuleName());
}

TEST_F(ReturnTypeASTTest, GetterGetName)
{
	std::shared_ptr<peg::Ast> get_name;
	EXPECT_TRUE(parser->parse("String[Int, Int]", get_name));
	const auto rt_get_name = return_type::create(get_name)->get_name();
	EXPECT_STREQ(rt_get_name.c_str(), "String");
}
} // namespace bpy::ast
