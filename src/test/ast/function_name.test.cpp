#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/function_name.hpp"

namespace bpy::ast {

class FunctionNameASTTest : public testing::BythonTestBase {
  public:
	FunctionNameASTTest() : BythonTestBase{RuleName::FunctionName}
	{
	}
};

TEST_F(FunctionNameASTTest, Create)
{
	std::shared_ptr<peg::Ast> isFunctionName;
	EXPECT_TRUE(parser->parse("bytHon7e", isFunctionName));
	const auto name_isFunctionName = function_name::create(isFunctionName);
	EXPECT_STREQ(name_isFunctionName->get_name().c_str(), "bytHon7e");

	EXPECT_EQ(name_isFunctionName->rule_name, GetRuleName());

	// false tests
	std::shared_ptr<peg::Ast> noIdentifier;
	EXPECT_FALSE(parser->parse("78Bython", isFunctionName));
}

TEST_F(FunctionNameASTTest, FalseTestNoFunctionName)
{
	std::shared_ptr<peg::Ast> no_function_name;
	EXPECT_FALSE(parser->parse("1st_spot", no_function_name));
}

} // namespace bpy::ast
