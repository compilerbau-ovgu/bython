#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/call_expression.hpp"

namespace bpy::ast {

class CallExpressionASTTest : public testing::BythonTestBase {
  public:
	CallExpressionASTTest() : BythonTestBase(RuleName::CallExpression)
	{
	}
};

TEST_F(CallExpressionASTTest, CreateConstructor)
{
	std::shared_ptr<peg::Ast> isCon;
	EXPECT_TRUE(parser->parse("String::new ()", isCon));
	const auto call_name_con = ast::call_expression::create(isCon);
	EXPECT_NO_THROW(
		std::get<ast::constructor::instance>(call_name_con->get_callee_var()));

	EXPECT_EQ(call_name_con->rule_name, GetRuleName());
}

TEST_F(CallExpressionASTTest, CreateFunctionName)
{
	std::shared_ptr<peg::Ast> isFN;
	EXPECT_TRUE(parser->parse("Compute1 ()", isFN));
	const auto call_name_FN = call_expression::create(isFN);
	EXPECT_NO_THROW(
		std::get<ast::function_name::instance>(call_name_FN->get_callee_var()));
	EXPECT_EQ(call_name_FN->rule_name, GetRuleName());
}

TEST_F(CallExpressionASTTest, CreateGetterConstructor)
{
	std::shared_ptr<peg::Ast> getter_constructor;
	EXPECT_TRUE(parser->parse("String::new ()", getter_constructor));
	const auto call_expr_getter =
		call_expression::create(getter_constructor)->get_callee_name();
	EXPECT_STREQ(call_expr_getter.c_str(), "String");
}

TEST_F(CallExpressionASTTest, GetterFunctionName)
{
	std::shared_ptr<peg::Ast> getter_function_name;
	EXPECT_TRUE(parser->parse("RandomGen ()", getter_function_name));
	const auto call_expr_getter =
		call_expression::create(getter_function_name)->get_callee_name();
	EXPECT_STREQ(call_expr_getter.c_str(), "RandomGen");
}

// false Tests

TEST_F(CallExpressionASTTest, FalseTestNoConstOrFN)
{
	std::shared_ptr<peg::Ast> miss_const_or_FN;
	EXPECT_FALSE(parser->parse("()", miss_const_or_FN));
}

TEST_F(CallExpressionASTTest, FalseTestWrongBrackets1)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("String::new {}", wrong_brackets));
}

TEST_F(CallExpressionASTTest, FalseTestWrongBrackets2)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("Compute []", wrong_brackets));
}

TEST_F(CallExpressionASTTest, FalseTestMoreThanOneCallArgs)
{
	std::shared_ptr<peg::Ast> two_callargs;
	EXPECT_FALSE((parser->parse("String::new ( {},{} {},{})", two_callargs)));
}

TEST_F(CallExpressionASTTest, FalseTestConstAndFN)
{
	std::shared_ptr<peg::Ast> both;
	EXPECT_FALSE(parser->parse("String::new Compute ()", both));
}

} // namespace bpy::ast