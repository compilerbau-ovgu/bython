#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/func_decl_args.hpp"

namespace bpy::ast {

class FuncDeclArgsASTTest : public testing::BythonTestBase {
  public:
	FuncDeclArgsASTTest() : BythonTestBase{RuleName::FuncDeclArgs}
	{
	}
};

TEST_F(FuncDeclArgsASTTest, CreateVoid)
{
	std::shared_ptr<peg::Ast> void_;
	EXPECT_TRUE(parser->parse("Void", void_));
	const auto void_args_ast = func_decl_args::create(void_);

	EXPECT_EQ(void_args_ast->args, std::nullopt);
	EXPECT_TRUE(void_args_ast->dependencies().empty());

	EXPECT_EQ(void_args_ast->rule_name, GetRuleName());
}

TEST_F(FuncDeclArgsASTTest, CreateParameterList)
{
	std::shared_ptr<peg::Ast> args;
	EXPECT_TRUE(parser->parse(
		"argOne: TypeOne, argTwo: TypeTwo[String, String]", args));
	const auto non_void_ast = ast::func_decl_args::create(args);

	EXPECT_NE(non_void_ast->args, std::nullopt);
	const auto nonopt_args = *non_void_ast->args;

	EXPECT_FALSE(nonopt_args.empty());

	const auto deps = non_void_ast->dependencies();
	EXPECT_EQ(std::size(deps), 4);

	auto names = std::vector<std::string>{};
	std::transform(std::begin(deps), std::end(deps), std::back_inserter(names),
				   [](const auto& dep) {
					   const auto dep_as_typename =
						   std::dynamic_pointer_cast<ast::type_name>(dep);
					   return dep_as_typename->get_name();
				   });

	EXPECT_NE(std::find(std::begin(names), std::end(names), "TypeOne"),
			  std::end(names));
	EXPECT_NE(std::find(std::begin(names), std::end(names), "TypeTwo"),
			  std::end(names));
}

// false Test

TEST_F(FuncDeclArgsASTTest, FalseTestNoComma1)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse(
		"argOne: TypeOne argTwo: TypeTwo[String, String]", no_comma));
}

TEST_F(FuncDeclArgsASTTest, FalseTestNoComma2)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse(
		"argOne: TypeOne; argTwo: TypeTwo[String, String]", no_comma));
}

TEST_F(FuncDeclArgsASTTest, FalseTestNoComma3)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse(
		"argOne: TypeOne. argTwo: TypeTwo[String, String]", no_comma));
}
} // namespace bpy::ast
