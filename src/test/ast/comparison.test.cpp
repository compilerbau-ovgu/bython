#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/comparison.hpp"

namespace bpy::ast {

class ComparisonASTTest : public testing::BythonTestBase {
  public:
	ComparisonASTTest() : BythonTestBase{RuleName::Comparison}
	{
	}
};

TEST_F(ComparisonASTTest, CreateNonEqual)
{
	// NEQ
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a != b", _ast));
	const auto ab_equal = comparison::create(_ast);

	EXPECT_EQ(ab_equal->rule_name, GetRuleName());
}

TEST_F(ComparisonASTTest, CreateEqual)
{
	// EQ
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a == b", _ast));
	const auto ab_equal = comparison::create(_ast);

	EXPECT_EQ(ab_equal->rule_name, GetRuleName());
}

TEST_F(ComparisonASTTest, CreateGreaterEqual)
{
	// GEQ
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a >= b", _ast));
	const auto ab_geq = comparison::create(_ast);

	EXPECT_EQ(ab_geq->rule_name, GetRuleName());
}

TEST_F(ComparisonASTTest, CreateLesserEqual)
{
	// LEQ
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a <= b", _ast));
	const auto ab_leq = comparison::create(_ast);

	EXPECT_EQ(ab_leq->rule_name, GetRuleName());
}

TEST_F(ComparisonASTTest, CreateGreater)
{
	// GRT
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a > b", _ast));
	const auto ab_grt = comparison::create(_ast);

	EXPECT_EQ(ab_grt->rule_name, GetRuleName());
}

TEST_F(ComparisonASTTest, CreateLesser)
{
	// LSR
	std::shared_ptr<peg::Ast> _ast;
	EXPECT_TRUE(parser->parse("a < b", _ast));
	const auto ab_lsr = comparison::create(_ast);

	EXPECT_EQ(ab_lsr->rule_name, GetRuleName());
}

} // namespace bpy::ast
