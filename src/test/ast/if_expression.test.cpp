#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/if_expression.hpp"

namespace bpy::ast {

class IfExpressionASTTest : public testing::BythonTestBase {
  public:
	IfExpressionASTTest() : BythonTestBase{RuleName::IfExpression}
	{
	}
};

TEST_F(IfExpressionASTTest, Create)
{
	std::shared_ptr<peg::Ast> if_expression_ast;
	EXPECT_TRUE(parser->parse("if true {} else {}", if_expression_ast));
	const auto if_expression_test = if_expression::create(if_expression_ast);

	EXPECT_EQ(if_expression_test->rule_name, GetRuleName());
}

TEST_F(IfExpressionASTTest, CreateElseIf)
{
	std::shared_ptr<peg::Ast> if_expression_ast;
	EXPECT_TRUE(parser->parse("if true {} else if false {} else {}",
							  if_expression_ast));
	const auto if_expression_test = if_expression::create(if_expression_ast);

	EXPECT_EQ(if_expression_test->rule_name, GetRuleName());
}

// false Test

TEST_F(IfExpressionASTTest, FalseTestMissIf1)
{
	std::shared_ptr<peg::Ast> miss_if;
	EXPECT_FALSE(parser->parse("true {}", miss_if));
}

TEST_F(IfExpressionASTTest, FalseTestMissIf2)
{
	std::shared_ptr<peg::Ast> miss_if;
	EXPECT_FALSE(parser->parse("if true {} else false {}", miss_if));
}

TEST_F(IfExpressionASTTest, FalseTestMissBool1)
{
	std::shared_ptr<peg::Ast> miss_bool;
	EXPECT_FALSE(parser->parse("if {} else if false {}", miss_bool));
}

TEST_F(IfExpressionASTTest, FalseTestMissBool2)
{
	std::shared_ptr<peg::Ast> miss_bool;
	EXPECT_FALSE(parser->parse("if true {} else if {}", miss_bool));
}

TEST_F(IfExpressionASTTest, FalseTestMissBlock)
{
	std::shared_ptr<peg::Ast> miss_block;
	EXPECT_FALSE(parser->parse("if true else if false {}", miss_block));
}

TEST_F(IfExpressionASTTest, FalseTestMissWhitespace1)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("iftrue {} else if false {}", whitespace));
}

TEST_F(IfExpressionASTTest, FalseTestMissWhitespace2)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("if true {} else iffalse {}", whitespace));
}

TEST_F(IfExpressionASTTest, FalseTestMissElse1)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("if true {} ", miss_else));
}

TEST_F(IfExpressionASTTest, FalseTestMissElse2)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("if true {} if false {}", miss_else));
}

} // namespace bpy::ast
