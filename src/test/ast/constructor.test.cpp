#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/constructor.hpp"

namespace bpy::ast {

class ConstructorASTTest : public testing::BythonTestBase {
  public:
	ConstructorASTTest() : BythonTestBase{RuleName::Constructor}
	{
	}
};

TEST_F(ConstructorASTTest, Create)
{
	std::shared_ptr<peg::Ast> constructor_type;
	EXPECT_TRUE(parser->parse("String::new", constructor_type));
	const auto constructor = constructor::create(constructor_type);
	EXPECT_STREQ(constructor->get_name().c_str(), "String");
	EXPECT_TRUE(constructor->rule_name == GetRuleName());
}

TEST_F(ConstructorASTTest, GetterTypeName)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(parser->parse("String::new", getter_type_name));
	const auto contruct_getter =
		constructor::create(getter_type_name)->get_name();
	EXPECT_STREQ(contruct_getter.c_str(), "String");
}

// false Test

TEST_F(ConstructorASTTest, FalseTestMissTN)
{
	std::shared_ptr<peg::Ast> miss_tn;
	EXPECT_FALSE(parser->parse(":: new", miss_tn));
}

TEST_F(ConstructorASTTest, FalseTestSingleColon)
{
	std::shared_ptr<peg::Ast> single_colon;
	EXPECT_FALSE(parser->parse("String:new", single_colon));
}

TEST_F(ConstructorASTTest, FalseTestMissDoubleColon)
{
	std::shared_ptr<peg::Ast> miss_colon;
	EXPECT_FALSE(parser->parse("String new", miss_colon));
}

TEST_F(ConstructorASTTest, FalseTestToManyColons)
{
	std::shared_ptr<peg::Ast> many_colons;
	EXPECT_FALSE(parser->parse("String:::new", many_colons));
}

TEST_F(ConstructorASTTest, FalseTestMissNew)
{
	std::shared_ptr<peg::Ast> miss_new;
	EXPECT_FALSE(parser->parse("String:: ", miss_new));
}

TEST_F(ConstructorASTTest, FalseTestCapitalN)
{
	std::shared_ptr<peg::Ast> spell_mistake;
	EXPECT_FALSE(parser->parse("String:: New", spell_mistake));
}

} // namespace bpy::ast