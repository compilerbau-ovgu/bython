#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/variable_name.hpp"

namespace bpy::ast {

class VariableNameASTTest : public testing::BythonTestBase {
  public:
	VariableNameASTTest() : BythonTestBase{RuleName::VariableName}
	{
	}
};

TEST_F(VariableNameASTTest, Create)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("bytHon7e", isIdentifier));
	const auto name_isIdentifier = variable_name::create(isIdentifier);
	EXPECT_STREQ(name_isIdentifier->get_name().c_str(), "bytHon7e");

	EXPECT_EQ(name_isIdentifier->rule_name, GetRuleName());

	// empty dependencies
	const auto deps = name_isIdentifier->dependencies();
	EXPECT_TRUE(deps.empty());

	// false tests
	std::shared_ptr<peg::Ast> noIdentifier;
	EXPECT_FALSE(parser->parse("78Bython", isIdentifier));
}

TEST_F(VariableNameASTTest, GetName)
{
	std::shared_ptr<peg::Ast> get_name;
	EXPECT_TRUE(parser->parse("CountX_2Times", get_name));
	const auto name_get_name = variable_name::create(get_name)->get_name();
	EXPECT_STREQ(name_get_name.c_str(), "CountX_2Times");
}

// false Tests

TEST_F(VariableNameASTTest, FalseTestNoIdentifier)
{
	std::shared_ptr<peg::Ast> noIdentifier;
	EXPECT_FALSE(parser->parse("2Times", noIdentifier));
}

} // namespace bpy::ast
