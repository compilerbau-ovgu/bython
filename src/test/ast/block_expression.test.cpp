#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/block_expression.hpp"

namespace bpy::ast {

class BlockExpressionASTTest : public testing::BythonTestBase {
  public:
	BlockExpressionASTTest() : BythonTestBase{RuleName::BlockExpression}
	{
	}
};

TEST_F(BlockExpressionASTTest, CreateEmpty)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("{}", isIdentifier));
	const auto block_expression = block_expression::create(isIdentifier);
	EXPECT_EQ(block_expression->statements.size(), 0);

	EXPECT_EQ(block_expression->rule_name, GetRuleName());
}

TEST_F(BlockExpressionASTTest, CreateStatement)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("{let a = {};}", isIdentifier));
	const auto block_expression = block_expression::create(isIdentifier);
	EXPECT_EQ(block_expression->statements.size(), 1);

	EXPECT_EQ(block_expression->rule_name, GetRuleName());
}

TEST_F(BlockExpressionASTTest, CreateExpression)
{
	// is an Identifier
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("{{}}", isIdentifier));
	const auto block_expression = block_expression::create(isIdentifier);
	EXPECT_TRUE(block_expression->value_expression);

	EXPECT_EQ(block_expression->rule_name, GetRuleName());
}

// false tests

TEST_F(BlockExpressionASTTest, FalseTestNoBrackets1)
{
	std::shared_ptr<peg::Ast> no_brackets;
	EXPECT_FALSE(parser->parse("let a = {};", no_brackets));
}

TEST_F(BlockExpressionASTTest, FalseTestWrongBrackets1)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("[let a = {};]", wrong_brackets));
}

TEST_F(BlockExpressionASTTest, FalseTestWrongBrackets2)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("[{}]", wrong_brackets));
}

TEST_F(BlockExpressionASTTest, FalseTestWrongBrackets3)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("(let a = {};)", wrong_brackets));
}

TEST_F(BlockExpressionASTTest, FalseTestWrongBrackets4)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(parser->parse("({})", wrong_brackets));
}

TEST_F(BlockExpressionASTTest, FalseTestMoreThanOneExpr)
{
	std::shared_ptr<peg::Ast> two_expr;
	EXPECT_FALSE(parser->parse("{{}{}}", two_expr));
}

} // namespace bpy::ast
