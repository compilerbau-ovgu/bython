#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/callargs.hpp"

namespace bpy::ast {

class CallArgsASTTest : public testing::BythonTestBase {
  public:
	CallArgsASTTest() : BythonTestBase{RuleName::CallArgs}
	{
	}
};

TEST_F(CallArgsASTTest, Create)
{
	{
		std::shared_ptr<peg::Ast> isIdentifier;
		EXPECT_TRUE(parser->parse("{}", isIdentifier));
		const auto callargs = callargs::create(isIdentifier);
		EXPECT_EQ(callargs->members.size(), 1);

		EXPECT_EQ(callargs->rule_name, GetRuleName());
	}

	{
		std::shared_ptr<peg::Ast> isIdentifier;
		EXPECT_TRUE(parser->parse("{},{}", isIdentifier));
		const auto callargs = callargs::create(isIdentifier);
		EXPECT_EQ(callargs->members.size(), 2);

		EXPECT_EQ(callargs->rule_name, GetRuleName());
	}
}

// false Tests

TEST_F(CallArgsASTTest, FalseTestNoComma1)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("{} {}", no_comma));
}

TEST_F(CallArgsASTTest, FalseTestFalseComma1)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("{};{}", no_comma));
}

TEST_F(CallArgsASTTest, FalseTestFalseComma2)
{
	std::shared_ptr<peg::Ast> no_comma;
	EXPECT_FALSE(parser->parse("{}.{}", no_comma));
}

} // namespace bpy::ast
