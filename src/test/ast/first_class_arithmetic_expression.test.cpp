#include <fstream>
#include <gtest/gtest.h>
#include <peglib.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/first_class_arithmetic_expression.hpp"

namespace bpy::ast {

class FirstClassArithmeticExpressionASTTest : public testing::BythonTestBase {
  public:
	FirstClassArithmeticExpressionASTTest()
		: BythonTestBase{RuleName::FirstClassArithmeticExpression}
	{
	}
};

TEST_F(FirstClassArithmeticExpressionASTTest, CreateContextual)
{
	std::shared_ptr<peg::Ast> contextual;
	EXPECT_TRUE(parser->parse("a", contextual));
	const auto expression =
		first_class_arithmetic_expression::create(contextual);
	EXPECT_FALSE((bool) expression->binary_operator);

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(FirstClassArithmeticExpressionASTTest, CreateMultiplication)
{
	// is an Identifier
	std::shared_ptr<peg::Ast> mult;
	EXPECT_TRUE(parser->parse("a*b", mult));
	const auto expression = first_class_arithmetic_expression::create(mult);
	EXPECT_TRUE((bool) expression->binary_operator);
	EXPECT_EQ("*", expression->binary_operator.value().first);

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(FirstClassArithmeticExpressionASTTest, CreateDivision)
{
	// is an Identifier
	std::shared_ptr<peg::Ast> division;
	EXPECT_TRUE(parser->parse("a/b", division));
	const auto expression = first_class_arithmetic_expression::create(division);
	EXPECT_TRUE((bool) expression->binary_operator);
	EXPECT_EQ("/", expression->binary_operator.value().first);

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
} // namespace bpy::ast
