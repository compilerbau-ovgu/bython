#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/integer.hpp"

namespace bpy::ast {

class IntegerASTTest : public testing::BythonTestBase {
  public:
	IntegerASTTest() : BythonTestBase{RuleName::IntegerConstant}
	{
	}
};

TEST_F(IntegerASTTest, Create)
{
	// signless
	std::shared_ptr<peg::Ast> signless;
	EXPECT_TRUE(parser->parse("123", signless));
	const auto bint_signless = integer::create(signless);
	EXPECT_EQ(bint_signless->value, 123);
	EXPECT_EQ(bint_signless->rule_name, GetRuleName());
}

// false test

TEST_F(IntegerASTTest, FalseTestNoNumb)
{
	std::shared_ptr<peg::Ast> no_number;
	EXPECT_FALSE(parser->parse("abc", no_number));
}

TEST_F(IntegerASTTest, FalseTestWithWhiteSpace)
{
	std::shared_ptr<peg::Ast> with_whitespace;
	EXPECT_FALSE(parser->parse("12 34", with_whitespace));
}

} // namespace bpy::ast
