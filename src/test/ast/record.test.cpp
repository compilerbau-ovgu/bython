#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/record/record.hpp"

namespace bpy::ast {

class RecordASTTest : public testing::BythonTestBase {
  public:
	RecordASTTest() : BythonTestBase{RuleName::Record}
	{
	}
};

TEST_F(RecordASTTest, CreateMinimal)
{
	std::shared_ptr<peg::Ast> record_ast;
	EXPECT_TRUE(parser->parse("record Stuff { a : Int }", record_ast));
	const auto record_object = record::create(record_ast);

	EXPECT_EQ(record_object->rule_name, GetRuleName());
}

TEST_F(RecordASTTest, GetterTypeName)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(parser->parse("record Stuff { a : Int }", getter_type_name));
	const auto record_getter =
		record::create(getter_type_name)->get_type_name();
	EXPECT_STREQ(record_getter.c_str(), "Stuff");
}

// false Test

TEST_F(RecordASTTest, FalseTestMissRecord)
{
	std::shared_ptr<peg::Ast> miss_record;
	EXPECT_FALSE(parser->parse("Stuff { a : Int, b : Float }", miss_record));
}

TEST_F(RecordASTTest, FalseTestMissTypeName)
{
	std::shared_ptr<peg::Ast> miss_tn;
	EXPECT_FALSE(parser->parse("record { a : Int, b : Float }", miss_tn));
}

TEST_F(RecordASTTest, FalseTestMissList)
{
	std::shared_ptr<peg::Ast> miss_list;
	EXPECT_FALSE(parser->parse("record Stuff", miss_list));
}

TEST_F(RecordASTTest, FalseTestNoComma)
{
	std::shared_ptr<peg::Ast> miss_comma;
	EXPECT_FALSE(
		parser->parse("record Stuff { a : Int b : Float }", miss_comma));
}

TEST_F(RecordASTTest, FalseTestWrongComma)
{
	std::shared_ptr<peg::Ast> wrong_comma;
	EXPECT_FALSE(
		parser->parse("record Stuff { a : Int; b : Float }", wrong_comma));
}

TEST_F(RecordASTTest, FalseTestNoBrackets)
{
	std::shared_ptr<peg::Ast> no_brackets;
	EXPECT_FALSE(
		parser->parse("record Stuff a : Int, b : Float ", no_brackets));
}

TEST_F(RecordASTTest, FalseTestWrongBrackets)
{
	std::shared_ptr<peg::Ast> wrong_brackets;
	EXPECT_FALSE(
		parser->parse("record Stuff [ a : Int, b : Float } ", wrong_brackets));
}

} // namespace bpy::ast
