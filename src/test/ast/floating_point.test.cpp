#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/floating_point.hpp"

namespace bpy::ast {

class FloatASTTest : public testing::BythonTestBase {
  public:
	FloatASTTest() : BythonTestBase{RuleName::FloatConstant}
	{
	}
};

TEST_F(FloatASTTest, Create)
{
	// signless
	std::shared_ptr<peg::Ast> signless;
	EXPECT_TRUE(parser->parse("123.2", signless));
	const auto bfloat_signless = floating_point::create(signless);
	EXPECT_EQ(bfloat_signless->rule_name, GetRuleName());
	EXPECT_FLOAT_EQ(bfloat_signless->value, 123.2);
}

// false Test

TEST_F(FloatASTTest, FalseTestNoNumb)
{
	std::shared_ptr<peg::Ast> no_number;
	EXPECT_FALSE(parser->parse("pi.e", no_number));
}

TEST_F(FloatASTTest, FalseTestWhiteSpace1)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("12 3.45"));
}
// should be false, ows at integer
/*TEST_F(FloatASTTest, CreateFalseWhiteSpace2)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("123 .45"));
}*/

TEST_F(FloatASTTest, FalseTestWhiteSpace3)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("123.4 5"));
}

TEST_F(FloatASTTest, FalseTestComma)
{
	std::shared_ptr<peg::Ast> comma;
	EXPECT_FALSE(parser->parse("12,34", comma));
}

TEST_F(FloatASTTest, FalseTestSemicolon)
{
	std::shared_ptr<peg::Ast> semicolon;
	EXPECT_FALSE(parser->parse("12;34", semicolon));
}

} // namespace bpy::ast
