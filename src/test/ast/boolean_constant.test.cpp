#include <gtest/gtest.h>
#include <test_base.hpp>
#include <variant>

#include "ast/constant/boolean_constant.hpp"

namespace bpy::ast {
class BooleanConstantASTTest : public testing::BythonTestBase {
  public:
	BooleanConstantASTTest() : BythonTestBase{RuleName::BooleanConstant}
	{
	}
};

TEST_F(BooleanConstantASTTest, CreateFalse)
{
	// boolean - false
	std::shared_ptr<peg::Ast> false_ast;
	EXPECT_TRUE(parser->parse("false", false_ast));
	const auto false_node = ast::boolean_constant::create(false_ast);

	EXPECT_FALSE(false_node->value);

	EXPECT_EQ(false_node->rule_name, GetRuleName());
}

TEST_F(BooleanConstantASTTest, CreateTrue)
{
	// boolean - true
	std::shared_ptr<peg::Ast> true_ast;
	EXPECT_TRUE(parser->parse("true", true_ast));
	const auto true_node = ast::boolean_constant::create(true_ast);

	EXPECT_TRUE(true_node->value);

	EXPECT_EQ(true_node->rule_name, GetRuleName());
}

} // namespace bpy::ast