#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/none_boolean_expression.hpp"

namespace bpy::ast {

class NoneBooleanExpressionASTTest : public testing::BythonTestBase {
  public:
	NoneBooleanExpressionASTTest()
		: BythonTestBase{RuleName::NoneBooleanExpression}
	{
	}
};

TEST_F(NoneBooleanExpressionASTTest, CreateExpression)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("a", isIdentifier));
	const auto expression = none_boolean_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<variable_name>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneBooleanExpressionASTTest, CreateBooleanConstant)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("true", isIdentifier));
	const auto expression = none_boolean_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<boolean_constant>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneBooleanExpressionASTTest, CreateComparision)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("a==b", isIdentifier));
	const auto expression = none_boolean_expression::create(isIdentifier);
	EXPECT_NO_THROW(std::get<std::shared_ptr<comparison>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneBooleanExpressionASTTest, CreateBooleanExpression)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("!(a&&b)", isIdentifier));
	const auto expression = none_boolean_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<boolean_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneBooleanExpressionASTTest, CreateCallExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("!String::new ()", is_identifier));
	const auto expression = none_boolean_expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<call_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneBooleanExpressionASTTest, CreateIfExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("!if true {} else {}", is_identifier));
	const auto expression = none_boolean_expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<if_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
} // namespace bpy::ast
