#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/none_arithmetic_expression.hpp"

namespace bpy::ast {

class NoneArithmeticExpressionASTTest : public testing::BythonTestBase {
  public:
	NoneArithmeticExpressionASTTest()
		: BythonTestBase{RuleName::NoneArithmeticExpression}
	{
	}
};

TEST_F(NoneArithmeticExpressionASTTest, CreateCallExprArithmeticExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("-String::new ()", is_identifier));
	const auto expression = none_arithmetic_expression::create(is_identifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<call_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneArithmeticExpressionASTTest, CreateIfExprArithmeticExpression)
{
	std::shared_ptr<peg::Ast> is_identifier;
	EXPECT_TRUE(parser->parse("-if true {} else {}", is_identifier));
	const auto expression = none_arithmetic_expression::create((is_identifier));
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<if_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
TEST_F(NoneArithmeticExpressionASTTest, CreateVariableArithmeticExpression)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("a", isIdentifier));
	const auto expression = none_arithmetic_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<variable_name>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneArithmeticExpressionASTTest, CreateNegativeArithmeticExpression)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("-a", isIdentifier));
	const auto expression = none_arithmetic_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<variable_name>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneArithmeticExpressionASTTest, CreateArithmeticExpressionTest)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("-(a+b)", isIdentifier));
	const auto expression = none_arithmetic_expression::create(isIdentifier);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<arithmetic_expression>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(NoneArithmeticExpressionASTTest, CreateNumericArithmeticExpression)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("-4", isIdentifier));
	const auto expression = none_arithmetic_expression::create(isIdentifier);
	EXPECT_NO_THROW(std::get<std::shared_ptr<numeric>>(expression->value));

	EXPECT_EQ(expression->rule_name, GetRuleName());
	EXPECT_EQ(expression->unary_arithmetic_operator.value(), "-");
}
} // namespace bpy::ast
