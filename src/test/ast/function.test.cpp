#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/function/function.hpp"

namespace bpy::ast {

class FunctionASTTest : public testing::BythonTestBase {
  public:
	FunctionASTTest() : BythonTestBase{RuleName::Function}
	{
	}
};

TEST_F(FunctionASTTest, CreateMinimal)
{
	// signless
	std::shared_ptr<peg::Ast> function_ast;
	EXPECT_TRUE(parser->parse("func stuff = {}", function_ast));
	const auto function_object = function::create(function_ast);
	EXPECT_FALSE((bool) function_object->args);
	EXPECT_FALSE((bool) function_object->has_type());

	EXPECT_EQ(function_object->rule_name, GetRuleName());
}

TEST_F(FunctionASTTest, CreateWithArgs)
{
	// signless
	std::shared_ptr<peg::Ast> function_ast;
	EXPECT_TRUE(parser->parse("func stuff = a:Int, b:String {}", function_ast));
	const auto function_object = function::create(function_ast);
	EXPECT_TRUE((bool) function_object->args);
	EXPECT_FALSE((bool) function_object->has_type());

	EXPECT_EQ(function_object->rule_name, GetRuleName());
}

TEST_F(FunctionASTTest, CreateWithType)
{
	// signless
	std::shared_ptr<peg::Ast> function_ast;
	EXPECT_TRUE(parser->parse("func stuff = -> Int {}", function_ast));
	const auto function_object = function::create(function_ast);
	EXPECT_FALSE((bool) function_object->args);
	EXPECT_TRUE((bool) function_object->has_type());

	EXPECT_EQ(function_object->rule_name, GetRuleName());
}

TEST_F(FunctionASTTest, CreateFull)
{
	// signless
	std::shared_ptr<peg::Ast> function_ast;
	EXPECT_TRUE(
		parser->parse("func stuff = a:Int, b:String -> Int {}", function_ast));
	const auto function_object = function::create(function_ast);
	EXPECT_TRUE((bool) function_object->args);
	EXPECT_TRUE((bool) function_object->has_type());

	EXPECT_EQ(function_object->rule_name, GetRuleName());
}

TEST_F(FunctionASTTest, GetterFunctionName)
{
	std::shared_ptr<peg::Ast> getter_function_name;
	EXPECT_TRUE(parser->parse("func stuff = -> Int {}", getter_function_name));
	const auto function_getter =
		function::create(getter_function_name)->get_function_name();
	EXPECT_STREQ(function_getter->get_name().c_str(), "stuff");
}

TEST_F(FunctionASTTest, GetterReturnTypeTrue)
{
	std::shared_ptr<peg::Ast> getter_return_type;
	EXPECT_TRUE(parser->parse("func stuff = -> Int {}", getter_return_type));
	const auto function_getter =
		function::create(getter_return_type)->get_return_type()->get_name();
	EXPECT_STREQ(function_getter.c_str(), "Int");
}

TEST_F(FunctionASTTest, GetterReturnTypeFalse)
{
	std::shared_ptr<peg::Ast> getter_return_type;
	EXPECT_TRUE(parser->parse("func stuff = {}", getter_return_type));
	EXPECT_FALSE(function::create(getter_return_type)->has_type());
	/*const auto function_getter =
		function::create(getter_return_type)->get_return_type()->get_name();
	EXPECT_STREQ(function_getter.c_str(), "");*/
}

// false Test

TEST_F(FunctionASTTest, FalseTestMissFunc)
{
	std::shared_ptr<peg::Ast> miss_func;
	EXPECT_FALSE(parser->parse("stuff = Void -> Int {}", miss_func));
}

TEST_F(FunctionASTTest, FalseTestMissEqual)
{
	std::shared_ptr<peg::Ast> miss_equal;
	EXPECT_FALSE(parser->parse("func stuff Void {}", miss_equal));
}

TEST_F(FunctionASTTest, FalseTestDoubleEqual)
{
	std::shared_ptr<peg::Ast> double_equal;
	EXPECT_FALSE(parser->parse("func stuff == Void -> Int {}", double_equal));
}

TEST_F(FunctionASTTest, FalseTestMissBlockExpr)
{
	std::shared_ptr<peg::Ast> miss_blockexpr;
	EXPECT_FALSE(parser->parse("func stuff = ", miss_blockexpr));
}

} // namespace bpy::ast
