#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/constant.hpp"

namespace bpy::ast {

class ConstantASTTest : public testing::BythonTestBase {
  public:
	ConstantASTTest() : BythonTestBase{RuleName::Constant}
	{
	}
};

TEST_F(ConstantASTTest, CreateNumeric)
{
	// numeric - float
	std::shared_ptr<peg::Ast> floating_point;
	EXPECT_TRUE(parser->parse("123.242", floating_point));
	const auto constant_float = ast::constant::create(floating_point);
	EXPECT_NO_THROW(std::get<ast::numeric::instance>(constant_float->value));

	EXPECT_EQ(constant_float->rule_name, GetRuleName());
}

TEST_F(ConstantASTTest, CreateString)
{
	// string - single quotes
	std::shared_ptr<peg::Ast> single_quotes;
	EXPECT_TRUE(parser->parse("'Hello World!'", single_quotes));
	EXPECT_TRUE(single_quotes != nullptr);

	const auto constant_single_string = ast::constant::create(single_quotes);
	EXPECT_NO_THROW(
		std::get<ast::bstring::instance>(constant_single_string->value));

	EXPECT_EQ(constant_single_string->rule_name, GetRuleName());
}

TEST_F(ConstantASTTest, CreateBooleanConstant)
{
	// boolean - true
	std::shared_ptr<peg::Ast> true_ast;
	EXPECT_TRUE(parser->parse("true", true_ast));
	const auto true_node = ast::constant::create(true_ast);

	EXPECT_NO_THROW(
		std::get<ast::boolean_constant::instance>(true_node->value));
	EXPECT_EQ(true_node->rule_name, GetRuleName());
}
} // namespace bpy::ast
