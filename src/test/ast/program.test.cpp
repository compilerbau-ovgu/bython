#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/program.hpp"

namespace bpy::ast {

class ProgramASTTest : public testing::BythonTestBase {
  public:
	ProgramASTTest() : BythonTestBase{RuleName::Program}
	{
	}
};

TEST_F(ProgramASTTest, CreateEmpty)
{
	// signless
	std::shared_ptr<peg::Ast> program_ast;
	EXPECT_TRUE(parser->parse("", program_ast));
	const auto program_object = program::create(program_ast);
	EXPECT_EQ(program_object->contents.size(), 0);

	EXPECT_EQ(program_object->rule_name, GetRuleName());
}

TEST_F(ProgramASTTest, CreateFunction)
{
	// signless
	std::shared_ptr<peg::Ast> program_ast;
	EXPECT_TRUE(parser->parse("func stuff = {}", program_ast));
	const auto program_object = program::create(program_ast);
	EXPECT_EQ(program_object->contents.size(), 1);

	EXPECT_EQ(program_object->rule_name, GetRuleName());
}

TEST_F(ProgramASTTest, CreateRecord)
{
	// signless
	std::shared_ptr<peg::Ast> program_ast;
	EXPECT_TRUE(parser->parse("record Stuff { a : Int }", program_ast));
	const auto program_object = program::create(program_ast);
	EXPECT_EQ(program_object->contents.size(), 1);

	EXPECT_EQ(program_object->rule_name, GetRuleName());
}

/*TEST_F(ProgramASTTest, CreateStateMachine)
{
	std::shared_ptr<peg::Ast> program_ast;
	EXPECT_TRUE(parser->parse(R"(
record Transition {
	from: String,
	to: String,
	trigger: String,
	invokeUponActivation: Callable[Void]
}


record StateMachine {
	transitions: List[Transition],
	startState: String
}


func main = argv: List[String] -> Int {
	let dailyLifeStateMachine = StateMachine::new(
		List[Transition]::new(
			Transition::new("home", "work", "home2work", Void -> Void {
				println("taking train from home to work")
			}),
			Transition::new("work", "home", "work2home", Void -> Void {
				println("taking train from work to home")
			}),
			Transition::new("home", "bed", "home2bed", Void -> Void {
				println("going to bed")
			}),
			Transition::new("bed", "home", "bed2home", Void -> Void {
				println("waking up from bed")
			})
		), "home"
	);

	if x == y && y == x {
		y
	} else {
		z
	}
}
)",
							  program_ast));

	const auto program_object = program::create(program_ast);
	EXPECT_EQ(program_object->rule_name, GetRuleName());
} */

} // namespace bpy::ast
