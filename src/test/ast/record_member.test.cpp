#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/record/record_member.hpp"

namespace bpy::ast {

class RecordMemberASTTest : public testing::BythonTestBase {
  public:
	RecordMemberASTTest() : BythonTestBase{RuleName::RecordMember}
	{
	}
};

TEST_F(RecordMemberASTTest, Create)
{
	std::shared_ptr<peg::Ast> args;
	EXPECT_TRUE(parser->parse("variableOne: TypeOne", args));
	const auto record_member = record_member::create(args);

	EXPECT_STREQ(record_member->get_var_name().c_str(), "variableOne");
	EXPECT_STREQ(record_member->get_type_name().c_str(), "TypeOne");

	const auto deps = record_member->dependencies();
	EXPECT_EQ(std::size(deps), 1);

	auto names = std::vector<std::string>{};
	std::transform(std::begin(deps), std::end(deps), std::back_inserter(names),
				   [](const auto& dep) {
					   const auto dep_as_typename =
						   std::dynamic_pointer_cast<ast::type_name>(dep);
					   return dep_as_typename->get_name();
				   });

	EXPECT_NE(std::find(std::begin(names), std::end(names), "TypeOne"),
			  std::end(names));

	EXPECT_EQ(record_member->rule_name, GetRuleName());
}

TEST_F(RecordMemberASTTest, GetterVarName)
{
	std::shared_ptr<peg::Ast> getter_var_name;
	EXPECT_TRUE(parser->parse("variableOne: TypeOne", getter_var_name));
	const auto rm_getter =
		record_member::create(getter_var_name)->get_var_name();
	EXPECT_STREQ(rm_getter.c_str(), "variableOne");
}

TEST_F(RecordMemberASTTest, GetterTypeName)
{
	std::shared_ptr<peg::Ast> getter_type_name;
	EXPECT_TRUE(parser->parse("variableOne: TypeOne", getter_type_name));
	const auto rm_getter =
		record_member::create(getter_type_name)->get_type_name();
	EXPECT_STREQ(rm_getter.c_str(), "TypeOne");
}

// false Test

TEST_F(RecordMemberASTTest, FalseTestMissingColon)
{
	std::shared_ptr<peg::Ast> miss_colon;
	EXPECT_FALSE(parser->parse("VariableOne TypeOne", miss_colon));
}

TEST_F(RecordMemberASTTest, FalseTestNotAColon)
{
	std::shared_ptr<peg::Ast> not_colon;
	EXPECT_FALSE(parser->parse("VariableOne; TypeOne", not_colon));
}

} // namespace bpy::ast
