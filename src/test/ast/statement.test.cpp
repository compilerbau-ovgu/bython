#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/statement/statement.hpp"

namespace bpy::ast {

class StatementASTTest : public testing::BythonTestBase {
  public:
	StatementASTTest() : BythonTestBase{RuleName::Statement}
	{
	}
};

TEST_F(StatementASTTest, CreateLetStatement)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("let a = {};", state));
	const auto statement_test = statement::create(state);
	EXPECT_NO_THROW(
		std::get<std::shared_ptr<let_statement>>(statement_test->statements));
}

TEST_F(StatementASTTest, CreateBlockExpression)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("{};", state));
	const auto statement_test = statement::create(state);
	EXPECT_NO_THROW(
		std::get<block_expression::instance>(statement_test->statements));

	EXPECT_EQ(statement_test->rule_name, GetRuleName());
}

TEST_F(StatementASTTest, CreateIfStatement)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("if true {};", state));
	const auto statement_test = statement::create(state);
	EXPECT_NO_THROW(
		std::get<if_statement::instance>(statement_test->statements));

	EXPECT_EQ(statement_test->rule_name, GetRuleName());
}

TEST_F(StatementASTTest, CreateCallExpression)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("a();", state));
	const auto statement_test = statement::create(state);
	EXPECT_NO_THROW(
		std::get<call_expression::instance>(statement_test->statements));

	EXPECT_EQ(statement_test->rule_name, GetRuleName());
}

// false Test

TEST_F(StatementASTTest, FalseTestMissSemicolon)
{
	std::shared_ptr<peg::Ast> miss_semi;
	EXPECT_FALSE(parser->parse("if true {}", miss_semi));
}

TEST_F(StatementASTTest, FalseTestComma)
{
	std::shared_ptr<peg::Ast> comma;
	EXPECT_FALSE(parser->parse("if true {},", comma));
}

} // namespace bpy::ast
