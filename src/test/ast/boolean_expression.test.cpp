#include <fstream>
#include <gtest/gtest.h>
#include <peglib.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/boolean_expression.hpp"

namespace bpy::ast {

class BooleanExpressionASTTest : public testing::BythonTestBase {
  public:
	BooleanExpressionASTTest() : BythonTestBase{RuleName::BooleanExpression}
	{
	}
};

TEST_F(BooleanExpressionASTTest, CreateContextual)

{
	// Convert to boolean from context
	std::shared_ptr<peg::Ast> context;
	EXPECT_TRUE(parser->parse("a", context));
	const auto expression = boolean_expression::create(context);
	EXPECT_FALSE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(BooleanExpressionASTTest, CreateNegatedContextual)
{
	// Compare variables
	std::shared_ptr<peg::Ast> not_expr;
	EXPECT_TRUE(parser->parse("!a", not_expr));
	const auto expression = boolean_expression::create(not_expr);
	EXPECT_FALSE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(BooleanExpressionASTTest, CreateOr)
{
	// Convert both variables to boolean, then bool-or them
	std::shared_ptr<peg::Ast> ored;
	EXPECT_TRUE(parser->parse("a||b", ored));
	const auto expression = boolean_expression::create(ored);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(BooleanExpressionASTTest, CreateOrAndChain)
{
	// Convert all variables to boolean, then bool-or + bool-and them
	std::shared_ptr<peg::Ast> or_and;
	EXPECT_TRUE(parser->parse("a||a&&b", or_and));
	const auto expression = boolean_expression::create(or_and);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(BooleanExpressionASTTest, CreateComparisonChain)
{
	// Compare variables
	std::shared_ptr<peg::Ast> comp_chain;
	EXPECT_TRUE(parser->parse("a >= b || b >= a", comp_chain));
	const auto expression = boolean_expression::create(comp_chain);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(BooleanExpressionASTTest, CreateNegatedComparison)
{
	// Negate GEQ
	std::shared_ptr<peg::Ast> not_expr;
	EXPECT_TRUE(parser->parse("!(a >= b)", not_expr));
	const auto expression = boolean_expression::create(not_expr);
	EXPECT_FALSE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

// false Tests

TEST_F(BooleanExpressionASTTest, FalseTestNoRHS)
{
	std::shared_ptr<peg::Ast> no_rhs;
	EXPECT_FALSE(parser->parse("a ||", no_rhs));
}

TEST_F(BooleanExpressionASTTest, FalseTestNoLHS)
{
	std::shared_ptr<peg::Ast> no_lhs;
	EXPECT_FALSE(parser->parse("&& b", no_lhs));
}

TEST_F(BooleanExpressionASTTest, FalseTestNoOP)
{
	std::shared_ptr<peg::Ast> no_op;
	EXPECT_FALSE(parser->parse("a b"));
}

} // namespace bpy::ast
