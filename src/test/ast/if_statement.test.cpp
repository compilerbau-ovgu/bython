#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/statement/if_statement.hpp"

namespace bpy::ast {

class IfStatementASTTest : public testing::BythonTestBase {
  public:
	IfStatementASTTest() : BythonTestBase{RuleName::IfStatement}
	{
	}
};

TEST_F(IfStatementASTTest, Create)
{
	std::shared_ptr<peg::Ast> if_expression_ast;
	EXPECT_TRUE(parser->parse("if true {}", if_expression_ast));
	const auto if_statement_test = if_statement::create(if_expression_ast);

	EXPECT_EQ(if_statement_test->rule_name, GetRuleName());
}

TEST_F(IfStatementASTTest, CreateElseIf)
{
	std::shared_ptr<peg::Ast> if_expression_ast;
	EXPECT_TRUE(
		parser->parse("if true {} else if false {}", if_expression_ast));
	const auto if_statement_test = if_statement::create(if_expression_ast);

	EXPECT_EQ(if_statement_test->rule_name, GetRuleName());
}

// false Test

TEST_F(IfStatementASTTest, FalseTestMissIf1)
{
	std::shared_ptr<peg::Ast> miss_if;
	EXPECT_FALSE(parser->parse("true {}", miss_if));
}

TEST_F(IfStatementASTTest, FalseTestMissIf2)
{
	std::shared_ptr<peg::Ast> miss_if;
	EXPECT_FALSE(parser->parse("if true {} else false {}", miss_if));
}

TEST_F(IfStatementASTTest, FalseTestMissBool1)
{
	std::shared_ptr<peg::Ast> miss_bool;
	EXPECT_FALSE(parser->parse("if {} else if false {}", miss_bool));
}

TEST_F(IfStatementASTTest, FalseTestMissBool2)
{
	std::shared_ptr<peg::Ast> miss_bool;
	EXPECT_FALSE(parser->parse("if true {} else if {}", miss_bool));
}

TEST_F(IfStatementASTTest, FalseTestMissBlock)
{
	std::shared_ptr<peg::Ast> miss_block;
	EXPECT_FALSE(parser->parse("if true else if false {}", miss_block));
}

TEST_F(IfStatementASTTest, FalseTestMissWhitespace1)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("iftrue {} else if false {}", whitespace));
}

TEST_F(IfStatementASTTest, FalseTestMissWhitespace2)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE(parser->parse("if true {} else iffalse {}", whitespace));
}
} // namespace bpy::ast
