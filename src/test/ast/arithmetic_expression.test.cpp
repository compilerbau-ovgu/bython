#include <fstream>
#include <gtest/gtest.h>
#include <peglib.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/arithmetic_expression.hpp"

namespace bpy::ast {

class ArithmeticExpressionASTTest : public testing::BythonTestBase {
  public:
	ArithmeticExpressionASTTest()
		: BythonTestBase{RuleName::ArithmeticExpression}
	{
	}
};

TEST_F(ArithmeticExpressionASTTest, CreateContextual)
{
	std::shared_ptr<peg::Ast> contextual;
	EXPECT_TRUE(parser->parse("a", contextual));
	const auto expression = arithmetic_expression::create(contextual);
	EXPECT_FALSE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, CreateAddition)
{
	std::shared_ptr<peg::Ast> addition;
	EXPECT_TRUE(parser->parse("a+b", addition));
	const auto expression = arithmetic_expression::create(addition);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, CreateAdditionMultiplicationChain)
{
	std::shared_ptr<peg::Ast> add_mult;
	EXPECT_TRUE(parser->parse("a+a*b", add_mult));
	const auto expression = arithmetic_expression::create(add_mult);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, Division)
{
	std::shared_ptr<peg::Ast> add_mult;
	EXPECT_TRUE(parser->parse("a+a/b", add_mult));
	const auto expression = arithmetic_expression::create(add_mult);
	EXPECT_TRUE((bool) expression->has_rhs());

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, GetterGetRHS)
{
	std::shared_ptr<peg::Ast> get_rhs;
	EXPECT_TRUE(parser->parse("a+b*b", get_rhs));
	const auto arith_get_rhs = arithmetic_expression::create(get_rhs);
	EXPECT_TRUE(arith_get_rhs->get_rhs());

	EXPECT_EQ(arith_get_rhs->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, GetterGetRHSFalse)
{
	std::shared_ptr<peg::Ast> get_rhs;
	EXPECT_TRUE(parser->parse("a", get_rhs));
	const auto arith_get_rhs = arithmetic_expression::create(get_rhs);
	EXPECT_FALSE(arith_get_rhs->has_rhs());

	EXPECT_EQ(arith_get_rhs->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, GetterGetOP)
{
	std::shared_ptr<peg::Ast> get_op;
	EXPECT_TRUE(parser->parse("a+b", get_op));
	const auto arith_get_op = arithmetic_expression::create(get_op)->get_op();
	EXPECT_STREQ(arith_get_op.c_str(), "+");
}

TEST_F(ArithmeticExpressionASTTest, GetterGetLHS1)
{
	std::shared_ptr<peg::Ast> get_lhs;
	EXPECT_TRUE(parser->parse("a+b*b", get_lhs));
	const auto arith_get_lhs = arithmetic_expression::create(get_lhs);
	EXPECT_TRUE(arith_get_lhs->get_lhs());

	EXPECT_EQ(arith_get_lhs->rule_name, GetRuleName());
}

TEST_F(ArithmeticExpressionASTTest, GetterGetLHS2)
{
	std::shared_ptr<peg::Ast> get_lhs;
	EXPECT_TRUE(parser->parse("-int", get_lhs));
	const auto arith_get_lhs = arithmetic_expression::create(get_lhs);
	EXPECT_TRUE(arith_get_lhs->get_lhs());

	EXPECT_EQ(arith_get_lhs->rule_name, GetRuleName());
}

// false Tests
TEST_F(ArithmeticExpressionASTTest, FalseTestNoRHS)
{
	std::shared_ptr<peg::Ast> no_rhs;
	EXPECT_FALSE(parser->parse("lhs *", no_rhs));
}

TEST_F(ArithmeticExpressionASTTest, FalseTestNoLHS)
{
	std::shared_ptr<peg::Ast> no_lhs;
	EXPECT_FALSE(parser->parse("* rhs", no_lhs));
}

TEST_F(ArithmeticExpressionASTTest, FalseTestNoOP)
{
	std::shared_ptr<peg::Ast> no_op;
	EXPECT_FALSE(parser->parse("lhs rhs", no_op));
}

} // namespace bpy::ast
