#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/identifier.hpp"

namespace bpy::ast {

class IdentifierASTTest : public testing::BythonTestBase {
  public:
	IdentifierASTTest() : BythonTestBase{RuleName::Identifier}
	{
	}
};

TEST_F(IdentifierASTTest, Create)
{
	// starts with capital letter
	std::shared_ptr<peg::Ast> capitalLetter;
	EXPECT_TRUE(parser->parse("BytHon7asDf", capitalLetter));
	const auto ident_capitalLetter = identifier::create(capitalLetter);
	EXPECT_STREQ(ident_capitalLetter->ident.c_str(), "BytHon7asDf");

	// starts with lower case letter
	std::shared_ptr<peg::Ast> lowerCaseLetter;
	EXPECT_TRUE(parser->parse("bytHon7asDf", lowerCaseLetter));
	const auto ident_lowerCaseLetter = identifier::create(lowerCaseLetter);
	EXPECT_STREQ(ident_lowerCaseLetter->ident.c_str(), "bytHon7asDf");

	// false tests
	std::shared_ptr<peg::Ast> startsWithNumber;
	EXPECT_FALSE(parser->parse("7BytH0n", startsWithNumber));
	// empty dependencies
	const auto deps = ident_capitalLetter->dependencies();
	EXPECT_TRUE(deps.empty());

	EXPECT_EQ(ident_capitalLetter->rule_name, GetRuleName());
}

// false Test

TEST_F(IdentifierASTTest, FalseTestNoIdentifier1)
{
	std::shared_ptr<peg::Ast> no_identifier;
	EXPECT_FALSE(parser->parse("5ummand", no_identifier));
}

TEST_F(IdentifierASTTest, FalseTestNoIdentifier2)
{
	std::shared_ptr<peg::Ast> no_identifier;
	EXPECT_FALSE(parser->parse("#count", no_identifier));
}

TEST_F(IdentifierASTTest, FalseTestWhitespace1)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE((parser->parse("c ount", whitespace)));
}

TEST_F(IdentifierASTTest, FalseTestWhitespace2)
{
	std::shared_ptr<peg::Ast> whitespace;
	EXPECT_FALSE((parser->parse("cou nt", whitespace)));
}

TEST_F(IdentifierASTTest, KeywordTrue)
{
	std::shared_ptr<peg::Ast> kw_true;
	EXPECT_FALSE((parser->parse("true", kw_true)));

	EXPECT_FALSE((parser->parse("true because", kw_true)));

	EXPECT_TRUE((parser->parse("truebecause", kw_true)));
	const auto kw_identf = identifier::create(kw_true);
	EXPECT_STREQ(kw_identf->ident.c_str(), "truebecause");

	EXPECT_TRUE((parser->parse("istruebecause", kw_true)));
	const auto kw_identm = identifier::create(kw_true);
	EXPECT_STREQ(kw_identm->ident.c_str(), "istruebecause");

	EXPECT_TRUE((parser->parse("istrue", kw_true)));
	const auto kw_identb = identifier::create(kw_true);
	EXPECT_STREQ(kw_identb->ident.c_str(), "istrue");
}

TEST_F(IdentifierASTTest, KeywordFalse)
{
	std::shared_ptr<peg::Ast> kw_false;
	EXPECT_FALSE((parser->parse("false", kw_false)));

	EXPECT_FALSE((parser->parse("false because", kw_false)));

	EXPECT_TRUE((parser->parse("falsebecause", kw_false)));
	const auto kw_identf = identifier::create(kw_false);
	EXPECT_STREQ(kw_identf->ident.c_str(), "falsebecause");

	EXPECT_TRUE((parser->parse("isfalsebecause", kw_false)));
	const auto kw_identm = identifier::create(kw_false);
	EXPECT_STREQ(kw_identm->ident.c_str(), "isfalsebecause");

	EXPECT_TRUE((parser->parse("isfalse", kw_false)));
	const auto kw_identb = identifier::create(kw_false);
	EXPECT_STREQ(kw_identb->ident.c_str(), "isfalse");
}

TEST_F(IdentifierASTTest, KeywordIf)
{
	std::shared_ptr<peg::Ast> kw_if;
	EXPECT_FALSE((parser->parse("if", kw_if)));

	EXPECT_FALSE((parser->parse("if this happend", kw_if)));

	EXPECT_TRUE((parser->parse("ifthishappend", kw_if)));
	const auto kw_identf = identifier::create(kw_if);
	EXPECT_STREQ(kw_identf->ident.c_str(), "ifthishappend");

	EXPECT_TRUE((parser->parse("elseifthishappend", kw_if)));
	const auto kw_identm = identifier::create(kw_if);
	EXPECT_STREQ(kw_identm->ident.c_str(), "elseifthishappend");

	EXPECT_TRUE((parser->parse("elseif", kw_if)));
	const auto kw_identb = identifier::create(kw_if);
	EXPECT_STREQ(kw_identb->ident.c_str(), "elseif");
}

TEST_F(IdentifierASTTest, KeywordElse)
{
	std::shared_ptr<peg::Ast> kw_else;
	EXPECT_FALSE((parser->parse("else", kw_else)));

	EXPECT_FALSE((parser->parse("else this happend", kw_else)));

	EXPECT_TRUE((parser->parse("elsethishappend", kw_else)));
	const auto kw_identf = identifier::create(kw_else);
	EXPECT_STREQ(kw_identf->ident.c_str(), "elsethishappend");

	EXPECT_TRUE((parser->parse("ifelsethishappend", kw_else)));
	const auto kw_identm = identifier::create(kw_else);
	EXPECT_STREQ(kw_identm->ident.c_str(), "ifelsethishappend");

	EXPECT_TRUE((parser->parse("ifelse", kw_else)));
	const auto kw_identb = identifier::create(kw_else);
	EXPECT_STREQ(kw_identb->ident.c_str(), "ifelse");
}

TEST_F(IdentifierASTTest, KeywordFunc)
{
	std::shared_ptr<peg::Ast> kw_func;
	EXPECT_FALSE((parser->parse("func", kw_func)));

	EXPECT_FALSE((parser->parse("func callme", kw_func)));

	EXPECT_TRUE((parser->parse("funccallme", kw_func)));
	const auto kw_identf = identifier::create(kw_func);
	EXPECT_STREQ(kw_identf->ident.c_str(), "funccallme");

	EXPECT_TRUE((parser->parse("thisfunccallme", kw_func)));
	const auto kw_identm = identifier::create(kw_func);
	EXPECT_STREQ(kw_identm->ident.c_str(), "thisfunccallme");

	EXPECT_TRUE((parser->parse("thisfunc", kw_func)));
	const auto kw_identb = identifier::create(kw_func);
	EXPECT_STREQ(kw_identb->ident.c_str(), "thisfunc");
}

TEST_F(IdentifierASTTest, KeywordNew)
{
	std::shared_ptr<peg::Ast> kw_new;
	EXPECT_FALSE((parser->parse("new", kw_new)));

	EXPECT_FALSE((parser->parse("new type", kw_new)));

	EXPECT_TRUE((parser->parse("newtype", kw_new)));
	const auto kw_identf = identifier::create(kw_new);
	EXPECT_STREQ(kw_identf->ident.c_str(), "newtype");

	EXPECT_TRUE((parser->parse("thisnewtype", kw_new)));
	const auto kw_identm = identifier::create(kw_new);
	EXPECT_STREQ(kw_identm->ident.c_str(), "thisnewtype");

	EXPECT_TRUE((parser->parse("thisnew", kw_new)));
	const auto kw_identb = identifier::create(kw_new);
	EXPECT_STREQ(kw_identb->ident.c_str(), "thisnew");
}

} // namespace bpy::ast
