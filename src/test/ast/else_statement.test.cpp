#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/statement/else_statement.hpp"

namespace bpy::ast {

class ElseStatementASTTest : public testing::BythonTestBase {
  public:
	ElseStatementASTTest() : BythonTestBase{RuleName::ElseStatement}
	{
	}
};

TEST_F(ElseStatementASTTest, CreateBlock)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("else {}", state));
	const auto statement_test = else_statement::create(state);

	EXPECT_EQ(statement_test->rule_name, GetRuleName());
}

TEST_F(ElseStatementASTTest, CreateIfStatement)
{
	std::shared_ptr<peg::Ast> state;
	EXPECT_TRUE(parser->parse("else if true {}", state));
	const auto statement_test = else_statement::create(state);

	EXPECT_EQ(statement_test->rule_name, GetRuleName());
}

// false Tests

TEST_F(ElseStatementASTTest, FalseTestMissElse1)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("{}", miss_else));
}

TEST_F(ElseStatementASTTest, FalseTestMissElse2)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("if true {}", miss_else));
}

TEST_F(ElseStatementASTTest, FalseTestMissExpr)
{
	std::shared_ptr<peg::Ast> miss_expr;
	EXPECT_FALSE(parser->parse("else ", miss_expr));
}

} // namespace bpy::ast
