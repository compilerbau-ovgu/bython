#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/numeric.hpp"

namespace bpy::ast {

class NumericASTTest : public testing::BythonTestBase {
  public:
	NumericASTTest() : BythonTestBase{RuleName::NumericConstant}
	{
	}
};

TEST_F(NumericASTTest, CreateInteger)
{
	// integer
	std::shared_ptr<peg::Ast> integer;
	EXPECT_TRUE(parser->parse("122", integer));
	const auto bnumeric_integer = numeric::create(integer);
	EXPECT_NO_THROW(std::get<integer::instance>(bnumeric_integer->value));

	EXPECT_EQ(bnumeric_integer->rule_name, GetRuleName());
}

TEST_F(NumericASTTest, CreateFloat)
{
	// float
	std::shared_ptr<peg::Ast> floating_point;
	EXPECT_TRUE(parser->parse("123.242", floating_point));
	const auto bnumeric_float = numeric::create(floating_point);
	EXPECT_NO_THROW(std::get<floating_point::instance>(bnumeric_float->value));

	EXPECT_EQ(bnumeric_float->rule_name, GetRuleName());
}

} // namespace bpy::ast
