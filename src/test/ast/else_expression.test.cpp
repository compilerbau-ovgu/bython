#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/expression/else_expression.hpp"

namespace bpy::ast {

class ElseExpressionASTTest : public testing::BythonTestBase {
  public:
	ElseExpressionASTTest() : BythonTestBase{RuleName::ElseExpression}
	{
	}
};

TEST_F(ElseExpressionASTTest, CreateBlock)
{
	std::shared_ptr<peg::Ast> else_expression_ast;
	EXPECT_TRUE(parser->parse("else {}", else_expression_ast));
	const auto else_expression_test =
		else_expression::create(else_expression_ast);

	EXPECT_EQ(else_expression_test->rule_name, GetRuleName());
}

TEST_F(ElseExpressionASTTest, CreateIfStatment)
{
	std::shared_ptr<peg::Ast> else_expression_ast;
	EXPECT_TRUE(parser->parse("else if true {} else {}", else_expression_ast));
	const auto else_expression_test =
		else_expression::create(else_expression_ast);

	EXPECT_EQ(else_expression_test->rule_name, GetRuleName());
}

// false Tests

TEST_F(ElseExpressionASTTest, FalseTestMissElse1)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("{}", miss_else));
}

TEST_F(ElseExpressionASTTest, FalseTestMissElse2)
{
	std::shared_ptr<peg::Ast> miss_else;
	EXPECT_FALSE(parser->parse("if true {} else {}", miss_else));
}

TEST_F(ElseExpressionASTTest, FalseTestMissExpr)
{
	std::shared_ptr<peg::Ast> miss_expr;
	EXPECT_FALSE(parser->parse("else ", miss_expr));
}

} // namespace bpy::ast
