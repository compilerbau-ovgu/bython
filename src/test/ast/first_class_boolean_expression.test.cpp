#include <fstream>
#include <gtest/gtest.h>
#include <peglib.h>
#include <test_base.hpp>

#include "ast/arithmetic_expression/first_class_boolean_expression.hpp"

namespace bpy::ast {

class FirstClassBooleanExpressionASTTest : public testing::BythonTestBase {
  public:
	FirstClassBooleanExpressionASTTest()
		: BythonTestBase{RuleName::FirstClassBooleanExpression}
	{
	}
};

TEST_F(FirstClassBooleanExpressionASTTest, CreateContextual)
{
	std::shared_ptr<peg::Ast> isIdentifier;
	EXPECT_TRUE(parser->parse("a", isIdentifier));
	const auto expression =
		first_class_boolean_expression::create(isIdentifier);
	EXPECT_FALSE((bool) expression->binary_operator);

	EXPECT_EQ(expression->rule_name, GetRuleName());
}

TEST_F(FirstClassBooleanExpressionASTTest, CreateLiteralAnd)
{
	// true and false
	std::shared_ptr<peg::Ast> true_and_false;
	EXPECT_TRUE(parser->parse("true && false", true_and_false));
	const auto expression =
		first_class_boolean_expression::create(true_and_false);
	EXPECT_TRUE((bool) expression->binary_operator);

	EXPECT_EQ(expression->rule_name, GetRuleName());
}
} // namespace bpy::ast
