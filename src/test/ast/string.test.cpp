#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/string.hpp"

namespace bpy::ast {

class StringASTTest : public testing::BythonTestBase {
  public:
	StringASTTest() : BythonTestBase{RuleName::StringConstant}
	{
	}
};

TEST_F(StringASTTest, CreateSingleQuotes)
{
	// single quotes
	std::shared_ptr<peg::Ast> single_quotes;
	EXPECT_TRUE(parser->parse("'Hello World!'", single_quotes));
	EXPECT_TRUE(single_quotes != nullptr);

	const auto sstr = bstring::create(single_quotes);
	EXPECT_STREQ(sstr->chars.c_str(), "Hello World!");

	EXPECT_EQ(sstr->rule_name, GetRuleName());
}

TEST_F(StringASTTest, CreateDoubleQuotes)
{
	// double quotes
	std::shared_ptr<peg::Ast> double_quotes;
	EXPECT_TRUE(parser->parse("\"Hello World!\"", double_quotes));
	EXPECT_TRUE(double_quotes != nullptr);

	const auto dstr = bstring::create(double_quotes);
	EXPECT_STREQ(dstr->chars.c_str(), "Hello World!");

	EXPECT_EQ(dstr->rule_name, GetRuleName());
}

// false Test

TEST_F(StringASTTest, FalseTestMissingQuotes)
{
	std::shared_ptr<peg::Ast> miss_quotes;
	EXPECT_FALSE(parser->parse("Hello World", miss_quotes));
}

} // namespace bpy::ast
