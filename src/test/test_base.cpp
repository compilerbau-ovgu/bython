#include "test_base.hpp"

#include <compiler.hpp>
#include <fstream>
#include <gtest/gtest.h>
#include <parser.cpp>

#include "rules.hpp"

namespace bpy::testing {
BythonTestBase::BythonTestBase(ast::RuleName rule_name_) : rule_name{rule_name_}
{
}

void BythonTestBase::SetUp()
{
	auto grammar_file = std::ifstream{GRAMMAR_PATH};
	if (grammar_file.fail()) {
		throw std::invalid_argument{"Could not load grammar file!"};
	}
	const auto grammar =
		std::string{(std::istreambuf_iterator<char>{grammar_file}),
					std::istreambuf_iterator<char>{}};

	parser = new peg::parser{grammar.c_str()};
	parser->set_start(ast::rule_name_to_str(rule_name));
	parser->enable_ast();
}

void BythonTestBase::TearDown()
{
	delete parser;
}

ast::RuleName BythonTestBase::GetRuleName()
{
	return rule_name;
}

auto BythonProgrammTest::test_program(const std::string& program_path) -> void
{
	compiler::build_context::get_instance().reset();
	std::cerr << "Reading File:";
	const std::string test_programm = read_file(EXAMPLES_PATH + program_path);
	std::cerr << "Success!\n";

	std::shared_ptr<peg::Ast> test_ast;
	EXPECT_TRUE(parser->parse(test_programm.c_str(), test_ast));
	EXPECT_NE(test_ast, nullptr);

	EXPECT_TRUE(compiler::compile(test_ast));
}

} // namespace bpy::testing
