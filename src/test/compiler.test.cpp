#include <compiler.hpp>
#include <parser.hpp>
#include <test_base.hpp>

namespace bpy::compiler {
class CompilerTest : public testing::BythonProgrammTest {
};

TEST_F(CompilerTest, halloworld)
{
	test_program("hello_world.by");
}

TEST_F(CompilerTest, functiontest)
{
	test_program("functions.by");
}

} // namespace bpy::compiler
