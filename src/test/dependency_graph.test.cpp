#include <gtest/gtest.h>

#include "ast/program.hpp"
#include "dependency_graph.hpp"
#include "ostream_overloaded.hpp"
#include "test_base.hpp"

namespace bpy {

class DependencyGraphTest : public testing::BythonTestBase {
  public:
	DependencyGraphTest() : BythonTestBase{ast::RuleName::Program}
	{
	}
};

TEST_F(DependencyGraphTest, Create)
{
	// Simply compile a program and pass all dependencies in order
	std::shared_ptr<peg::Ast> program;
	EXPECT_TRUE(parser->parse(R"(
        record b {
            c : Int
        }

        func a = n:b -> b {
            a(n)
        }

        func main = -> b {
            a(b::new(42))
        }
	)",
							  program));

	const ast::program::instance program_ast = ast::program::create(program);
	const std::shared_ptr<dependency_graph> dgraph =
		std::make_shared<dependency_graph>(program_ast);

	std::vector<ast::base::instance> top_order;
	EXPECT_NO_THROW(top_order = dgraph->topological_order());
	EXPECT_EQ(top_order.size(), 4);

	for (const ast::base::instance& dep_name : top_order) {
		const auto& dep = program_ast->get(dep_name);
	}
}

} // namespace bpy
