#include <gtest/gtest.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <test_base.hpp>

#include "ast/constant/boolean_constant.hpp"
#include "ast/constant/constant.hpp"
#include "ast/constant/numeric.hpp"
#include "ast/constant/string.hpp"
#include "compiler/constant/constant.hpp"

namespace bpy::compiler {
class ConstantCompileTest : public testing::BythonTestBase {
  public:
	ConstantCompileTest() : BythonTestBase{bpy::ast::RuleName::Constant}
	{
	}
};

TEST_F(ConstantCompileTest, CodegenNumeric)
{
	// numeric - float
	std::shared_ptr<peg::Ast> floating_point;
	EXPECT_TRUE(parser->parse("123.242", floating_point));
	EXPECT_NE(floating_point, nullptr);

	const auto constant_float = ast::constant::create(floating_point);
	EXPECT_NO_THROW(std::get<ast::numeric::instance>(constant_float->value));

	// Check if Floating Point
	const auto float_codegen = compiler::codegen(constant_float);
	EXPECT_EQ(
		float_codegen->getType(),
		llvm::IntegerType::getFloatTy(*build_context::get_instance().context));
}

TEST_F(ConstantCompileTest, CodegenString)
{
	// string - single quotes
	std::shared_ptr<peg::Ast> single_quotes;
	EXPECT_TRUE(parser->parse("'Hello World!'", single_quotes));
	EXPECT_NE(single_quotes, nullptr);

	const auto constant_single_string = ast::constant::create(single_quotes);
	EXPECT_NO_THROW(
		std::get<ast::bstring::instance>(constant_single_string->value));

	compiler::codegen(constant_single_string);

	const auto gvar =
		build_context::get_instance().module->getNamedValue(".strHello World!");
	EXPECT_TRUE(gvar != nullptr);

	// Assume each char is 8 bit wide
	// TODO: Match [std::size("Hello World") x i8]*

	// Module
	auto module = llvm::CloneModule(*build_context::get_instance().module);
	auto eb = llvm::EngineBuilder{std::move(module)};
	const auto ee = eb.create();

	const char* data = static_cast<const char*>(
		ee->getPointerToGlobalIfAvailable(gvar->getName()));
	EXPECT_TRUE(data != nullptr);
	EXPECT_STREQ(data, "Hello World!");
}

TEST_F(ConstantCompileTest, CodegenBooleanConstant)
{
	// boolean - true
	std::shared_ptr<peg::Ast> true_ast;
	EXPECT_TRUE(parser->parse("true", true_ast));
	const auto true_node = ast::constant::create(true_ast);
	const auto true_codegen = codegen(true_node);

	auto& bc = *build_context::get_instance().context;
	EXPECT_EQ(true_codegen->getType(),
			  llvm::ConstantInt::getTrue(bc)->getType());
}
} // namespace bpy::compiler
