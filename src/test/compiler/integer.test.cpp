#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/integer.hpp"
#include "compiler/constant/integer.hpp"

namespace bpy::compiler {

class IntegerCompilerTest : public testing::BythonTestBase {
  public:
	IntegerCompilerTest() : BythonTestBase{ast::RuleName::IntegerConstant}
	{
	}
};

TEST_F(IntegerCompilerTest, Codegen)
{
	// minus sign
	std::shared_ptr<peg::Ast> minus;
	EXPECT_TRUE(parser->parse("123", minus));
	const auto bint_minus = ast::integer::create(minus);
	EXPECT_EQ(bint_minus->value, 123);

	// codegen the given value
	const auto const_int = compiler::codegen(bint_minus);

	// check as many attributes that are publicly available as possible
	EXPECT_EQ(const_int->getSExtValue(), 123);

	// Expect 64 bits; see Issue #53
	EXPECT_EQ(32, const_int->getBitWidth());
}

} // namespace bpy::compiler
