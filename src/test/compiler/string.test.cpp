#include <gtest/gtest.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/IR/Constant.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <test_base.hpp>

#include "ast/constant/string.hpp"
#include "compiler.hpp"
#include "compiler/constant/string.hpp"

namespace bpy::compiler {
class StringCompilerTest : public testing::BythonTestBase {
  public:
	StringCompilerTest() : BythonTestBase{bpy::ast::RuleName::StringConstant}
	{
	}
};

TEST_F(StringCompilerTest, Create)
{
	// single quotes
	std::shared_ptr<peg::Ast> single_quotes;
	EXPECT_TRUE(parser->parse("'Hello World!'", single_quotes));
	EXPECT_TRUE(single_quotes != nullptr);

	const auto dstr = ast::bstring::create(single_quotes);
	EXPECT_STREQ(dstr->chars.c_str(), "Hello World!");

	// the codegen invokes the following actions:
	//	1. creates a .strHelloWorld! at global scope in the llvm::Module
	//	2. initialises a string with "Hello World!"
	//	3. returns a pointer to the start of the string
	codegen(dstr);

	const auto gvar =
		build_context::get_instance().module->getNamedValue(".strHello World!");
	EXPECT_TRUE(gvar != nullptr);

	// Assume each char is 8 bit wide
	// TODO: Match [std::size("Hello World") x i8]*

	// Module
	auto module = llvm::CloneModule(*build_context::get_instance().module);
	auto eb = llvm::EngineBuilder{std::move(module)};
	const auto ee = eb.create();

	const char* data = static_cast<const char*>(
		ee->getPointerToGlobalIfAvailable(gvar->getName()));
	EXPECT_TRUE(data != nullptr);
	EXPECT_STREQ(data, "Hello World!");
}
} // namespace bpy::compiler
