#include <gtest/gtest.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <test_base.hpp>

#include "ast/constant/boolean_constant.hpp"
#include "compiler/constant/boolean_constant.hpp"

namespace bpy::compiler {

class BooleanConstantCompilerTest : public testing::BythonTestBase {
  public:
	BooleanConstantCompilerTest()
		: BythonTestBase{bpy::ast::RuleName::BooleanConstant}
	{
	}
};

TEST_F(BooleanConstantCompilerTest, CodegenFalse)
{
	// boolean - false
	std::shared_ptr<peg::Ast> false_ast;
	EXPECT_TRUE(parser->parse("false", false_ast));
	const auto false_node = ast::boolean_constant::create(false_ast);

	const auto false_codegen = codegen(false_node);
	const auto false_codegen_ci = llvm::cast<llvm::ConstantInt>(false_codegen);
	ASSERT_EQ(false_codegen_ci->getBitWidth(), 1);
	ASSERT_EQ(false_codegen_ci->getValue(), 0);
	ASSERT_EQ(false_node->rule_name, bpy::ast::RuleName::BooleanConstant);
}

TEST_F(BooleanConstantCompilerTest, CodegenTrue)
{
	// boolean - true
	std::shared_ptr<peg::Ast> true_ast;
	EXPECT_TRUE(parser->parse("true", true_ast));
	const auto true_node = ast::boolean_constant::create(true_ast);

	const auto true_codegen = codegen(true_node);
	const auto true_codegen_ci = llvm::cast<llvm::ConstantInt>(true_codegen);
	ASSERT_EQ(true_codegen_ci->getBitWidth(), 1);
	ASSERT_EQ(true_codegen_ci->getValue(), 1);

	ASSERT_EQ(true_node->rule_name, bpy::ast::RuleName::BooleanConstant);
}
} // namespace bpy::compiler
