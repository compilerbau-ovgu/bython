#include <gtest/gtest.h>
#include <test_base.hpp>

#include "ast/constant/numeric.hpp"
#include "compiler/constant/numeric.hpp"

namespace bpy::compiler {
class NumericCompilerTest : public testing::BythonTestBase {
  public:
	NumericCompilerTest() : BythonTestBase{bpy::ast::RuleName::NumericConstant}
	{
	}
};

TEST_F(NumericCompilerTest, Create)
{
	// integer
	std::shared_ptr<peg::Ast> integer;
	EXPECT_TRUE(parser->parse("122", integer));
	const auto bnumeric_integer = ast::numeric::create(integer);
	EXPECT_NO_THROW(std::get<ast::integer::instance>(bnumeric_integer->value));

	// float
	std::shared_ptr<peg::Ast> floating_point;
	EXPECT_TRUE(parser->parse("123.242", floating_point));
	const auto bnumeric_float = ast::numeric::create(floating_point);
	EXPECT_NO_THROW(
		std::get<ast::floating_point::instance>(bnumeric_float->value));

	// Check if Signed Int
	const auto integer_codegen = compiler::codegen(bnumeric_integer);
	EXPECT_EQ(
		integer_codegen->getType(),
		llvm::IntegerType::getInt32Ty(*build_context::get_instance().context));

	// Check if Floating Point
	const auto float_codegen = compiler::codegen(bnumeric_float);
	EXPECT_EQ(
		float_codegen->getType(),
		llvm::IntegerType::getFloatTy(*build_context::get_instance().context));
}
} // namespace bpy::compiler
