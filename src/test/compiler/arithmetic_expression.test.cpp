#include <gtest/gtest.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <test_base.hpp>

#include "compiler/arithmetic_expression/arithmetic_expression.hpp"

namespace bpy::compiler {
class ArithmeticExpressionCodegenTest : public testing::BythonTestBase {
  public:
	ArithmeticExpressionCodegenTest()
		: BythonTestBase{bpy::ast::RuleName::ArithmeticExpression}
	{
	}
};
TEST_F(ArithmeticExpressionCodegenTest, CodegenArithmeticExpression)
{
	// numeric - float
	std::shared_ptr<peg::Ast> arithmetic;
	EXPECT_TRUE(parser->parse("5+5", arithmetic));
	EXPECT_NE(arithmetic, nullptr);

	const auto arithmetic_expression =
		ast::arithmetic_expression::create(arithmetic);

	// Check arithmetic
	const auto arithmetic_codegen = compiler::codegen(arithmetic_expression);
}

} // namespace bpy::compiler
