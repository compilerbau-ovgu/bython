/// \file

#include "compiler.hpp"

#include "llvm/Transforms/InstCombine/InstCombine.h"

#include <compiler/function.hpp>
#include <llvm/Bitcode/BitcodeReader.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Utils/Cloning.h>

#include "dependency_graph.hpp"
#include "rules.hpp"
#include "type_system.hpp"

namespace bpy::compiler {

/**
 * Helper function to generate function types
 *
 * @tparam ParameterTypes types of parameters
 * @param return_type the return type of the function
 * @param parameter_types the types of of the arguments to be passed
 * @return the corresponding function type
 */
template <typename... ParameterTypes>
static auto make_funct_l(llvm::Type* return_type,
						 ParameterTypes... parameter_types)
	-> llvm::FunctionType*
{
	return llvm::FunctionType::get(
		return_type,
		llvm::ArrayRef<llvm::Type*>{parameter_types...},
		/*isVarArg=*/false);
}

/**
 * An exception that is to be thrown if an error occurs during the build process
 */
struct build_exception : std::exception {
	const char* message;
	build_exception(const char* message) : message(message)
	{
	}
	[[nodiscard]] const char* what() const noexcept override
	{
		return message;
	}
};

build_context::build_context()
	: context(std::make_shared<llvm::LLVMContext>())
	, builder(std::make_shared<llvm::IRBuilder<>>(*context))
	, module(std::make_shared<llvm::Module>("BythonModule", *context))
{
    // register all builtin functions
	add_io_builtins();
	add_math_builtins();
	add_list_builtins();
	add_builtin_types();
}

/**
 * Implements singleton trait
 *
 * @return an instance of the build_context class
 */
auto build_context::get_instance() -> build_context&
{
	return singleton<build_context>::get_instance();
}

/**
 * Reset the contents of the class
 */
auto build_context::reset() -> void
{
	module = std::make_shared<llvm::Module>("BythonModule", *context);
	namedValues.clear();
}

/**
 * Register all io functions
 */
auto build_context::add_io_builtins() -> void
{
	auto& c = *context;

	// println_int(int) -> void
	builtin_functions["println_int"] = make_funct_l(
		llvm::Type::getVoidTy(c), types::get_ir_type<types::BInt>()(c));

    // print_int(int) -> void
	builtin_functions["print_int"] = make_funct_l(
		llvm::Type::getVoidTy(c), types::get_ir_type<types::BInt>()(c));

    // println_float(float) -> void
	builtin_functions["println_float"] = make_funct_l(
		llvm::Type::getVoidTy(c), types::get_ir_type<types::BFloat>()(c));

    // print_float(int) -> void
	builtin_functions["print_float"] = make_funct_l(
		llvm::Type::getVoidTy(c), types::get_ir_type<types::BFloat>()(c));

    // println_str(i8*) -> void
	builtin_functions["println_str"] =
		make_funct_l(llvm::Type::getVoidTy(c), types::get_ir_type<char*>()(c));

    // print_str(i8*) -> void
	builtin_functions["print_str"] =
		make_funct_l(llvm::Type::getVoidTy(c), types::get_ir_type<char*>()(c));
}

/**
 * Register all builtin functions for lists
 */
auto build_context::add_list_builtins() -> void
{
	auto& c = *context;

	// list_int_init(void) -> i8*
	builtin_functions["list_int_init"] =
		make_funct_l(types::get_ir_type<void*>()(c));

    // list_float_init(void) -> i8*
	builtin_functions["list_float_init"] =
		make_funct_l(types::get_ir_type<void*>()(c));

    // list_string_init(void) -> i8*
	builtin_functions["list_string_init"] =
		make_funct_l(types::get_ir_type<void*>()(c));

    // list_merge(i8*, i8*) -> i8*
	builtin_functions["list_merge"] =
		make_funct_l(types::get_ir_type<void*>()(c), // out
					 types::get_ir_type<void*>()(c), // list1,
					 types::get_ir_type<void*>()(c)  // list2
		);

    // list_print(i8*) -> void
	builtin_functions["list_print"] =
		make_funct_l(llvm::Type::getVoidTy(c), types::get_ir_type<void*>()(c));

    // list_size(i8*) -> int
	builtin_functions["list_size"] = make_funct_l(
		types::get_ir_type<types::BInt>()(c), types::get_ir_type<void*>()(c));

    // list_push_back(i8*, i8*) -> i8*
	builtin_functions["list_push_back"] =
		make_funct_l(types::get_ir_type<void*>()(c),		// out
					 types::get_ir_type<void*>()(c),		// in
					 types::get_ir_type<types::BInt>()(c)); // value

    // list_get(i8*, int) -> i8*
	builtin_functions["list_get"] =
		make_funct_l(types::get_ir_type<void*>()(c),
					 types::get_ir_type<void*>()(c),
					 types::get_ir_type<types::BInt>()(c));

	// list_get_int(i8*, int) -> int
	builtin_functions["list_get_int"] =
		make_funct_l(types::get_ir_type<types::BInt>()(c),
					 types::get_ir_type<void*>()(c),
					 types::get_ir_type<types::BInt>()(c));

	// list_pop_front(i8*) -> i8*
	builtin_functions["list_pop_front"] =
		make_funct_l(types::get_ir_type<void*>()(c),  // out
					 types::get_ir_type<void*>()(c)); // in
}

/**
 * Register all math related builtin functions
 */
auto build_context::add_math_builtins() -> void
{
	auto& c = *context;

	// sin(float) -> float
	builtin_functions["sin"] =
		make_funct_l(types::get_ir_type<types::BFloat>()(c),
					 types::get_ir_type<types::BFloat>()(c));

    // iabs(int) -> int
	builtin_functions["iabs"] =
		make_funct_l(types::get_ir_type<types::BInt>()(c),
					 types::get_ir_type<types::BInt>()(c));

    // fabs(float) -> float
	builtin_functions["fabs"] =
		make_funct_l(types::get_ir_type<types::BFloat>()(c),
					 types::get_ir_type<types::BFloat>()(c));
}

/**
 * Register all builtin types
 */
auto build_context::add_builtin_types() -> void
{
	builtin_types.emplace_back("Int");
	builtin_types.emplace_back("Float");
	builtin_types.emplace_back("Bool");
	builtin_types.emplace_back("Void");
	builtin_types.emplace_back("String");
	builtin_types.emplace_back("List");
}

/**
 * Time the execution of a function
 *
 * @tparam Function the type of the function \p f to be timed
 * @tparam Parameters the types of the parameters to be passed to the function \p f
 * @param f the function to be timed
 * @param parameters the parameters to be passed to the function \p f
 * @return the return value of the function \p f
 */
template <typename Function, typename... Parameters>
static auto time_execution(Function f, Parameters... parameters)
{
	const auto start = std::chrono::steady_clock::now();
	const auto result = std::invoke(f, parameters...);

	const auto end = std::chrono::steady_clock::now();
	const auto time_taken =
		std::chrono::duration_cast<std::chrono::microseconds>(end - start)
			.count();

	std::cerr << "Took: " << time_taken << "µs\n\n";
	return result;
}

/**
 * Convert the AST into LLVM IR Code and execute it
 *
 * @param ptr a shared pointer to the AST to be compiled
 * @return true if the compilation was successful, false otherwise
 */
auto compile(const std::shared_ptr<peg::Ast>& ptr) -> bool
{
    // convert ast into codegennable classes
	const auto program = ast::program::create(ptr);

	std::cerr << "Compiling:\n";
	// time topological sorting
	const auto ordered = time_execution(
		[&](const std::shared_ptr<peg::Ast>& ptr_) {
			// std::cerr << "Parsing Program\n";
			std::cerr << *program;
			const auto dep_graph = dependency_graph{program};
			return dep_graph.topological_order();
			// return program->contents;
		},
		ptr);

	// Compile functions
	if (!time_execution(
			[&](const decltype(ordered)& blocks) {
				try {
					int N = blocks.size();
					int i = 0;
					for (const auto& block : blocks) {
						const auto generable = program->get(block);

						std::cerr << "[" << std::to_string(++i) << "/"
								  << std::to_string(N) << "] Building " << block
								  << ": ";

						auto& bc = build_context::get_instance();

						if (const auto fptr =
								std::dynamic_pointer_cast<ast::function>(
									generable);
							fptr != nullptr) {
							codegen(fptr);
							std::cerr << "Success!\n";
						}

						// NOTE: We cannot codegen records
						else if (const auto tptr =
									 std::dynamic_pointer_cast<ast::record>(
										 generable)) {
							// std::cerr << "Detected type name: " << *tptr
							// <<
							// "\n";
							// TODO: Check if record type, then codegen
							std::cerr << "Success!\n";
						}
						else if (const auto tptr = std::dynamic_pointer_cast<
									 ast::function_name>(block)) {
							if (bc.builtin_functions.find(tptr->get_name()) !=
								bc.builtin_functions.end()) {
								std::cerr << "Builtin Funktion!\n";
							}
							else {
								throw build_exception("Function not found!");
							}
						}
						else if (const auto tptr =
									 std::dynamic_pointer_cast<ast::type_name>(
										 block)) {
							if (std::find(bc.builtin_types.begin(),
										  bc.builtin_types.end(),
										  tptr->get_name()) !=
								bc.builtin_types.end()) {
								std::cerr << "Builtin Type!\n";
							}
							else {
								throw build_exception("Type not found!");
							}
						}
						else {
							throw build_exception("Building failed!");
						}
					}
				}

				catch (const std::exception& e) {
					std::cerr << e.what() << "\n";
					return false;
				}

				return true;
			},
			ordered)) {
		return false;
	}

	auto& bc = build_context::get_instance();

	std::cerr << "Printing IR Code:\n\n";
	std::cerr << stringify_value((*bc.module)) << "\n";

	const auto bython_main_fn = (*bc.module).getFunction("main");
	if (bython_main_fn == nullptr) {
		std::cerr << "ERROR: No main function given!\n";
		return false;
	}

	// JIT compile LLVM IR code
	std::cerr << "Running EngineBuilder: ";
	auto module = llvm::CloneModule((*bc.module));
	auto eb = llvm::EngineBuilder{std::move(module)};
	const auto ee = eb.create();
	std::cerr << "Success!\n";

	// Execute program
	std::cerr << "Running Programm:\n";
	// TODO: Arguments go here
	// IMPORTANT: DO NOT CHANGE (0) to {0}, because the std::string
	// constructor will attempt to construct a string from a nullptr
	const auto argv = std::vector<std::string>(0);
	const auto ret =
		ee->runFunctionAsMain(bython_main_fn, argv, /*envp=*/nullptr);

	// TODO: Is using bython_main_fn after move of unique_ptr safe?
	std::cerr << "return value: " << ret << "\n";

	return true;
}
} // namespace bpy::compiler
