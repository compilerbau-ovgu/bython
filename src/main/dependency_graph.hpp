#pragma once

#include <unordered_map>
#include <unordered_set>
#include <variant>

#include "ast/base.hpp"
#include "ast/program.hpp"

namespace bpy {
class dependency_graph {
  public:
	dependency_graph() = delete;
	dependency_graph(const ast::program::instance&);

	auto topological_order() const -> std::vector<ast::base::instance>;

  private:
	using hasher = ast::hash_by;
	using comparator = ast::identical_to;

	using base_inst = ast::base::instance;

	using base_inst_set = std::unordered_set<base_inst,
											 /*class Hash=*/hasher,
											 /*class KeyEqual=*/comparator>;

	using base_inst_map = std::unordered_map<base_inst,
											 base_inst_set,
											 /*class Hash=*/hasher,
											 /*class KeyEqual=*/comparator>;

	base_inst_map graph_;
	base_inst main_function;

	auto add_connection(const ast::base::instance&, const ast::base::instance&)
		-> void;
	auto contains(const ast::base::instance&) const -> bool;

	friend auto operator<<(std::ostream&, const dependency_graph&)
		-> std::ostream&;
	friend auto operator<<(std::ostream&,
						   const dependency_graph::base_inst_map&)
		-> std::ostream&;
};

auto operator<<(std::ostream&, const dependency_graph&) -> std::ostream&;
auto operator<<(std::ostream&, const dependency_graph::base_inst_map&)
	-> std::ostream&;
} // namespace bpy
