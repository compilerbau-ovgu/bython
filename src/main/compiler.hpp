#pragma once

#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"

#include <iostream>
#include <memory>
#include <peglib.h>

#include "ast/base.hpp"
#include "ast/ostream_overloaded.hpp"
#include "ast/program.hpp"
#include "singleton.hpp"

namespace bpy::compiler {

auto compile(const std::shared_ptr<peg::Ast>& ptr) -> bool;

template <typename LLVMValue>
auto stringify_value(const LLVMValue& v) -> std::string
{
	auto value_str = std::string{};
	auto value_rso = llvm::raw_string_ostream{value_str};

	value_rso << v << "\n";
	value_rso.flush();

	return value_str;
}

template <typename Iter>
auto stringify_container(Iter start, Iter end, std::string delim = "")
	-> std::string
{
	auto r = std::string{};
	auto r_rso = llvm::raw_string_ostream{r};

	while (start != end) {
		r_rso << *start;
		if (++start != end) {
			r_rso << delim;
		}
	}

	return r;
}

class build_context : public crtp::singleton<build_context> {
  public:
	build_context();

	static auto get_instance() -> build_context&;
	auto reset() -> void;

	std::shared_ptr<llvm::LLVMContext> context;
	std::shared_ptr<llvm::IRBuilder<>> builder;
	std::shared_ptr<llvm::Module> module;

	std::map<std::string, llvm::Value*> namedValues;
	std::map<std::string, llvm::FunctionType*> builtin_functions;
	std::vector<std::string> builtin_types;

  private:
	auto add_io_builtins() -> void;
	auto add_math_builtins() -> void;
	auto add_list_builtins() -> void;
	auto add_builtin_types() -> void;
};
} // namespace bpy::compiler
