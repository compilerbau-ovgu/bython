#pragma once

#include <ast/variable_name.hpp>
#include <llvm/IR/Value.h>

namespace bpy::compiler {

auto read(const std::string&) -> llvm::Value*;

auto read(const ast::variable_name::instance&) -> llvm::Value*;

auto store(const std::string&, llvm::Value*) -> llvm::Value*;

auto store(const ast::variable_name::instance&, llvm::Value*) -> llvm::Value*;
} // namespace bpy::compiler