/// \file

#include "variable_name.hpp"

#include <compiler.hpp>

namespace bpy::compiler {

/**
 * Read a variable
 *
 * @param identifier a string containing the name of the variable
 * @return a pointer to the value stored in \p identifier
 */
auto read(const std::string& identifier) -> llvm::Value*
{
	auto& bc = build_context::get_instance();
	return bc.namedValues.at(identifier);
}

/**
 * Read a variable
 *
 * @param instance a shared pointer to the variable to be codegenned
 * @return a pointer to the value store in \p instance
 */
auto read(const bpy::ast::variable_name::instance& instance) -> llvm::Value*
{
	return read(instance->get_name());
}


/**
 * Store a value in a variable
 *
 * @param lhs a string containing the name of the variable
 * @param variable_value a pointer to the value to be stored in \p variable_value
 * @return \p variable_value
 */
auto store(const std::string& lhs, llvm::Value* variable_value) -> llvm::Value*
{
	auto& bc = build_context::get_instance();
	bc.namedValues[lhs] = variable_value;
	return variable_value;
}


/**
 * Store a value in a variable
 *
 * @param lhs a shared pointer to the variable
 * @param variable_value a pointer to the value to be stored in \p variable_value
 * @return \p variable_value
 */
auto store(const ast::variable_name::instance& lhs, llvm::Value* variable_value)
	-> llvm::Value*
{
	return store(lhs->get_name(), variable_value);
}
} // namespace bpy::compiler
