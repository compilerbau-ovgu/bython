#pragma once

#include <ast/function/function.hpp>
#include <llvm/IR/Function.h>

namespace bpy::compiler {
    auto codegen(const ast::function::instance&) -> llvm::Function*;
}