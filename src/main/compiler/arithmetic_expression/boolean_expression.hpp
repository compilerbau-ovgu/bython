#include <llvm/IR/Value.h>

#include "ast/arithmetic_expression/boolean_expression.hpp"

namespace bpy::compiler {
	auto codegen(const ast::boolean_expression::instance&) -> llvm::Value*;
}