/// \file

#include "first_class_boolean_expression.hpp"

#include <compiler.hpp>

#include "none_boolean_expression.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR Code for a first class boolean expression.
 *
 * Supported operations are logical-and, logical-or, not-equals and equals.
 *
 * @param fc_bexpr a shared pointer to the first class boolean expression to be codegenned
 * @return a value that points to the generated instruction
 *
 * @throws std::logic_error if the op is unknown
 */
auto codegen(const ast::first_class_boolean_expression::instance& fc_bexpr)
	-> llvm::Value*
{
	const auto& lhs_genned = codegen(fc_bexpr->value);
	// check if this is a unary boolean expression
	if (fc_bexpr->binary_operator == std::nullopt) {
		return lhs_genned;
	}

	const auto& [op, rhs] = *fc_bexpr->binary_operator;

	const auto& rhs_genned = codegen(rhs);

	auto& bc = build_context::get_instance();

	llvm::Value* boolean_op_instr = nullptr;

	// logical and
	if (op == "&&") {
		boolean_op_instr = bc.builder->CreateAnd(lhs_genned, rhs_genned, "and");
	}
	// logical or
	else if (op == "||") {
		boolean_op_instr = bc.builder->CreateOr(lhs_genned, rhs_genned, "or");
	}
	// non equal
	else if (op == "!=") {
		boolean_op_instr =
			bc.builder->CreateICmpNE(lhs_genned, rhs_genned, "neq");
	}
	// equal
	else if (op == "==") {
		boolean_op_instr =
			bc.builder->CreateICmpEQ(lhs_genned, rhs_genned, "eq");
	}
	// unknown op
	else {
		throw std::logic_error{"Unexpected operator: " + op};
	}

	return boolean_op_instr;
}
} // namespace bpy::compiler
