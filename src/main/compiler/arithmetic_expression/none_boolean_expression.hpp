#include <llvm/IR/Value.h>

#include "ast/arithmetic_expression/none_boolean_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::none_boolean_expression::instance&) -> llvm::Value*;
}