#pragma once

#include <ast/arithmetic_expression/arithmetic_expression.hpp>
#include <llvm/IR/Constant.h>

#include "first_class_arithmetic_expression.hpp"

namespace bpy::compiler {

auto codegen(const ast::arithmetic_expression::instance&) -> llvm::Value*;

auto codegen_binary_op(llvm::Value*, const std::string&, llvm::Value*)
	-> llvm::Value*;
} // namespace bpy::compiler
