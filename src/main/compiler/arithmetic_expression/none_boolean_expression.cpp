/// \file

#include "none_boolean_expression.hpp"

#include "ast/arithmetic_expression/boolean_expression.hpp"
#include "ast/arithmetic_expression/comparison.hpp"
#include "ast/constant/boolean_constant.hpp"
#include "ast/variable_name.hpp"
#include "compiler.hpp"
#include "compiler/arithmetic_expression/boolean_expression.hpp"
#include "compiler/arithmetic_expression/comparison.hpp"
#include "compiler/constant/boolean_constant.hpp"
#include "compiler/expression/call_expression.hpp"
#include "compiler/expression/if_expression.hpp"
#include "compiler/expression/lambda_expression.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR Code for a none boolean expression.
 *
 * The only supported unary boolean operator is the unary not.
 *
 * @param none_bexpr a shared pointer to the none boolean expressionn to codegenned
 * @return a value that points to the generated instruction
 *
 * @throws std::logic_error if the unary op is unknown
 */
auto codegen(const ast::none_boolean_expression::instance& none_bexpr)
	-> llvm::Value*
{
	struct codegen_visitor {
		build_context& bc;

		explicit codegen_visitor(build_context& bc_) : bc{bc_}
		{
		}

		auto operator()(const ast::boolean_expression::instance& bexpr)
		{
			// a boolean expression returns a boolean
			return codegen(bexpr);
		}

		auto operator()(const ast::comparison::instance& comp)
		{
			// all comparisons should return a boolean
			return codegen(comp);
		}

		auto operator()(const ast::call_expression::instance& call)
		{
			return codegen(call);
		}

		auto operator()(const ast::lambda_expression::instance& lambda)
		{
			return codegen(lambda);
		}

		auto operator()(const ast::if_expression::instance& ifexpr)
		{
			return codegen(ifexpr);
		}

		auto operator()(const ast::boolean_constant::instance& bconst)
		{
			// already a boolean
			return llvm::dyn_cast<llvm::Value>(codegen(bconst));
		}

		auto operator()(const ast::variable_name::instance& varname)
		{
			// must cast to boolean
			const auto& named_values = bc.namedValues;
			if (const auto& ident = named_values.find(varname->get_name());
				ident != named_values.end()) {
				llvm::Value* load_instr =
					bc.builder->CreateLoad(llvm::Type::getInt1Ty(*bc.context),
										   ident->second,
										   "castToBool");
				return load_instr;
			}

			throw std::logic_error{"Unable to find variable name in map: " +
								   varname->get_name()};
		}
	};

	auto& bc = build_context::get_instance();
	llvm::Value* rhs = std::visit(codegen_visitor{bc}, none_bexpr->value);

	llvm::Value* none_bool_instr = nullptr;
	// no unary op, nop
	if (none_bexpr->unary_boolean_operator == std::nullopt) {
		none_bool_instr = rhs;
	}
	// codegen unary not
	else if (const auto& op = *none_bexpr->unary_boolean_operator; op == "!") {
		none_bool_instr = bc.builder->CreateNot(rhs, "boolnot");
	}
	// unknown op
	else {
		throw std::logic_error{"Unknown unary boolean operator: " + op};
	}

	return none_bool_instr;
}
} // namespace bpy::compiler
