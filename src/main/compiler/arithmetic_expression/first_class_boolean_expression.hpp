#include <llvm/IR/Value.h>

#include "ast/arithmetic_expression/first_class_boolean_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::first_class_boolean_expression::instance&) -> llvm::Value*;
}