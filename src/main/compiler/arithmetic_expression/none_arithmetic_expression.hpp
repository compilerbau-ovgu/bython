#pragma once

#include <ast/arithmetic_expression/none_arithmetic_expression.hpp>
#include <llvm/IR/Value.h>

#include "../constant/numeric.hpp"
#include "arithmetic_expression.hpp"
#include "variable_name.hpp"

namespace bpy::compiler {

auto codegen(const ast::none_arithmetic_expression::instance&) -> llvm::Value*;
}