/// \file

#include "comparison.hpp"

#include <compiler.hpp>

#include "arithmetic_expression.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a comparison expression.
 *
 * Supported operations are Lesser, LesserEquals, GreaterEquals, Greater,
 * NotEqual and Equals.
 * The left and right hand operands will be promoted if needed.
 *
 * @param comp a shared pointer to the comparison expression to be codegenned
 * @return a value that points to the generated instruction
 *
 * @throws std::logic_error if the op is unknown
 */
auto codegen(const ast::comparison::instance& comp) -> llvm::Value*
{
	llvm::Value* lhs = codegen(comp->left);
	llvm::Value* rhs = codegen(comp->right);

	auto& bc = build_context::get_instance();

	auto& builder = *build_context::get_instance().builder;
	auto& c = *build_context::get_instance().context;

	const bool lhs_is_float = lhs->getType()->isFloatTy();
	const bool rhs_is_float = rhs->getType()->isFloatTy();

	if (lhs_is_float != rhs_is_float) {
		// Have to promote lhs
		if (!lhs_is_float) {
			lhs = builder.CreateSIToFP(
				lhs, types::get_ir_type<types::BFloat>()(c), "lhs_to_float");
		}

		// Have to promote rhs
		if (!rhs_is_float) {
			rhs = builder.CreateSIToFP(
				rhs, types::get_ir_type<types::BFloat>()(c), "rhs_to_float");
		}
	}

	llvm::Value* comp_instr = nullptr;
	// GreaterEquals
	if (const auto& op = comp->binary_operator; op == ">=") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpSGE(lhs, rhs, "igeqtmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpOGE(lhs, rhs, "fgegtmp");
		}
	}
	// LesserEquals
	else if (op == "<=") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpSLE(lhs, rhs, "ileqtmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpOLE(lhs, rhs, "flegtmp");
		}
	}
	// Equals
	else if (op == "==") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpEQ(lhs, rhs, "ieqltmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpOEQ(lhs, rhs, "feqltmp");
		}
	}
	// NotEqual
	else if (op == "!=") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpNE(lhs, rhs, "ineqtmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpONE(lhs, rhs, "fnegtmp");
		}
	}
	// Greater
	else if (op == ">") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpSGT(lhs, rhs, "igrttmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpOGT(lhs, rhs, "fgrttmp");
		}
	}
	// Lesser
	else if (op == "<") {
		if (!lhs_is_float) {
			comp_instr = bc.builder->CreateICmpSLT(lhs, rhs, "ilsrtmp");
		}
		else {
			comp_instr = bc.builder->CreateFCmpOLT(lhs, rhs, "flsrtmp");
		}
	}
	// unknown op
	else {
		throw std::logic_error{"Unknown comparison operator: " + op};
	}

	return comp_instr;
}
} // namespace bpy::compiler
