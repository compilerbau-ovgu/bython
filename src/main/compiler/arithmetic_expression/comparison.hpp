#include <llvm/IR/Value.h>

#include "ast/arithmetic_expression/comparison.hpp"

namespace bpy::compiler {
    auto codegen(const ast::comparison::instance& comp) -> llvm::Value*;
}