/// \file

#include "first_class_arithmetic_expression.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR Code for a first class arithmetic expression.
 *
 * @param instance a shared pointer to the first class arithmetic expression to codegenned
 * @return a value that points to the generated instruction
 */
auto codegen(const ast::first_class_arithmetic_expression::instance& instance)
	-> llvm::Value*
{
	llvm::Value* lhs = codegen(instance->value);

	// check if binary expression
	if (instance->binary_operator != std::nullopt) {
		const auto& [op, rhs_expr] = *instance->binary_operator;
		lhs = codegen_binary_op(lhs, op, codegen(rhs_expr));
	}

	return lhs;
}
} // namespace bpy::compiler
