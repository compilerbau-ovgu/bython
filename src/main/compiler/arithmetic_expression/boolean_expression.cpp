/// \file

#include "boolean_expression.hpp"

#include <llvm/IR/Value.h>

#include "../../type_system.hpp"
#include "compiler.hpp"
#include "first_class_boolean_expression.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a boolean expression.
 *
 * Supported operations are logical-and, logical-or, not-equal and equals.
 * The left and right hand operands will be promoted if needed
 *
 * @param bexpr a shared pointer to the boolean expression to be codegenned
 * @return a value that points to the generated instruction
 *
 * @throws std::logic_error if the op is unknown
 */
auto codegen(const ast::boolean_expression::instance& bexpr) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	auto lhs_val = codegen(bexpr->get_lhs());
	const bool lhs_is_float = lhs_val->getType()->isFloatTy();

	// unary boolean expression, convert expression to contextual bool
	if (!bexpr->has_rhs()) {
		llvm::Value* unary_op = nullptr;

		if (!lhs_is_float) {
			llvm::Value* rhs_zero = llvm::ConstantInt::getSigned(
				llvm::IntegerType::get(
					*bc.context, lhs_val->getType()->getPrimitiveSizeInBits()),
				0);

			unary_op =
				bc.builder->CreateICmpNE(lhs_val, rhs_zero, "icontextbool");
		}
		else {
			llvm::Value* rhs_zero =
				llvm::ConstantFP::get(*bc.context, llvm::APFloat(0.0));
			unary_op =
				bc.builder->CreateFCmpONE(lhs_val, rhs_zero, "fcontextbool");
		}

		return unary_op;
	}

	// binary boolean expression
	const auto& op = bexpr->get_op();
	const auto& rhs = bexpr->get_rhs();

	auto rhs_val = codegen(rhs);
	const bool rhs_is_float = rhs_val->getType()->isFloatTy();

	// convert lhs and rhs if needed
	if (lhs_is_float != rhs_is_float) {
		if (lhs_is_float) {
			rhs_val = types::int322Float(rhs_val);
		}
		else {
			lhs_val = types::int322Float(lhs_val);
		}
	}

	llvm::Value* boolean_junctor = nullptr;
	// logical-and
	if (op == "&&") {
		boolean_junctor = bc.builder->CreateAnd(lhs_val, rhs_val, "booland");
	}
	// logical-or
	else if (op == "||") {
		boolean_junctor = bc.builder->CreateOr(lhs_val, rhs_val, "boolor");
	}
	// not-equal
	else if (op == "!=") {
		if (!lhs_is_float) {
			boolean_junctor =
				bc.builder->CreateICmpNE(lhs_val, rhs_val, "iboolneq");
		}
		else {
			boolean_junctor =
				bc.builder->CreateFCmpONE(lhs_val, rhs_val, "fboolneq");
		}
	}
	// equals
	else if (op == "==") {
		if (!lhs_is_float) {
			boolean_junctor =
				bc.builder->CreateICmpEQ(lhs_val, rhs_val, "ibooleq");
		}
		else {
			boolean_junctor =
				bc.builder->CreateFCmpOEQ(lhs_val, rhs_val, "fbooleq");
		}
	}
	else {
		throw std::logic_error{"Unexpected operator: " + op};
	}

	return boolean_junctor;
}
} // namespace bpy::compiler
