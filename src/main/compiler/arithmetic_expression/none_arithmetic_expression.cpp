/// \file

#include "none_arithmetic_expression.hpp"

#include "compiler/expression/call_expression.hpp"
#include "compiler/expression/if_expression.hpp"
#include "compiler/expression/lambda_expression.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a none arithmetic expression.
 * Only the unary minus generates extra code, unary plus is a nop.
 *
 * @param instance  a shared pointer to the none arithmetic expression to codegenned
 * @return a value that points to the generated instruction
 */
auto codegen(const bpy::ast::none_arithmetic_expression::instance& instance)
	-> llvm::Value*
{
	struct value_visitor {
		auto
		operator()(const ast::arithmetic_expression::instance& arithexpr)
		{
			return codegen(arithexpr);
		}
		auto operator()(const ast::call_expression::instance& call)
		{
			return codegen(call);
		}
		auto operator()(const ast::lambda_expression::instance& lambda)
		{
			return codegen(lambda);
		}
		auto operator()(const ast::if_expression::instance& ifexpr)
		{
			return codegen(ifexpr);
		}
		auto operator()(const ast::numeric::instance& num)
		{
			return llvm::dyn_cast<llvm::Value>(codegen(num));
		}
		auto operator()(const ast::variable_name::instance& varname)
		{
			return read(varname);
		}
	};

	// check if prefixed with a unary op
	if (instance->unary_arithmetic_operator &&
		*instance->unary_arithmetic_operator == "-") {
		// Flip sign my multiply with -1
		llvm::Value* rhs = std::visit(value_visitor{}, instance->value);

		const bool rhs_is_float = rhs->getType()->isFloatTy();
		const bool rhs_is_integral = rhs->getType()->isIntegerTy();

		if (!rhs_is_float && !rhs_is_integral) {
			throw std::logic_error{
				"RHS of unary flip is not an arithmetic type!"};
		}

		llvm::Value* minusOne = nullptr;
		auto& c = *build_context::get_instance().context;

		// generate multiplications with appropriate types to flip signs
		if (rhs_is_float) {
			minusOne = llvm::ConstantFP::get(
				types::get_ir_type<types::BFloat>()(c), -1.0);
		}
		else {
			minusOne = llvm::ConstantInt::get(
				types::get_ir_type<types::BInt>()(c), -1);
		}
		const std::string op = "*";
		return codegen_binary_op(minusOne, op, rhs);
	}
	return std::visit(value_visitor{}, instance->value);
}
} // namespace bpy::compiler
