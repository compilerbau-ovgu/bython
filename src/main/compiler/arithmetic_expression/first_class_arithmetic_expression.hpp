#pragma once

#include <ast/arithmetic_expression/first_class_arithmetic_expression.hpp>
#include <llvm/IR/Value.h>

#include "none_arithmetic_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::first_class_arithmetic_expression::instance&)
	-> llvm::Value*;
}