/// \file

#include "arithmetic_expression.hpp"

#include "llvm/IR/Instructions.h"

#include "type_system.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR Code for an arithmetic expression.
 *
 * @param instance a shared pointer to the arithmetic expression to be
 * codegenned
 * @return a value that points to the generated instruction
 */
auto codegen(const bpy::ast::arithmetic_expression::instance& instance)
	-> llvm::Value*
{
	llvm::Value* lhs = codegen(instance->get_lhs());
	if (instance->has_rhs()) {
		lhs = codegen_binary_op(lhs, instance->get_op(),
								codegen(instance->get_rhs()));
	}

	return lhs;
}

/**
 * Generate LLVM IR Code for an arithmetic expression that has a left and right side.
 *
 * Supported operations are modulus, addition, subtraction, multiplication and
 * division. The left and right hand operands will be promoted if needed unless
 * the operation is modulus.
 *
 * @param lhs a value that points to the result of the left hand side
 * @param op a string containing the operation to be generated
 * @param rhs a value that points to the result of the right hand side
 * @return a value that points to the generated instruction
 * 
 * @throws std::logic_error if the left or right hand sides are not arithmetic types
 * @throws std::invalid_argument if the op is unknown
 */
auto codegen_binary_op(llvm::Value* lhs,
					   const std::string& op,
					   llvm::Value* rhs) -> llvm::Value*
{
	auto& builder = *build_context::get_instance().builder;
	auto& c = *build_context::get_instance().context;

	const bool lhs_is_float = lhs->getType()->isFloatTy();
	const bool rhs_is_float = rhs->getType()->isFloatTy();

	const bool lhs_is_arithmetic =
		lhs_is_float || lhs->getType()->isIntegerTy();
	const bool rhs_is_arithmetic =
		rhs_is_float || rhs->getType()->isIntegerTy();

	if (!lhs_is_arithmetic || !rhs_is_arithmetic) {
		throw std::logic_error{
			"Error: Cannot perform arithmetic between two non-arithmetic "
			"types!"};
	}

	// Only support modulus between two integers
	if (op == "%") {
		if (lhs_is_float || rhs_is_float) {
			const auto error_count =
				static_cast<int>(lhs_is_float) + static_cast<int>(rhs_is_float);
			const auto error =
				std::string{"Error: Cannot perform modulus between "} +
				std::to_string(error_count) + " non-integer types!\n";
			throw std::logic_error{error};
		}

		return builder.CreateSRem(lhs, rhs, "modtmp");
	}

	// int and float or float and int
	if (lhs_is_float != rhs_is_float) {
		// Have to promote lhs
		if (!lhs_is_float) {
			lhs = builder.CreateSIToFP(
				lhs, types::get_ir_type<types::BFloat>()(c), "lhs_to_float");
		}

		// Have to promote rhs
		if (!rhs_is_float) {
			rhs = builder.CreateSIToFP(
				rhs, types::get_ir_type<types::BFloat>()(c), "rhs_to_float");
		}
	}

	const bool fp_math_op_required = rhs->getType()->isFloatTy();

	llvm::Value* math_op = nullptr;

	// addition
	if (op == "+") {
		if (fp_math_op_required) {
			math_op = builder.CreateFAdd(lhs, rhs, "faddtmp");
		}
		else {
			math_op = builder.CreateAdd(lhs, rhs, "iaddtmp");
		}
	}
	// subtraction
	if (op == "-") {
		if (fp_math_op_required) {
			math_op = builder.CreateFSub(lhs, rhs, "fsubtmp");
		}
		else {
			math_op = builder.CreateSub(lhs, rhs, "isubtmp");
		}
	}
	// multiplication
	if (op == "*") {
		if (fp_math_op_required) {
			math_op = builder.CreateFMul(lhs, rhs, "fmultmp");
		}
		else {
			math_op = builder.CreateMul(lhs, rhs, "imultmp");
		}
	}
	// division
	if (op == "/") {
		if (fp_math_op_required) {
			math_op = builder.CreateFDiv(lhs, rhs, "fdivtmp");
		}
		else {
			math_op = builder.CreateSDiv(lhs, rhs, "idivtmp");
		}
	}
	// unknown op
	if (math_op == nullptr) {
		throw std::invalid_argument("unknown arithmetic operator");
	}
	// std::cerr << math_op << "\n";
	// std::cerr << compiler::stringify_value(*math_op) << "\n";
	return math_op;
}
} // namespace bpy::compiler
