#pragma once

#include "ast/program.hpp"

namespace bpy::compiler {
auto codegen(const ast::program::instance&) -> bool;
}
