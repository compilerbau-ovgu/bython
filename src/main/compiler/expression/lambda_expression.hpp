#pragma once

#include <llvm/IR/Value.h>

#include "ast/expression/lambda_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::lambda_expression::instance&) -> llvm::Value*;
}
