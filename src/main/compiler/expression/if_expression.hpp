#pragma once

#include <llvm/IR/Instructions.h>

#include "ast/expression/if_expression.hpp"

namespace bpy::compiler {
auto codegen_branches(llvm::Function*,
					  const std::vector<ast::boolean_expression::instance>&,
					  const std::vector<ast::block_expression::instance>&,
					  bool) -> llvm::Value*;

auto codegen(const ast::if_expression::instance&) -> llvm::Value*;
} // namespace bpy::compiler
