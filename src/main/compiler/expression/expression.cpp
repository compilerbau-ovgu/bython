/// \file

#include "compiler/expression/expression.hpp"

#include "compiler/arithmetic_expression/arithmetic_expression.hpp"
#include "compiler/arithmetic_expression/boolean_expression.hpp"
#include "compiler/constant/string.hpp"
#include "compiler/expression/block_expression.hpp"
#include "compiler/expression/call_expression.hpp"
#include "compiler/expression/if_expression.hpp"
#include "compiler/expression/lambda_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::expression::instance& expr) -> llvm::Value*
{
	struct expr_visitor {
		auto operator()(const ast::lambda_expression::instance& lambda_expr)
		{
			return codegen(lambda_expr);
		}

		auto operator()(const ast::if_expression::instance& if_expr)
		{
			return codegen(if_expr);
		}

		auto operator()(const ast::block_expression::instance& block_expr)
		{
			return codegen(block_expr);
		}

		auto operator()(const ast::call_expression::instance& call_expr)
		{
			return codegen(call_expr);
		}

		auto operator()(const ast::arithmetic_expression::instance& math_expr)
		{
			return llvm::dyn_cast<llvm::Value>(codegen(math_expr));
		}

		auto operator()(const ast::bstring::instance& str)
		{
			return llvm::dyn_cast<llvm::Value>(codegen(str));
		}

		auto operator()(const ast::boolean_expression::instance& bool_expr)
		{
			return codegen(bool_expr);
		}
	};

	return std::visit(expr_visitor{}, expr->value);
}

} // namespace bpy::compiler
