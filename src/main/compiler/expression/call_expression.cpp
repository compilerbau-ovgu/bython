/// \file

#include "compiler/expression/call_expression.hpp"

#include <algorithm>
#include <compiler.hpp>

#include "../../stdlib/bython-stdlib.h"
#include "compiler/expression/expression.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR code for a call expression.
 *
 * @param call a shared pointer to the call expression to be codegenned
 * @return a value that points to the return value of the callee
 *
 * @throws std::logic_error if the callee in question does not exist
 * @throws std::logic_error if the types of the arguments passed differ from the
 * parameters of the callee, or if the amount of arguments is wrong
 */
auto codegen(const ast::call_expression::instance& call) -> llvm::Value*
{
	struct fname_visitor {
		auto operator()(const ast::function_name::instance& fname)
		{
			return fname->get_name();
		}
		auto operator()(const ast::constructor::instance& ctor)
		{
			return ctor->get_name() + "::new";
		}
	};

	auto& bc = build_context::get_instance();
	const auto fname = call->get_callee_name();

	llvm::Function* callee = nullptr;

	// check if the callee is a builtin function
	if (bc.builtin_functions.find(fname) != bc.builtin_functions.end()) {
		const auto& callee_ft = bc.builtin_functions.at(fname);
		bc.module->getOrInsertFunction(fname, callee_ft);
		// std::cerr << fname << " is a builtin\n";

		// create the declaration of the builtin if it is the first time being
		// called
		if ((callee = bc.module->getFunction(fname)) == nullptr) {
			std::cerr << "inserting first occurence of " << fname << "\n";
			callee = llvm::Function::Create(
				callee_ft, llvm::Function::ExternalLinkage, fname);
			std::cerr << callee << "\n";
		}
	}
	// check if the callee is a user-declared function
	else if (callee = bc.module->getFunction(fname); callee != nullptr) {
	}

	// not user-declared or builtin
	else {
		throw std::logic_error{"Unable to find function: " + fname};
	}

	// count call expr args
	// 0 if std::nullopt, otherwise size
	const auto call_arg_cnt = call->call_args == std::nullopt
								  ? 0
								  : std::size((*call->call_args)->members);
	const auto fn_arg_cnt = callee->arg_size();

	if (call_arg_cnt != fn_arg_cnt) {
		const auto fn_err =
			fname + " expects " + std::to_string(fn_arg_cnt) + " parameters";
		const auto cl_err = std::string{"Got "} + std::to_string(call_arg_cnt) +
							" arguments instead";

		throw std::logic_error{fn_err + "\n" + cl_err};
	}

	auto args = std::vector<llvm::Value*>(0);

	// codegen the expressions in the argument list
	if (call->call_args != std::nullopt) {
		const auto& argv = *call->call_args;

		// TODO: Why can Clang not deduce codegen(ast::expression::instance&)?
		std::transform(std::begin(argv->members), std::end(argv->members),
					   std::back_inserter(args),
					   [](const auto& expr) { return codegen(expr); });
	}

	const auto args_begin = std::begin(args);
	const auto args_end = std::end(args);

	const auto param_begin = callee->arg_begin();
	const auto param_end = callee->arg_end();

	// Compare function parameters and call expression arguments' types
	const bool matching =
		std::equal(args_begin, args_end, param_begin,
				   [](const llvm::Value* v, const llvm::Argument& a) {
					   return v->getType() == a.getType();
				   });

	// difference between parameters and arguments, error
	if (!matching) {
		auto argv_str = std::string{};
		auto argv_rso = llvm::raw_string_ostream{argv_str};

		std::for_each(args_begin, args_end, [&](const llvm::Value* v) {
			argv_rso << *v->getType() << " + ";
		});
		argv_rso.flush();

		const auto param_str =
			stringify_container(param_begin, param_end, ", ");

		auto err =
			std::string{
				"In call to " + fname +
				": Parameter list differs from the argument list\nExpected: "} +
			param_str + "\nReceived: " + argv_str;

		throw std::logic_error{err};
	}

	// if the function returns void, then do not name the return value (there is none!)
	if (callee->getReturnType()->isVoidTy()) {
		return bc.builder->CreateCall(callee, args);
	}

	// if the function does not return void, store the return value
	return bc.builder->CreateCall(callee, args, fname + "_call");
}
} // namespace bpy::compiler
