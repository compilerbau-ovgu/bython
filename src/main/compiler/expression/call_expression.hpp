#pragma once

#include <llvm/IR/Value.h>

#include "ast/expression/call_expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::call_expression::instance&) -> llvm::Value*;
}