/// \file

#include "lambda_expression.hpp"

#include <iostream>

namespace bpy::compiler {
auto codegen(const ast::lambda_expression::instance& lambda_expr)
	-> llvm::Value*
{
	std::cerr << "WARNING: LAMBDA EXPRESSION CODEGEN IS NOT IMPLEMENTED!\n";
	return nullptr;
}
} // namespace bpy::compiler
