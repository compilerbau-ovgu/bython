#pragma once

#include "ast/expression/block_expression.hpp"
#include <llvm/IR/Value.h>

namespace bpy::compiler {
auto codegen(const ast::block_expression::instance&) -> llvm::Value*;
}