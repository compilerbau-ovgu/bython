#pragma once

#include <llvm/IR/Value.h>

#include "ast/expression/expression.hpp"

namespace bpy::compiler {
auto codegen(const ast::expression::instance&) -> llvm::Value*;
}