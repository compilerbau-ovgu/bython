/// \file

#include "compiler/expression/block_expression.hpp"

#include <compiler.hpp>

#include "compiler/expression/expression.hpp"
#include "compiler/statement/statement.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR code for a block expression.
 *
 * This will call the codegen for each statement stored in \p block, and
 * will return the generated value of the expression, if it is present.
 *
 * @param block a shared pointer to the block expression to be codegenned
 * @return a value that points to the generated value of the expression, or
 * nullptr if there is no expression
 */
auto codegen(const ast::block_expression::instance& block) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	// Set insert point just after the block
	/*llvm::BasicBlock* block_start =
		llvm::BasicBlock::Create(bc.context, "block_start");
	bc.builder.SetInsertPoint(block_start);*/

	// generate all statements
	for (const auto& statement : block->statements) {
		codegen(statement);
	}

	// if expression is present, return its result, else nullptr
	return block->value_expression != std::nullopt
			   ? codegen(*block->value_expression)
			   : nullptr;
}
} // namespace bpy::compiler
