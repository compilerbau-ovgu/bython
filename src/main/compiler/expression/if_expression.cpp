/// \file

#include "compiler/expression/if_expression.hpp"

#include <compiler.hpp>

#include "compiler/arithmetic_expression/boolean_expression.hpp"
// #include "compiler/expression/else_expression.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR code for an If Expression or Statement
 *
 * @param func the function the branching is in
 * @param conds a vector of conditions, one per branch
 * @param bodies a vector of bodies, one per branch, and optionally one more for
 * the else branch
 * @param is_statement true if an if statement is being codegenned, false if an
 * if expression is being codegenned
 * @return nullptr for an if statement, a value pointing to a PHI node
 * containing the result of the branching otherwise
 *
 * @throws std::logic_error if attempting to return a value from a branch in an
 * if statement
 * @throws std::logic_error if attempting a branch in an if expression does not
 * return a value
 */
auto codegen_branches(
	llvm::Function* func,
	const std::vector<ast::boolean_expression::instance>& conds,
	const std::vector<ast::block_expression::instance>& bodies,
	const bool is_statement) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	// else does not have a condition, but if and else if do
	// --> one more body than conditions
	const bool has_else = std::size(conds) != std::size(bodies);

	// initialise phi node in first iteration of loop if it is requried
	llvm::PHINode* phi = nullptr;
	// first type of phi node we have to check against
	llvm::Type* phi_ref_type = nullptr;

	llvm::BasicBlock* then_blck = nullptr; // true branch -> then
	llvm::BasicBlock* else_blck = nullptr; // false branch -> other
	llvm::BasicBlock* merge_blck =
		llvm::BasicBlock::Create(*bc.context, "merge_point"); // merge point

	for (std::size_t branch_nr = 0; branch_nr < std::size(conds); ++branch_nr) {
		// codegen condition
		llvm::Value* cond_val = codegen(conds[branch_nr]);

		// create blocks or then and else cases
		auto then_blck_name =
			llvm::Twine{"then_branch"} + llvm::Twine{branch_nr};
		then_blck =
			llvm::BasicBlock::Create(*bc.context, then_blck_name /*, func*/);

		auto else_blck_name =
			llvm::Twine{"else_branch"} + llvm::Twine{branch_nr};
		else_blck = llvm::BasicBlock::Create(*bc.context, else_blck_name);

		// insert basicblock of true branch into the function
		func->getBasicBlockList().push_back(then_blck);

		// codegen a condition branch,
		// if cond_val maps to true, then jump to then_blck
		// otherwise jump to else_blck
		bc.builder->CreateCondBr(cond_val, then_blck, else_blck);

		// codegen branch
		bc.builder->SetInsertPoint(then_blck);
		llvm::Value* then_block_val = codegen(bodies[branch_nr]);

		// codegen jump to merge point
		bc.builder->CreateBr(merge_blck);
		then_blck = bc.builder->GetInsertBlock();

		// if branch does not return a value (i.e. ended on a statement)
		// and we are codegenning an if expression, raise error
		if (then_block_val == nullptr && !is_statement) {
			throw std::logic_error{
				"Block Expression #" + std::to_string(branch_nr + 1) +
				" in If Expression ends on a statement, not an "
				"expression"};
		}

		// if branch does return a value (i.e. ended on an expression)
		// and we are codegenning an if statement, raise error
		if (then_block_val != nullptr && is_statement) {
			throw std::logic_error{
				"Block Expression #" + std::to_string(branch_nr + 1) +
				" in If Statement ends on an expression, not an "
				"statement"};
		}

		// Expression must return phi node
		if (branch_nr == 0 && !is_statement) {
			phi_ref_type = then_block_val->getType();
			phi = llvm::PHINode::Create(phi_ref_type, std::size(bodies),
										llvm::Twine{"expr_phi"} +
											llvm::Twine{std::size(bodies)});
		}

		if (phi != nullptr) {
			// Check if branch value's type differs from the type the PHI node
			// expects
			if (phi_ref_type != then_block_val->getType()) {
				throw std::logic_error{
					std::string("Else Block returns wrong type. Should be ") +
					stringify_value(*phi_ref_type) + " but was " +
					stringify_value(*then_block_val->getType())};
			}

			// Add branch's path to the PHI
			phi->addIncoming(then_block_val, then_blck);
		}

		// codegen else case
		func->getBasicBlockList().push_back(else_blck);
		bc.builder->SetInsertPoint(else_blck);
	}

	// else_blck cannot be nullptr when has_else is true, because the else
	// cannot exist without the if, meaning the loop will have been executed at
	// least one, therefore else_back cannot be a nullptr. q.e.d
	if (has_else && else_blck != nullptr) {
		/*llvm::BasicBlock* final_else_blck = llvm::BasicBlock::Create(
			bc.context, llvm::Twine{"branch"} +
		   llvm::Twine{std::size(bodies)});*/
		llvm::BasicBlock* final_else_blck = else_blck;
		func->getBasicBlockList().push_back(final_else_blck);

		// move builder and insert IR code for else branch
		bc.builder->SetInsertPoint(final_else_blck);
		llvm::Value* final_else_val = codegen(bodies.back());

		// codegen jump to merge point
		bc.builder->CreateBr(merge_blck);
		final_else_blck = bc.builder->GetInsertBlock();

		if (final_else_val == nullptr && !is_statement) {
			throw std::logic_error{
				"Else Expression in If Expression ends on a statement, not an "
				"expression"};
		}

		if (final_else_val != nullptr && is_statement) {
			throw std::logic_error{
				"Else Statement in If Statement ends on an expression, not an "
				"statement"};
		}

		// add else branch's value to the PHI
		if (phi != nullptr) {
			phi->addIncoming(final_else_val, final_else_blck);
		}
	}
	// case for if without else -> branch to merge point
	if (is_statement && std::size(bodies) == 1 && std::size(conds) == 1) {
		bc.builder->SetInsertPoint(else_blck);
		bc.builder->CreateBr(merge_blck);
	}

	func->getBasicBlockList().push_back(merge_blck);
	bc.builder->SetInsertPoint(merge_blck);

	// add PHI to LLVM IR code
	if (phi != nullptr) {
		return bc.builder->Insert(phi, llvm::Twine{"phi"} +
										   llvm::Twine{std::size(bodies)});
	}
	// can be nullptr in case of if statement
	return phi;
}

/**
 * Generate LLVM IR code for an If Expression
 *
 * @param if_expr a shared pointer to the If Expression to be codegenned
 * @return a value pointing to a PHI node containing the result of the branching
 */
auto codegen(const ast::if_expression::instance& if_expr) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	// function we are currently inserting into
	llvm::Function* parent = bc.builder->GetInsertBlock()->getParent();

	// gather boolean expressions for branches
	const auto conds = if_expr->collect_conditions();
	// gather branch contents
	const auto bodies = if_expr->collect_bodies();

	// std::cerr << "gathered " << std::size(conds) << " conditions\n";
	// std::cerr << "gathered " << std::size(bodies) << " block_exprs\n";

	// generate LLVM IR Code
	return codegen_branches(parent, conds, bodies, /*is_statement=*/false);
}
} // namespace bpy::compiler
