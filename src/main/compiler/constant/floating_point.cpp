/// \file

#include "floating_point.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR code for a float.
 *
 * @param floatValue a shared pointer to the float to be codegenned
 * @return a value that points to the generated float
 */
auto codegen(const ast::floating_point::instance& floatValue)
	-> llvm::ConstantFP*
{
	return llvm::ConstantFP::get(*build_context::get_instance().context,
								 llvm::APFloat(floatValue->value));
}
} // namespace bpy::compiler
