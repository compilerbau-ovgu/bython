/// \file

#include "constant.hpp"

#include <sstream>

#include "boolean_constant.hpp"
#include "numeric.hpp"
#include "string.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a constant.
 *
 * Strings, integers and floats are all constants.
 *
 * @param constant a shared pointer to the constant to be codegenned
 * @return a value that points to the generated constant
 */
auto codegen(const ast::constant::instance& constant) -> llvm::Constant*
{
	struct constant_visitor {
		auto operator()(const ast::bstring::instance& string) -> decltype(auto)
		{
			return codegen(string);
		}

		auto operator()(const ast::numeric::instance& numeric) -> decltype(auto)
		{
			return codegen(numeric);
		}

		auto operator()(const ast::boolean_constant::instance& boolean)
			-> decltype(auto)
		{
			return codegen(boolean);
		}
	};

	return std::visit(constant_visitor{}, constant->value);
}
} // namespace bpy::compiler*/