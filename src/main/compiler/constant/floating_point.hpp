#pragma once

#include "llvm/IR/Constants.h"

#include <limits>

#include "ast/constant/floating_point.hpp"
#include "ast/constant/integer.hpp"
#include "compiler.hpp"
#include "type_system.hpp"

namespace bpy::compiler {

auto codegen(const ast::floating_point::instance&) -> llvm::ConstantFP*;

}
