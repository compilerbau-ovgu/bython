/// \file

#include "boolean_constant.hpp"

#include "compiler.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a boolean constant (true, false).
 *
 * @param boolean a shared pointer to the boolean constant to be codegenned
 * @return a value that points to the generated boolean value
 */
auto codegen(const ast::boolean_constant::instance& boolean) -> llvm::Constant*
{
	auto& bc = *build_context::get_instance().context;

	return boolean->value ? llvm::ConstantInt::getTrue(bc)
						  : llvm::ConstantInt::getFalse(bc);
}
} // namespace bpy::compiler
