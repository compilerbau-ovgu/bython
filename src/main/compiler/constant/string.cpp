/// \file

#include "string.hpp"

#include <limits>
#include <llvm/Support/Casting.h>
#include <vector>

#include "compiler.hpp"

namespace bpy::compiler {
// adapted from http://ugweb.cs.ualberta.ca/~c415/vcalc/vcalcse9.html

/**
 * Generate LLVM IR code for a string
 *
 * @param bstring a shared pointer to the string to be codegenned
 * @return a value that points to the generated string
 */
auto codegen(const ast::bstring::instance& bstring) -> llvm::Constant*
{
	using string_char = decltype(bstring->chars)::value_type;

	// Initialise char array from string
	build_context& bc = build_context::get_instance();
	// const auto char_type =
	//	llvm::IntegerType::get(*bc.context, sizeof(string_char));

	/*const auto string_type =
		llvm::ArrayType::get(char_type, std::size(bstring->chars));*/

	// Note: If we decide to do this, should the codegen for bstring check if
	// the declaration already exists at the start of the codegen?
	const auto gname = ".str" + bstring->chars;

	const auto gdata = llvm::ConstantDataArray::getString(*bc.context,
														  bstring->chars,
														  /*AddNull*/ true);

	// Register the declaration statement in the module
	// Note: Name string declaration after string content?
	// Note: Would help avoid duplicates?
	llvm::Constant* text =
		bc.module->getOrInsertGlobal(gname, gdata->getType());
	const auto gdata_loc = llvm::cast<llvm::GlobalVariable>(text);

	gdata_loc->setInitializer(gdata);

	// Cast to appropriate type and return
	return llvm::ConstantExpr::getBitCast(
		gdata_loc, llvm::Type::getInt8PtrTy(*bc.context));
}
} // namespace bpy::compiler
