#pragma once

#include "llvm/IR/Constants.h"

#include "ast/constant/constant.hpp"
#include "compiler.hpp"
#include "type_system.hpp"

namespace bpy::compiler {

auto codegen(const ast::constant::instance&) -> llvm::Constant*;

}
