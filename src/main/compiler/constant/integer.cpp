/// \file

#include "integer.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR code for an integer
 *
 * @param int_ptr a shared pointer to the integer to be codegenned
 * @return a value thats to the generated integer
 */
auto codegen(const ast::integer::instance& int_ptr) -> llvm::ConstantInt*
{
	// NOTE: ::digits does not! count the sign-bit, so add one
	// NOTE: see Issue #53
	auto llvm_int_type =
		llvm::IntegerType::get(*build_context::get_instance().context,
							   std::numeric_limits<types::BInt>::digits + 1);

	return llvm::ConstantInt::getSigned(llvm_int_type, int_ptr->value);
}
} // namespace bpy::compiler
