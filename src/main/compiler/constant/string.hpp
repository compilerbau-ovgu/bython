#pragma once

#include "llvm/IR/Constants.h"
#include "llvm/IR/GlobalVariable.h"

#include "ast/constant/string.hpp"
#include "compiler.hpp"

namespace bpy::compiler {

auto codegen(const ast::bstring::instance&) -> llvm::Constant*;

}
