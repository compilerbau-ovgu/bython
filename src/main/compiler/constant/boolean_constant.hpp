#pragma once

#include <llvm/IR/Constant.h>

#include "ast/constant/boolean_constant.hpp"

namespace bpy::compiler {

auto codegen(const ast::boolean_constant::instance&) -> llvm::Constant*;

}
