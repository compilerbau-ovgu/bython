#pragma once

#include "llvm/IR/Constants.h"

#include <limits>
#include <variant>

#include "ast/constant/floating_point.hpp"
#include "ast/constant/integer.hpp"
#include "ast/constant/numeric.hpp"
#include "compiler.hpp"
#include "type_system.hpp"

namespace bpy::compiler {

auto codegen(const ast::numeric::instance& num_ins) -> llvm::Constant*;

} // namespace bpy::compiler
