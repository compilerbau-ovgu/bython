/// \file

#include "numeric.hpp"

#include "floating_point.hpp"
#include "integer.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR code for a numeric value.
 *
 * Possible numeric values are integers and floats.
 *
 * @param num_ins a shared pointer to the numeric value to be codegenned
 * @return a value that points to the generated numeric value
 */
auto codegen(const ast::numeric::instance& num_ins) -> llvm::Constant*
{
	struct numeric_visit {
		auto operator()(const ast::integer::instance& integer) -> decltype(auto)
		{
			return llvm::cast<llvm::Constant>(codegen(integer));
		}

		auto operator()(const ast::floating_point::instance& fp)
			-> decltype(auto)
		{
			return llvm::cast<llvm::Constant>(codegen(fp));
		}
	};

	return std::visit(numeric_visit{}, num_ins->value);
}
} // namespace bpy::compiler
