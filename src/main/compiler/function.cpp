/// \file

#include "function.hpp"

#include "llvm/IR/Verifier.h"

#include <compiler.hpp>

namespace bpy::compiler {

/**
 * Generate LLVM IR code for a Function
 *
 * @param ast_function a shared pointer to the Function to be codegenned
 * @return a pointer to the codegenned Function
 *
 * @throws std::runtime_error if the function prototype could not be generated
 * @throws std::logic_error if the function has a return instruction but is
 * meant to return void
 * @throws std::logic_error if the function has no return instruction but is
 * meant to return something
 * @throws std::logic_error if llvm::verifyModule finds a codegen error
 */
auto codegen(const ast::function::instance& ast_function) -> llvm::Function*
{
	auto& bc = build_context::get_instance();
	// First, check for an existing function from a previous 'extern'
	// declaration.
	llvm::Function* function =
		bc.module->getFunction(ast_function->get_function_name()->get_name());

	llvm::FunctionType* FT = nullptr;

	// generate the prototype
	if (function == nullptr) {
		// Function has parameters
		if (ast_function->has_args()) {
			auto fda_types = std::vector<llvm::Type*>{};
			for (std::size_t i = 0; i < ast_function->arg_size(); ++i) {
				const auto& arg = ast_function->get_args()[i];
				fda_types.emplace_back(
					types::get_type_from_identifier(arg->get_type()));
			}
			// NOTE: When using std::transform, the program segfaults
			/*std::transform(std::begin(ast_function->get_args()),
			std::end(ast_function->get_args()), std::back_inserter(fda_types),
			[](const auto& arg) { return
			types::get_type_from_identifier(arg->get_type());
			});*/
			FT = llvm::FunctionType::get(types::get_type_from_identifier(
											 ast_function->get_return_type()),
										 fda_types, false);
			function = llvm::Function::Create(
				FT, llvm::Function::ExternalLinkage,
				ast_function->get_function_name()->get_name(), *bc.module);
			// Set names for all arguments.
			unsigned idx = 0;
			for (auto& arg : function->args()) {
				arg.setName(ast_function->get_args()[idx++]->get_var_name());
			}
		}
		// Void Function
		else {
			llvm::Type* type = llvm::Type::getVoidTy(*bc.context);
			FT = llvm::FunctionType::get(llvm::Type::getVoidTy(*bc.context),
										 type, false);
			function = llvm::Function::Create(
				FT, llvm::Function::ExternalLinkage,
				ast_function->get_function_name()->get_name(), *bc.module);
		}
	}

	// Changed from function to FT even though when one is not null, so is the
	// other
	// Reason: the linter cannot derive the validity of FT (i.e. its address)
	// when the check function == nullptr is executed
	if (FT == nullptr || function == nullptr) {
		throw std::runtime_error(
			"Error while generating Function. Couldn't create Function "
			"Prototype");
	}

	// Create a new basic block to start insertion into.
	llvm::BasicBlock* BB =
		llvm::BasicBlock::Create(*bc.context, "entry", function);
	bc.builder->SetInsertPoint(BB);

	// Record the function arguments in the NamedValues map.
	bc.namedValues.clear();
	for (auto& arg : function->args()) {
		bc.namedValues[arg.getName()] = &arg;
	}
	// NOTE: If the block expression ends on a statement, the
	// codegen shall return nullptr
	llvm::Value* body = codegen(ast_function->block);

	// generate main function return value if none provided by the user
	// bool is_main = ast_function->get_function_name()->get_name() == "main";
	const bool void_ret = body == nullptr && FT->getReturnType()->isVoidTy();
	const bool type_ret =
		body != nullptr && FT->getReturnType() == body->getType();

	if (const bool correct_return = void_ret || type_ret; !correct_return) {
		/*if (is_main) {
			auto& instr = BB->back();
			if (!instr.isTerminator()) {
				auto llvm_int_type = llvm::IntegerType::get(
					bc.context, std::numeric_limits<types::BInt>::digits + 1);
				bc.builder.CreateRet(
					llvm::ConstantInt::getSigned(llvm_int_type, 0));
			}
		}*/
		// else {
		auto type_str = std::string{};
		auto rso = llvm::raw_string_ostream{type_str};
		FT->getReturnType()->print(rso);

		// body == nullptr -> no return, but decl wants returns
		// body != nullptr -> return, but decl says void
		auto err_prefix =
			body != nullptr ? "Function body has no value, but return type is "
							: "Function body wishes to return a value, but the "
							  "return type is ";

		throw std::logic_error{function->getName().str() + ": " +
							   std::string{err_prefix} + rso.str()};
		//}
	}

	// codegen return instruction
	if (body != nullptr) {
		bc.builder->CreateRet(body);
	}
	// codegen return instruction with no value
	else {
		bc.builder->CreateRetVoid();
	}

	// check that the codegen does not contain errors
	auto errors = std::string{};
	if (auto err_stream = llvm::raw_string_ostream{errors};
		llvm::verifyModule(*bc.module, &err_stream)) {
		err_stream.flush();

		std::cerr << stringify_value(*bc.module);
		std::cerr << errors << "\n";

		throw std::logic_error{"llvm::verifyModule failed!"};
	}
	return function;
}
} // namespace bpy::compiler
