#include "compiler/statement/else_statement.hpp"

#include <compiler.hpp>
#include <variant>

#include "ast/statement/if_statement.hpp"
#include "compiler/arithmetic_expression/boolean_expression.hpp"
#include "compiler/statement/if_statement.hpp"

namespace bpy::compiler {
auto codegen(const ast::else_statement::instance& else_stmt,
			 llvm::Function* const parent,
			 llvm::BasicBlock* const this_branch,
			 llvm::BasicBlock* const merge,
			 std::size_t branch_count) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	// else if branch, identical to codegenning normal if branch
	if (const auto else_if_ptr =
			std::get_if<ast::if_statement::instance>(&else_stmt->value);
		else_if_ptr != nullptr) {
		const auto& else_if = *else_if_ptr;

		llvm::Value* condition = codegen(else_if->ruling);
		return codegen_branch(else_if, condition, parent, this_branch, merge,
							  branch_count);
	}

	// else branch
	if (const auto else_caseless_ptr =
			std::get_if<ast::block_expression::instance>(&else_stmt->value);
		else_caseless_ptr != nullptr) {
		const auto& else_caseless = *else_caseless_ptr;

		// else Branch
		llvm::BasicBlock* else_branch =
			llvm::BasicBlock::Create(*bc.context, "final_else", parent);
		parent->getBasicBlockList().push_back(else_branch);

		// codegen else branch
		bc.builder->SetInsertPoint(else_branch);

		// discard block expression return value
		(void) codegen(else_caseless);

		// Jump to merge point
		bc.builder->CreateBr(merge);
		return nullptr;
	}

	throw std::logic_error{"Unexpected class in else statement"};
}
} // namespace bpy::compiler
