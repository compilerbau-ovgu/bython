#pragma once

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Value.h>

#include "ast/statement/if_statement.hpp"

namespace bpy::compiler {

auto codegen(const ast::if_statement::instance&) -> llvm::Value*;

auto codegen_branch(const ast::if_statement::instance&,
					llvm::Value*,
					llvm::Function*,
					llvm::BasicBlock*,
					llvm::BasicBlock*,
					std::size_t) -> llvm::Value*;

} // namespace bpy::compiler