#pragma once

#include <llvm/IR/Function.h>
#include <llvm/IR/Value.h>

#include "ast/statement/else_statement.hpp"

namespace bpy::compiler {
auto codegen(const ast::else_statement::instance&,
			 llvm::Function*,
			 llvm::BasicBlock*,
			 llvm::BasicBlock*,
			 std::size_t) -> llvm::Value*;
} // namespace bpy::compiler