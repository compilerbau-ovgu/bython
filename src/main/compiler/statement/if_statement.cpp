/// \file

#include "compiler/statement/if_statement.hpp"

#include <compiler.hpp>

#include "compiler/arithmetic_expression/boolean_expression.hpp"
#include "compiler/expression/if_expression.hpp"
#include "compiler/statement/else_statement.hpp"
#include "compiler/statement/if_statement.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR code for an If Expression
 *
 * @param if_stmt a shared pointer to the If Statement to be codegenned
 * @return nullptr
 */
auto codegen(const ast::if_statement::instance& if_stmt) -> llvm::Value*
{
	auto& bc = build_context::get_instance();

	llvm::Function* parent = bc.builder->GetInsertBlock()->getParent();

	const auto conds = if_stmt->collect_conditions();
	const auto bodies = if_stmt->collect_bodies();

	// std::cerr << "gathered " << std::size(conds) << " conditions\n";
	// std::cerr << "gathered " << std::size(bodies) << " block_exprs\n";

	return codegen_branches(parent, conds, bodies, /*is_statement=*/true);
}
} // namespace bpy::compiler
