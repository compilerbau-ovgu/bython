#pragma once

#include <llvm/IR/Value.h>

#include "ast/statement/let_statement.hpp"

namespace bpy::compiler {
auto codegen(const ast::let_statement::instance&) -> llvm::Value*;
}