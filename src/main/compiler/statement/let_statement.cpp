/// \file

#include "compiler/statement/let_statement.hpp"

#include <compiler.hpp>

#include "type_system.hpp"

namespace bpy::compiler {

/**
 * Generate LLVM IR Code for a Let Statement
 *
 * @param let a shared pointer to the Let Statement to codegenned
 * @return a value pointing to the generated store instruction
 *
 * @throws std::logic_error if the type of the right hand side does not match
 * the type hint of the left hand side
 */
auto codegen(const ast::let_statement::instance& let) -> llvm::Value*
{
	auto& bc = build_context::get_instance();
	const auto& lhs_name = let->get_var_name();

	// cannot declare pre-existing variable name
	if (bc.namedValues.find(lhs_name) != std::end(bc.namedValues)) {
		throw std::logic_error{std::string{"Redeclaring variable: "} +
							   lhs_name};
	}

	// codegen the RHS of the statement
	llvm::Value* rhs = codegen(let->right_hand);

	// conditionally check correctness of assignment
	if (let->has_type_hint()) {
		if (types::get_type_from_identifier(let->get_actual_type_name()) !=
			rhs->getType()) {
			throw std::logic_error{
				std::string{"Type mismatch: LHS has type hint "} +
				let->get_type_name() + ", but RHS has " +
				stringify_value(*rhs->getType())};
		}
	}

	// rhs = compiler::store(lhs_name, rhs);

	// codegen store and load instructions
	llvm::AllocaInst* variable_value = bc.builder->CreateAlloca(
		rhs->getType(), nullptr, llvm::Twine(lhs_name));
	llvm::StoreInst* store = bc.builder->CreateStore(rhs, variable_value);
	bc.namedValues[lhs_name] = bc.builder->CreateLoad(variable_value);

	// return store instruction
	return store;
	// return rhs;
}
} // namespace bpy::compiler
