#pragma once

#include "ast/statement/statement.hpp"

#include <llvm/IR/Value.h>

namespace bpy::compiler {
    auto codegen(const ast::statement::instance&) -> llvm::Value*;
}