/// \file

#include "compiler/statement/statement.hpp"

#include "compiler/expression/block_expression.hpp"
#include "compiler/expression/call_expression.hpp"
#include "compiler/statement/if_statement.hpp"
#include "compiler/statement/let_statement.hpp"

namespace bpy::compiler {
/**
 * Generate LLVM IR Code for a Statement
 *
 * @param statement a shared pointer to the Statement to be codegenned
 * @return a value pointing to a Store Instruction in the case of a let
 * statement, nullptr otherwise
 */
auto codegen(const ast::statement::instance& statement) -> llvm::Value*
{
	struct codegen_visitor {
		auto operator()(const ast::if_statement::instance& ifs) -> llvm::Value*
		{
			return codegen(ifs);
		}
		auto operator()(const ast::block_expression::instance& block)
			-> llvm::Value*
		{
			// Block Expression can return an expression, so warn that it will
			// be discarded in this context
			if (const auto ptr = codegen(block); ptr != nullptr) {
				std::cerr << "WARNING: Block Expression with an expression as "
							 "final value, will be discarded\n";
			}
			return nullptr;
		}
		auto operator()(const ast::call_expression::instance& call)
			-> llvm::Value*
		{
			// discard return value of call expression
			(void) codegen(call);
			return nullptr;
		}
		auto operator()(const ast::let_statement::instance& let) -> llvm::Value*
		{
			// return StoreInst to controller
			return codegen(let);
		}
	};

	return std::visit(codegen_visitor{}, statement->statements);
}
} // namespace bpy::compiler