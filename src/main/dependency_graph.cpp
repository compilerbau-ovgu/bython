/// \file

#include "dependency_graph.hpp"

#include <random>

#include "ostream_overloaded.hpp"

namespace bpy {
/**
 * Register dependencies between code elements
 *
 * @param program a shared pointer to the program whose dependencies are to be
 * registered
 */
dependency_graph::dependency_graph(const ast::program::instance& program)
{
	// const auto& program_dependencies = program->dependencies();
	const std::vector<ast::function::instance> functions =
		bpy::ast::get<ast::function>(program);

	// Enter dependencies
	for (const ast::program::program_type& record_function :
		 program->contents) {
		base_inst holder;
		base_inst name;
		if (const ast::function::instance* function_ptr =
				std::get_if<ast::function::instance>(&record_function)) {
			const ast::function::instance& function = *function_ptr;
			holder = function;
			name = function->get_function_name();
		}
		else if (const ast::record::instance* record_ptr =
					 std::get_if<ast::record::instance>(&record_function)) {
			const ast::record::instance& record = *record_ptr;
			holder = record;
			name = record->get_type();
		}

		if (holder) {
			for (const base_inst& record_function_dep :
				 holder->dependencies()) {
				add_connection(name, record_function_dep);
			}
		}
	}
}

/**
 * Register relationship that represents \p from depending on \p o
 *
 * @param from the dependee
 * @param to the dependency
 */
auto dependency_graph::add_connection(const ast::base::instance& from,
									  const ast::base::instance& to) -> void
{
	// enter from and to if they are not yet present in the map
	if (!contains(from)) {
		graph_[from] = base_inst_set{};
	}
	if (!contains(to)) {
		graph_[to] = base_inst_set{};
	}

	graph_[from].insert(to);
}

/**
 * Check if an AST class has been registered in the dependency graph
 *
 * @param ast check if this class be has registered
 * @return true if \p ast has been registered, false otherwise
 */
auto dependency_graph::contains(const ast::base::instance& ast) const -> bool
{
	return graph_.find(ast) != graph_.end();
}

/**
 * Generate topologically sorted order of codgennable elements, so that during
 * compilation, all functions and types can be resolved in the correct order
 *
 * @return a vector containing the topologically sorted AST classes
 */
auto dependency_graph::topological_order() const
	-> std::vector<ast::base::instance>
{
	// Implements Kane's Algorithm found here:
	// https://en.wikipedia.org/wiki/Topological_sorting#Kahn's_algorithm

	// Requires a copy of the graph
	base_inst_map graph_copy = base_inst_map{graph_};

	// empty list that will contain the sorted elements
	auto order = std::vector<base_inst>{};

	auto from_nodes = std::vector<base_inst>{};
	auto to_nodes = base_inst_set{};

	std::transform(graph_copy.begin(), graph_copy.end(),
				   std::back_inserter(from_nodes),
				   [](const auto& kvp) { return kvp.first; });

	std::for_each(graph_copy.begin(), graph_copy.end(), [&](const auto& kvp) {
		const base_inst_set& children_set = kvp.second;
		std::for_each(
			children_set.begin(), children_set.end(),
			[&](const auto& base_inst) { to_nodes.emplace(base_inst); });
	});

	// set of all nodes with no incoming edge
	// Collect all from_nodes whitch is not dependent on
	auto sources = base_inst_set{};

	// std::set_difference requires the first pair of iterators belong to a
	// sorted collection
	std::sort(from_nodes.begin(), from_nodes.end());
	// std::set_difference(from_nodes.begin(), from_nodes.end(),
	// to_nodes.begin(), 					to_nodes.end(),
	// std::inserter(sources, sources.begin()));

	const auto main_function_it = std::find_if(
		std::begin(from_nodes), std::end(from_nodes),
		[](const base_inst& inst) {
			if (const ast::function_name::instance& function =
					std::dynamic_pointer_cast<ast::function_name>(inst)) {
				return function->get_name() == "main";
			}
			return false;
		});
	if (main_function_it == std::end(from_nodes)) {
		throw std::invalid_argument{"main function not found!"};
	}

	sources.emplace(*main_function_it);

	// while sources is non-empty
	while (!sources.empty()) {
		// remove a node current_node from sources, add current_node to the tail
		// of order
		// std::sample(sources.begin(), sources.end(),
		// std::back_inserter(order), 			1,
		// std::mt19937{std::random_device{}()});
		order.push_back((*(sources.begin())));
		const base_inst& current_node = order.back();
		sources.erase(current_node);

		// for each node m with an edge e from current_node to m do
		// base_inst_set must be a copy, because .at(current_node).erase(m) will
		// invalidate iterators
		const base_inst_set n_children = graph_copy.at(current_node);
		for (const base_inst& m : n_children) {
			// remove edge e from the graph

			// Returns the amount of values erased!
			// remove edge e from the graph
			graph_copy.at(current_node).erase(m);
			if (ast::identical_to()(m, current_node)) {
				continue;
			}
			// if m has no incoming edges
			// i.e. if m is not a base_inst
			// i.e. m is not in any hashsets
			const bool m_is_not_child = std::all_of(
				graph_copy.begin(), graph_copy.end(), [&](const auto& kvp) {
					return kvp.second.find(m) == kvp.second.end() ||
						   ast::identical_to()(m, kvp.first);
				});
			if (m_is_not_child) {
				// insert m into sources
				sources.insert(m);
			}
		}
	}

	// topologically sorted
	std::reverse(order.begin(), order.end());
	return order;
}


auto operator<<(std::ostream& os, const dependency_graph& dep) -> std::ostream&
{
	os << "Dependency Graph: \n" << dep.graph_;
	return os;
}


auto operator<<(std::ostream& os, const dependency_graph::base_inst_map& dep)
	-> std::ostream&
{
	for (const auto& knot : dep) {
		os << knot.first << "\t->\t";
		for (const auto& child : knot.second) {
			os << child << ", ";
		}
		os << std::endl;
	}
	return os;
}
} // namespace bpy
