#pragma once

#include <ast/type_name.hpp>
#include <cstdint>
#include <llvm/IR/Constants.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Type.h>
#include <memory>
#include <string>

namespace bpy::types {
using BInt = std::int_least32_t;
using BFloat = float;
using BBool = bool;

auto string2int32(const std::string&) -> BInt;

auto string2Float(const std::string&) -> BFloat;

auto string2bool(const std::string&) -> BBool;

auto int322Float(llvm::Value*) -> llvm::Value*;

template <typename BythonType>
auto get_ir_type() -> decltype(auto)
{
	// Have to return lambdas because including this file in compiler.hpp
	// here causes cyclic dependency
	if constexpr (std::is_same_v<BythonType, BInt>) {
		return [](llvm::LLVMContext& c) { return llvm::Type::getInt32Ty(c); };
	}
	else if constexpr (std::is_same_v<BythonType, BFloat>) {
		return [](llvm::LLVMContext& c) { return llvm::Type::getFloatTy(c); };
	}
	else if constexpr (std::is_same_v<BythonType, BBool>) {
		return [](llvm::LLVMContext& c) {
			return llvm::ConstantInt::getTrue(c)->getType();
		};
	}
	else if constexpr (std::is_same_v<BythonType, void>) {
		return [](llvm::LLVMContext& c) { return llvm::Type::getVoidTy(c); };
	}
	else if constexpr (std::is_pointer_v<BythonType>) {
		return [](llvm::LLVMContext& c) { return llvm::Type::getInt8PtrTy(c); };
	}
	else {
		static_assert("Unable to map type at compile time!");
	}
}

auto get_type_from_identifier(const ast::type_name::instance&) -> llvm::Type*;
} // namespace bpy::types
