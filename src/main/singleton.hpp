#pragma once

#include <memory>

namespace bpy::crtp {

// https://stackoverflow.com/questions/51974670/implementing-singleton-with-crtp
template <class Derived>
class singleton {
  public:
	static Derived& get_instance()
	{
		static Derived* ptr = nullptr;

		if (ptr == nullptr) {
			ptr = new Derived{};
		}

		return *ptr;
	}

	virtual ~singleton()
	{
	}

	singleton(singleton const&) = delete;
	singleton& operator=(singleton const&) = delete;

  protected:
	singleton() = default;
};

} // namespace bpy::crtp
