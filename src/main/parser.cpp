/// \file

#include "parser.hpp"

#include <fstream>
#include <iterator>
#include <string>

static auto read_file(const std::string& filepath) -> std::string
{
	std::ifstream ifs;
	std::cerr << " Opening ifstream: " << filepath << " ";
	ifs.open(filepath);
	if (!ifs.good()) {
		throw std::invalid_argument{"Unable to open " + filepath};
	}
	std::cerr << "Success!\n";

	std::string file_string((std::istreambuf_iterator<char>(ifs)),
							std::istreambuf_iterator<char>());

	ifs.close();

	return file_string;
}

namespace bpy::parsing {
auto parse_bython(const std::string& bython_file) -> std::shared_ptr<peg::Ast>
{
	const std::string bython_grammar = read_file(GRAMMAR_PATH);
	const std::string bython_code = read_file(bython_file);

	std::cerr << "Parsing: ";
	auto parser = peg::parser{};
	parser.log = [](std::size_t line, std::size_t column,
					const std::string& message) {
		std::cerr << line << ":" << column << ": " << message << "\n";
	};

	if (auto ok = parser.load_grammar(bython_grammar.c_str()); !ok) {
		std::cerr << "warning: unable to load grammar!\n";
		std::exit(1);
	}

	else {
		parser.enable_ast();

		if (auto basic = std::shared_ptr<peg::Ast>{};
			parser.parse(bython_code.c_str(), basic)) {
			std::cerr << "Success!\n";
			return basic;
		}

		std::cerr << "failed to parse bython code\n";
		std::exit(1);
	}
}
} // namespace bpy::parsing
