/// \file

#include "type_system.hpp"

#include <ast/type_name.hpp>
#include <charconv>
#include <compiler.hpp>
#include <stdexcept>

namespace bpy::types {
auto string2int32(const std::string& bint_str) -> BInt
{
	BInt result = {};

	const auto [/*unused*/ _, e] =
		std::from_chars(bint_str.data(),							// NOLINT
						bint_str.data() + bint_str.size(), result); // NOLINT

	if (e == std::errc{}) {
		return result;
	}

	throw std::invalid_argument{"Unable to convert " + bint_str + " to a BInt"};
}

auto string2Float(const std::string& bfloat_str) -> BFloat
{
	// std::from_chars would be more desirable, but neither Clang or GCC seem to
	// have implemented it
	return std::strtof(bfloat_str.c_str(), nullptr);
}

auto string2bool(const std::string& bool_str) -> BBool
{
	return bool_str == "true";
}

auto int322Float(llvm::Value* bfloat_bint) -> llvm::Value*
{
	auto& builder = *compiler::build_context::get_instance().builder;
	auto& c = *compiler::build_context::get_instance().context;

	return builder.CreateSIToFP(
		bfloat_bint, types::get_ir_type<types::BFloat>()(c), "int_to_float");
}
auto get_type_from_identifier(const ast::type_name::instance& ident)
	-> llvm::Type*
{
	auto& bc = compiler::build_context::get_instance();
	llvm::Type* type_ptr = nullptr;

	const auto& type_string = ident->get_name();

	// LLVM does not support llvm::Type::getVoidPtrTy or similar
    // so just use llvm::Type::getInt8PtrTy
	if (type_string == "String" || type_string == "List") {
		type_ptr = llvm::Type::getInt8PtrTy(*bc.context);
	}
	else if (type_string == "Int") {
		type_ptr = llvm::Type::getInt32Ty(*bc.context);
	}
	else if (type_string == "Bool") {
		type_ptr = llvm::ConstantInt::getTrue(*bc.context)->getType();
	}
	else if (type_string == "Float") {
		type_ptr = llvm::Type::getFloatTy(*bc.context);
	}
	else if (type_string == "Void") {
		type_ptr = llvm::Type::getVoidTy(*bc.context);
	}
	else {
		throw std::runtime_error("Trying to convert invalid type: " +
								 ident->get_name());
	}
	return type_ptr;
}
} // namespace bpy::types
