#pragma once

namespace bpy::ast {

enum class RuleName {
	Program,
	Function,
	Record,
	RecordBody,
	RecordMember,
	FuncDeclArgs,
	FuncDeclArg,
	ReturnType,
	BlockExpression,
	Statement,
	Expression,
	LetStatement,
	IfStatement,
	ElseStatement,
	IfExpression,
	ElseExpression,
	CallExpression,
	CallArgs,
	LambdaExpression,
	BooleanExpression,
	FirstClassBooleanExpression,
	NoneBooleanExpression,
	Comparison,
	ArithmeticExpression,
	FirstClassArithmeticExpression,
	NoneArithmeticExpression,
	FunctionName,
	Constructor,
	TypeName,
	VariableName,
	Identifier,
	Constant,
	NumericConstant,
	FloatConstant,
	IntegerConstant,
	BooleanConstant,
	StringConstant,
};

auto rule_name_to_str(const RuleName) -> const char*;

} // namespace bpy::ast
