#pragma once

#include <algorithm>
#include <mutex>
#include <peglib.h>
#include <string>
#include <thread>
#include <type_traits>
#include <unordered_set>
#include <utility>
#include <vector>

#include "rules.hpp"

namespace bpy::ast {

class base {
  public:
	using instance = std::shared_ptr<base>;

	base() = delete;
	virtual ~base() = default;

	explicit base(RuleName);

	virtual auto dependencies() const
		-> const std::unordered_set<base::instance>& = 0;

	virtual auto hash_impl() const noexcept -> std::size_t = 0;

	RuleName rule_name;

  protected:
	struct constructor_access {
	};

	mutable std::once_flag dependency_cache_flag;
	mutable std::unordered_set<base::instance> dependencies_;
};

template <typename AstClass>
auto get(const base::instance& ast) -> std::vector<typename AstClass::instance>
{
	static_assert(std::is_base_of_v<base, AstClass>,
				  "Cannot get class that does not derive from bpy::ast::base");
	auto ast_classes = std::vector<typename AstClass::instance>{};

	const auto& dependencies = ast->dependencies();
	std::for_each(std::begin(dependencies),
				  std::end(dependencies),
				  [&](const base::instance& dependency) {
					  if (const auto ast_class =
							  std::dynamic_pointer_cast<AstClass>(dependency);
						  ast_class)
						  ast_classes.emplace_back(ast_class);
				  });

	return ast_classes;
}

struct identical_to {
	auto operator()(const ast::base::instance&,
					const ast::base::instance&) const -> bool;
};

struct hash_by {
	auto operator()(const ast::base::instance&) const -> std::size_t;
};

} // namespace bpy::ast
