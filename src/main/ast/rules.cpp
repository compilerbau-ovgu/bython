/// \file

#include "rules.hpp"

namespace bpy::ast {
    /**
     * utility method for translating a @class RuleName to string
     * @param rn the rule to translate
     * @return the translated rule
     */
auto rule_name_to_str(const RuleName rn) -> const char*
{
	switch (rn) {
		case RuleName::Program:
			return "Program";

		case RuleName::Function:
			return "Function";

		case RuleName::Record:
			return "Record";

		case RuleName::RecordBody:
			return "RecordBody";

		case RuleName::RecordMember:
			return "RecordMember";

		case RuleName::FuncDeclArgs:
			return "FuncDeclArgs";

		case RuleName::FuncDeclArg:
			return "FuncDeclArg";

		case RuleName::ReturnType:
			return "ReturnType";

		case RuleName::BlockExpression:
			return "BlockExpression";

		case RuleName::Statement:
			return "Statement";

		case RuleName::Expression:
			return "Expression";

		case RuleName::LetStatement:
			return "LetStatement";

		case RuleName::IfStatement:
			return "IfStatement";

		case RuleName::ElseStatement:
			return "ElseStatement";

		case RuleName::IfExpression:
			return "IfExpression";

		case RuleName::ElseExpression:
			return "ElseExpression";

		case RuleName::CallExpression:
			return "CallExpression";

		case RuleName::CallArgs:
			return "CallArgs";

		case RuleName::LambdaExpression:
			return "LambdaExpression";

		case RuleName::BooleanExpression:
			return "BooleanExpression";

		case RuleName::FirstClassBooleanExpression:
			return "FirstClassBooleanExpression";

		case RuleName::NoneBooleanExpression:
			return "NoneBooleanExpression";

		case RuleName::Comparison:
			return "Comparison";

		case RuleName::ArithmeticExpression:
			return "ArithmeticExpression";

		case RuleName::FirstClassArithmeticExpression:
			return "FirstClassArithmeticExpression";

		case RuleName::NoneArithmeticExpression:
			return "NoneArithmeticExpression";

		case RuleName::FunctionName:
			return "FunctionName";

		case RuleName::Constructor:
			return "Constructor";

		case RuleName::TypeName:
			return "TypeName";

		case RuleName::VariableName:
			return "VariableName";

		case RuleName::Identifier:
			return "Identifier";

		case RuleName::Constant:
			return "Constant";

		case RuleName::NumericConstant:
			return "NumericConstant";

		case RuleName::FloatConstant:
			return "FloatConstant";

		case RuleName::IntegerConstant:
			return "IntegerConstant";

		case RuleName::BooleanConstant:
			return "BooleanConstant";

		case RuleName::StringConstant:
			return "StringConstant";
	}
}
} // namespace bpy::ast
