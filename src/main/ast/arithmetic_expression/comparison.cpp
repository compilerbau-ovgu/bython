/// \file

#include "ast/arithmetic_expression/comparison.hpp"

namespace bpy::ast {

comparison::comparison(base::constructor_access /*unnamed*/,
					   arithmetic_expression::instance _left,
					   std::string _binary_operator,
					   arithmetic_expression::instance _right)
	: comparison{std::move(_left), std::move(_binary_operator),
				 std::move(_right)}
{
}

comparison::comparison(arithmetic_expression::instance _left,
					   std::string _binary_operator,
					   arithmetic_expression::instance _right)
	: base{RuleName::Comparison}
	, left{std::move(_left)}
	, binary_operator{std::move(_binary_operator)}
	, right{std::move(_right)}
{
}

/**
 * creates a representation of a FirstClassBooleanExpression Bython Rule.  A Comparison contains a Arithmetic Expression,
 * a Comparison Operator and another Arithmetic Expression
 * @param ptr the PEG AST class to transform
 * @return an FirstClassBooleanExpression instance
 */
auto comparison::create(const std::shared_ptr<peg::Ast>& ptr)
	-> comparison::instance
{
	throw_if_bad_ast(ptr);

	const peg::Ast& ast = *ptr;
	auto& children = ptr->nodes;
	return std::make_shared<comparison>(
		base::constructor_access{}, arithmetic_expression::create(children[0]),
		children[1]->token, arithmetic_expression::create(children[2]));
}

/**
 * a statement cant be compared
 * @return always false
 */
auto comparison::operator==(const comparison& /*unused*/) const -> bool
{
	return false;
}

auto comparison::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {left->dependencies()};
		auto rhs_deps = right->dependencies();
		base::dependencies_.merge(rhs_deps);
	});
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto comparison::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	auto& children = ptr->nodes;
	if (children[0]->original_name != "ArithmeticExpression") {
		throw std::invalid_argument{
			std::string{"Comparison expected "
						"ArithmeticExpression on left side.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (children[1]->original_name != "ComparisonOperator") {
		throw std::invalid_argument{
			std::string{"Comparison expected "
						"ComparisonOperator between ArithmeticExpressions.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (children[2]->original_name != "ArithmeticExpression") {
		throw std::invalid_argument{
			std::string{"Comparison expected "
						"ArithmeticExpression on right side.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto comparison::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t leftSideHash = left->hash_impl();
	std::size_t operatorHash = std::hash<std::string>{}(binary_operator);
	std::size_t rightSideHash = right->hash_impl();
	return ruleHash ^
		   (leftSideHash ^ (operatorHash ^ (rightSideHash << 1) << 1) << 1);
}
} // namespace bpy::ast
