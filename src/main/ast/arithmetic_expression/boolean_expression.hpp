#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/first_class_boolean_expression.hpp"
#include "ast/constant/numeric.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class first_class_boolean_expression;

class boolean_expression : public base {
  public:
	using instance = std::shared_ptr<boolean_expression>;
	using rhs_expr_type =
		std::optional<std::pair<std::string, boolean_expression::instance>>;

	boolean_expression(base::constructor_access,
					   std::shared_ptr<first_class_boolean_expression>,
					   rhs_expr_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const boolean_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto has_rhs() const -> bool;

	auto get_lhs() const
		-> const std::shared_ptr<first_class_boolean_expression>&;
	auto get_op() const -> const std::string&;
	auto get_rhs() const -> const boolean_expression::instance&;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit boolean_expression(std::shared_ptr<first_class_boolean_expression>,
								rhs_expr_type);

	std::shared_ptr<first_class_boolean_expression> value;
	rhs_expr_type binary_operator;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
