/// \file

#include "boolean_expression.hpp"

namespace bpy::ast {

boolean_expression::boolean_expression(
	base::constructor_access /*unnamed*/,
	first_class_boolean_expression::instance _value,
	rhs_expr_type _binary_operator)
	: boolean_expression{std::move(_value), std::move(_binary_operator)}
{
}

boolean_expression::boolean_expression(
	first_class_boolean_expression::instance _value,
	rhs_expr_type _binary_operator)
	: base{RuleName::BooleanExpression}
	, value{std::move(_value)}
	, binary_operator{std::move(_binary_operator)}

{
}

/**
 * creates a representation of a BooleanExpression Bython Rule.  Its basically a wrapper for a FirstClassBooleanExpression and optionally
 * a SecondClassBinaryBooleanOperator and a BooleanExpression
 * @param ptr the PEG AST class to transform
 * @return an BooleanExpression instance
 */
auto boolean_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> boolean_expression::instance
{
	throw_if_bad_ast(ptr);

	const peg::Ast& ast = *ptr;
	const std::shared_ptr<peg::Ast>& first_class_bool_expr = ast.nodes.front();

	auto rhs = rhs_expr_type{};

	if (std::size(ast.nodes) == 3) {
		const std::shared_ptr<peg::Ast>& arithmetic_operator = ast.nodes[1];
		const std::shared_ptr<peg::Ast>& righthand_expression = ast.nodes[2];

		rhs = std::make_pair(arithmetic_operator->token,
							 boolean_expression::create(righthand_expression));
	}
	else {
		rhs = std::nullopt;
	}

	return std::make_shared<boolean_expression>(
		base::constructor_access{},
		first_class_boolean_expression::create(first_class_bool_expr),
		rhs);
}

/**
 * a statement cant be compared
 * @return always false
 */
auto boolean_expression::operator==(const boolean_expression& other) const
	-> bool
{
	return false;
}


/**
 * get the dependencis of a BooleanExpression are the dependencioes of its rhs and lhs childs
 * @return a unordered set containing the dependencies
 */
auto boolean_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {get_lhs()->dependencies()};
		if (has_rhs()) {
			auto rhs_deps = get_rhs()->dependencies();
			base::dependencies_.merge(rhs_deps);
		}
	});
	return base::dependencies_;
} // namespace bpy::ast

auto boolean_expression::has_rhs() const -> bool
{
	return binary_operator != std::nullopt;
}

auto boolean_expression::get_lhs() const
	-> const std::shared_ptr<first_class_boolean_expression>&
{
	return value;
}

auto boolean_expression::get_op() const -> const std::string&
{
	return binary_operator->first;
}

auto boolean_expression::get_rhs() const -> const boolean_expression::instance&
{
	return binary_operator->second;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto boolean_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const std::shared_ptr<peg::Ast>& ast_first_child = ptr->nodes.front();
	if (ast_first_child->original_name != "FirstClassBooleanExpression") {
		throw std::invalid_argument{
			std::string{"BooleanExpression expected "
						"FirstClassBooleanExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (!(ptr->nodes.size() == 1 || ptr->nodes.size() == 3)) {
		throw std::invalid_argument{
			std::string{"BooleanExpression expected to have one "
						"or three children.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		if (ptr->nodes[1]->original_name !=
			"SecondClassBinaryBooleanOperator") {
			throw std::invalid_argument{
				std::string{"BooleanExpression expected  "
							"SecondClassBinaryBooleanOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (!ptr->nodes[1]->is_token) {
			throw std::invalid_argument{
				std::string{"BooleanExpression expected  "
							"SecondClassBinaryBooleanOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (ptr->nodes[2]->original_name != "BooleanExpression") {
			throw std::invalid_argument{
				std::string{"BooleanExpression expected  "
							"BooleanExpression after Operator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

auto boolean_expression::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t boolExprHash = value->hash_impl();
	if (binary_operator) {
		boolExprHash =
			boolExprHash ^ (std::hash<std::string>{}(binary_operator->first) ^
							(binary_operator->second->hash_impl() << 1) << 1);
	}
	return ruleHash ^ (boolExprHash << 1);
}
} // namespace bpy::ast
