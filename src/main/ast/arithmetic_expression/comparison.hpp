#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/arithmetic_expression.hpp"
#include "base.hpp"

namespace bpy::ast {

class arithmetic_expression;

class comparison : public base {
  public:
	using instance = std::shared_ptr<comparison>;

	comparison(base::constructor_access,
			   std::shared_ptr<arithmetic_expression>,
			   std::string,
			   std::shared_ptr<arithmetic_expression>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto operator==(const comparison&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	std::shared_ptr<arithmetic_expression> left;
	std::string binary_operator;
	std::shared_ptr<arithmetic_expression> right;

  private:
	explicit comparison(std::shared_ptr<arithmetic_expression>,
						std::string,
						std::shared_ptr<arithmetic_expression>);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
