#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/arithmetic_expression.hpp"
#include "ast/constant/numeric.hpp"
#include "ast/expression/call_expression.hpp"
#include "ast/expression/if_expression.hpp"
#include "ast/expression/lambda_expression.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class arithmetic_expression;
class if_expression;
class lambda_expression;
class call_expression;

class none_arithmetic_expression : public base {
  public:
	using instance = std::shared_ptr<none_arithmetic_expression>;
	using none_arithmetic_expression_type =
		std::variant<std::shared_ptr<arithmetic_expression>,
					 std::shared_ptr<call_expression>,
					 std::shared_ptr<lambda_expression>,
					 std::shared_ptr<if_expression>,
					 numeric::instance,
					 variable_name::instance>;

	none_arithmetic_expression(base::constructor_access,
							   std::optional<std::string>,
							   none_arithmetic_expression_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const none_arithmetic_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::optional<std::string> unary_arithmetic_operator;
	none_arithmetic_expression_type value;

  private:
	explicit none_arithmetic_expression(std::optional<std::string>,
										none_arithmetic_expression_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
