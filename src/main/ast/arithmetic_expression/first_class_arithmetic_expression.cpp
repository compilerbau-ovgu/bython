/// \file

#include "ast/arithmetic_expression/first_class_arithmetic_expression.hpp"

namespace bpy::ast {

first_class_arithmetic_expression::first_class_arithmetic_expression(
	base::constructor_access /*unnamed*/,
	none_arithmetic_expression::instance _value,
	rhs_expr_type _binary_operator)
	: first_class_arithmetic_expression{std::move(_value),
										std::move(_binary_operator)}
{
}

first_class_arithmetic_expression::first_class_arithmetic_expression(
	none_arithmetic_expression::instance _value, rhs_expr_type _binary_operator)
	: base{RuleName::FirstClassArithmeticExpression}
	, value{std::move(_value)}
	, binary_operator{std::move(_binary_operator)}

{
}

/**
 * creates a representation of a FirstClassArithmeticExpression Bython Rule. It can, but dont have to contain a rhs
 * @param ptr the PEG AST class to transform
 * @return an FirstClassArithmeticExpression instance
 */
auto first_class_arithmetic_expression::create(
	const std::shared_ptr<peg::Ast>& ptr)
	-> first_class_arithmetic_expression::instance
{
	throw_if_bad_ast(ptr);

	const peg::Ast& ast = *ptr;
	const std::shared_ptr<peg::Ast>& none_arithmetic = ast.nodes.front();
	if (ast.nodes.size() == 3) {
		const std::shared_ptr<peg::Ast>& arithmetic_operator = ast.nodes[1];
		const std::shared_ptr<peg::Ast>& righthand_expression = ast.nodes[2];
		return std::make_shared<first_class_arithmetic_expression>(
			base::constructor_access{},
			none_arithmetic_expression::create(none_arithmetic),
			rhs_expr_type(
				std::make_pair(arithmetic_operator->token,
							   first_class_arithmetic_expression::create(
								   righthand_expression))));
	}
	return std::make_shared<first_class_arithmetic_expression>(
		base::constructor_access{},
		none_arithmetic_expression::create(none_arithmetic),
		std::nullopt);
}

/**
 * a statement cant be compared
 * @return always false
 */
auto first_class_arithmetic_expression::
operator==(const first_class_arithmetic_expression& other) const -> bool
{
	return false;
}

/**
 * get the dependencis of a None Arithmetic Expression are the dependencioes of its rhs and lhs childs
 * @return a unordered set containing the dependencies
 */
auto first_class_arithmetic_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {get_lhs()->dependencies()};
		if (has_rhs()) {
			auto rhs_deps = get_rhs()->dependencies();
			base::dependencies_.merge(rhs_deps);
		}
	});
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto first_class_arithmetic_expression::throw_if_bad_ast(
	const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const std::shared_ptr<peg::Ast>& ast_first_child = ptr->nodes.front();
	if (ast_first_child->original_name != "NoneArithmeticExpression") {
		throw std::invalid_argument{
			std::string{"FirstClassArithmeticExpression expected "
						"NoneArithmeticExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (!(ptr->nodes.size() == 1 || ptr->nodes.size() == 3)) {
		throw std::invalid_argument{
			std::string{"FirstClassArithmeticExpression expected to have one "
						"to tree children.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		if (ptr->nodes[1]->original_name !=
			"FirstClassBinaryArithmeticOperator") {
			throw std::invalid_argument{
				std::string{"FirstClassArithmeticExpression expected  "
							"FirstClassBinaryArithmeticOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (!ptr->nodes[1]->is_token) {
			throw std::invalid_argument{
				std::string{"FirstClassArithmeticExpression expected  "
							"FirstClassBinaryArithmeticOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (ptr->nodes[2]->original_name != "FirstClassArithmeticExpression") {
			throw std::invalid_argument{
				std::string{"FirstClassArithmeticExpression expected  "
							"FirstClassArithmeticExpression after Operator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

auto first_class_arithmetic_expression::hash_impl() const noexcept
	-> std::size_t
{
	std::size_t rule_hash = std::hash<RuleName>{}(rule_name);
	std::size_t fcae = value->hash_impl();

	if (binary_operator == std::nullopt) {
		const auto& first = binary_operator->first;
		const auto& second = binary_operator->second;

		fcae ^= std::hash<std::string>{}(first) ^ (second->hash_impl() << 1);
	}

	return rule_hash ^ (fcae << 1);
}

/**
 * gets the lhs of the Arithmetic expression
 * @return the lhs as None Arithmetic Expression
 */
auto first_class_arithmetic_expression::get_lhs() const
	-> const std::shared_ptr<none_arithmetic_expression>&
{
	return value;
}

/**
 * checks if the Arithmetic expression has a rhs
 * @return  true if a rhs exists, false otherwise
 */
auto first_class_arithmetic_expression::has_rhs() const -> bool
{
	return binary_operator != std::nullopt;
}

/**
 * get the rhs of the Arithmetic expression
 * @return the rhs as FirstClassArithmeticExpression
 */
auto first_class_arithmetic_expression::get_rhs() const
	-> const std::shared_ptr<first_class_arithmetic_expression>&
{
	return binary_operator->second;
}
} // namespace bpy::ast
