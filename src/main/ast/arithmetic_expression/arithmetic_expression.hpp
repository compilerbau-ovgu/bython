#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/first_class_arithmetic_expression.hpp"
#include "ast/constant/numeric.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class first_class_arithmetic_expression;

class arithmetic_expression : public base {
  public:
	using instance = std::shared_ptr<arithmetic_expression>;
	using rhs_expr_type =
		std::optional<std::pair<std::string, arithmetic_expression::instance>>;

	arithmetic_expression(base::constructor_access,
						  std::shared_ptr<first_class_arithmetic_expression>,
						  rhs_expr_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const arithmetic_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto has_rhs() const -> bool;

	auto get_lhs() const
		-> const std::shared_ptr<first_class_arithmetic_expression>&;
	auto get_op() const -> const std::string&;
	auto get_rhs() const -> const arithmetic_expression::instance&;

  private:
	explicit arithmetic_expression(
		std::shared_ptr<first_class_arithmetic_expression>, rhs_expr_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);

	std::shared_ptr<first_class_arithmetic_expression> value;
	rhs_expr_type binary_operator;
};

} // namespace bpy::ast
