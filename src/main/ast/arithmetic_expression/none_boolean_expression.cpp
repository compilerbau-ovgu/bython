/// \file

#include "none_boolean_expression.hpp"

namespace bpy::ast {

none_boolean_expression::none_boolean_expression(
	base::constructor_access /*unnamed*/,
	std::optional<std::string> operator_,
	none_boolean_expression_type value_)
	: none_boolean_expression{std::move(operator_), std::move(value_)}
{
}

none_boolean_expression::none_boolean_expression(
	std::optional<std::string> operator_, none_boolean_expression_type value_)
	: base{RuleName::NoneBooleanExpression}
	, unary_boolean_operator{std::move(operator_)}
	, value{std::move(value_)}

{
}

/**
 * creates a representation of a NoneBooleanExpression Bython Rule. Contains optional a ! (not) oeprator
 * and a Expression, Comparison, Booleanconstant or a variable name
 * @param ptr the PEG AST class to transform
 * @return an NoneBooleanExpression instance
 */
auto none_boolean_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> none_boolean_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& ast_operator = ast.nodes.front();
	std::optional<std::string> boolean_operator = std::nullopt;
	if (ast_operator->original_name == "UnaryBooleanOperator") {
		boolean_operator = ast_operator->token;
	}

	const std::shared_ptr<peg::Ast>& child = ast.nodes.back();
	std::string child_name = child->original_name;
	if (child_name == "BooleanExpression") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(boolean_expression::create(child)));
	}
	if (child_name == "Comparison") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(comparison::create(child)));
	}
	if (child_name == "CallExpression") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(call_expression::create(child)));
	}
	if (child_name == "LambdaExpression") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(lambda_expression::create(child)));
	}
	if (child_name == "IfExpression") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(if_expression::create(child)));
	}
	if (child_name == "BooleanConstant") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(boolean_constant::create(child)));
	}
	if (child_name == "VariableName") {
		return std::make_shared<none_boolean_expression>(
			base::constructor_access{}, boolean_operator,
			none_boolean_expression_type(variable_name::create(child)));
	}
	throw std::invalid_argument{
		std::string{
			"NoneArithmeticExpression <- UnaryArithmeticOperator?  ('(' %ows "
			"ArithmeticExpression ')' %ows / CallExpression / LambdaExpression "
			"/ IfExpression / NumericConstant / VariableName)\n"} +
		std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
}

/**
 * a statement cant be compared
 * @return always false
 */
auto none_boolean_expression::
operator==(const none_boolean_expression& other) const -> bool
{
	return false;
}

/**
 * get the dependencis of a None Boolean Expression are the dependencioes of its childs
 * @return a unordered set containing the dependencies
 */
auto none_boolean_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	struct dept_visitor {
		auto operator()(const boolean_expression::instance& ae) noexcept
		{
			return ae->dependencies();
		}
		auto operator()(const call_expression::instance& ce) noexcept
		{
			return ce->dependencies();
		}
		auto operator()(const lambda_expression::instance& le) noexcept
		{
			return le->dependencies();
		}
		auto operator()(const if_expression::instance& ie) noexcept
		{
			return ie->dependencies();
		}
		auto operator()(const comparison::instance& n) noexcept
		{
			return n->dependencies();
		}
		auto operator()(const variable_name::instance& vn) noexcept
		{
			return vn->dependencies();
		}
		auto operator()(const boolean_constant::instance& bc) noexcept
		{
			return bc->dependencies();
		}
	};

	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {std::visit(dept_visitor{}, value)};
	});
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto none_boolean_expression::throw_if_bad_ast(
	const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const std::shared_ptr<peg::Ast>& ast_operator = ptr->nodes.front();
	if (ast_operator->original_name == "UnaryArithmeticOperator") {
		if (!ast_operator->is_token) {
			throw std::invalid_argument{
				std::string{"UnaryArithmeticOperator expected to be token.\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
	if (ptr->nodes.size() > 2 || ptr->nodes.empty()) {
		throw std::invalid_argument{
			std::string{
				"NoneArithmeticExpression expected to have one or two child."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto none_boolean_expression::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const boolean_expression::instance& be) noexcept
		{
			return be->hash_impl();
		}
		auto operator()(const comparison::instance& c) noexcept
		{
			return c->hash_impl();
		}
		auto operator()(const call_expression::instance& ce) noexcept
		{
			return ce->hash_impl();
		}
		auto operator()(const lambda_expression::instance& le) noexcept
		{
			return le->hash_impl();
		}
		auto operator()(const if_expression::instance& ie) noexcept
		{
			return ie->hash_impl();
		}
		auto operator()(const boolean_constant::instance& bc) noexcept
		{
			return bc->hash_impl();
		}
		auto operator()(const variable_name::instance& vn) noexcept
		{
			return vn->hash_impl();
		}
	};
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t noneBooleanExpressionHash = std::visit(hash_visitor{}, value);
	if (unary_boolean_operator) {
		std::size_t unaryHash =
			std::hash<std::string>{}(unary_boolean_operator.value());
		noneBooleanExpressionHash ^= unaryHash << 1;
	}
	return ruleHash ^ (noneBooleanExpressionHash << 1);
}
} // namespace bpy::ast
