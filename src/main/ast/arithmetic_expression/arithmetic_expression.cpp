/// \file

#include "ast/arithmetic_expression/arithmetic_expression.hpp"

namespace bpy::ast {

arithmetic_expression::arithmetic_expression(
	base::constructor_access /*unnamed*/,
	first_class_arithmetic_expression::instance _value,
	rhs_expr_type _binary_operator)
	: arithmetic_expression{std::move(_value), std::move(_binary_operator)}
{
}

arithmetic_expression::arithmetic_expression(
	first_class_arithmetic_expression::instance _value,
	rhs_expr_type _binary_operator)
	: base{RuleName::ArithmeticExpression}
	, value{std::move(_value)}
	, binary_operator{std::move(_binary_operator)}

{
}


/**
 * creates a representation of a ArithmeticExpression Bython Rule.  Its basically a wrapper for a FirstClassArithmeticExpression and optionally
 * a SecondClassBinaryBooleanOperator and a BooleanExpression
 * @param ptr the PEG AST class to transform
 * @return an ArithmeticExpression instance
 */
auto arithmetic_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> arithmetic_expression::instance
{
	throw_if_bad_ast(ptr);

	const peg::Ast& ast = *ptr;
	const std::shared_ptr<peg::Ast>& none_arithmetic = ast.nodes.front();

	auto rhs = rhs_expr_type{};
	if (std::size(ast.nodes) == 3) {
		const std::shared_ptr<peg::Ast>& arithmetic_operator = ast.nodes[1];
		const std::shared_ptr<peg::Ast>& righthand_expression = ast.nodes[2];

		rhs =
			std::make_pair(arithmetic_operator->token,
						   arithmetic_expression::create(righthand_expression));
	}
	else {
		rhs = std::nullopt;
	}

	return std::make_shared<arithmetic_expression>(
		base::constructor_access{},
		first_class_arithmetic_expression::create(none_arithmetic),
		rhs);
}

/**
 * a statement cant be compared
 * @return always false
 */
auto arithmetic_expression::
operator==(const arithmetic_expression& /*unused*/) const -> bool
{
	return false;
}

/**
 * get the dependencis of a ArithmeticExpression are the dependencioes of its rhs and lhs childs
 * @return a unordered set containing the dependencies
 */
auto arithmetic_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {get_lhs()->dependencies()};
		if (has_rhs()) {
			auto rhs_deps = get_rhs()->dependencies();
			base::dependencies_.merge(rhs_deps);
		}
	});
	return base::dependencies_;
}

/**
 * checks if the Arithmetic expression has a rhs
 * @return true if rhs is present, false otherwise
 */
auto arithmetic_expression::has_rhs() const -> bool
{
	return binary_operator != std::nullopt;
}

/**
 * get the lhs of the arithmetic expression
 * @return  the lhs
 */
auto arithmetic_expression::get_lhs() const
	-> const first_class_arithmetic_expression::instance&
{
	return value;
}

auto arithmetic_expression::get_op() const -> const std::string&
{
	return binary_operator->first;
}

/**
 * get the rhs of this arithmetic expression
 * @return the rhs arithmetic expression
 */
auto arithmetic_expression::get_rhs() const
	-> const arithmetic_expression::instance&
{
	return binary_operator->second;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto arithmetic_expression::throw_if_bad_ast(
	const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const std::shared_ptr<peg::Ast>& ast_first_child = ptr->nodes.front();
	if (ast_first_child->original_name != "FirstClassArithmeticExpression") {
		throw std::invalid_argument{
			std::string{"ArithmeticExpression expected "
						"FirstClassArithmeticExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (!(ptr->nodes.size() == 1 || ptr->nodes.size() == 3)) {
		throw std::invalid_argument{
			std::string{"ArithmeticExpression expected to have one "
						"to tree children.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		if (ptr->nodes[1]->original_name !=
			"SecondClassBinaryArithmeticOperator") {
			throw std::invalid_argument{
				std::string{"ArithmeticExpression expected  "
							"FirstClassBinaryArithmeticOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (!ptr->nodes[1]->is_token) {
			throw std::invalid_argument{
				std::string{"ArithmeticExpression expected  "
							"FirstClassBinaryArithmeticOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (ptr->nodes[2]->original_name != "ArithmeticExpression") {
			throw std::invalid_argument{
				std::string{"ArithmeticExpression expected  "
							"FirstClassArithmeticExpression after Operator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

auto arithmetic_expression::hash_impl() const noexcept -> std::size_t
{
	// NOTE: Cannot hash std::nullopt :(
	return binary_operator == std::nullopt
			   ? std::hash<std::size_t>{}(0)
			   : binary_operator->second->hash_impl() ^
					 (std::hash<std::string>{}(binary_operator->first) << 1);
}
} // namespace bpy::ast
