#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/boolean_expression.hpp"
#include "ast/arithmetic_expression/comparison.hpp"
#include "ast/constant/boolean_constant.hpp"
#include "ast/expression/call_expression.hpp"
#include "ast/expression/if_expression.hpp"
#include "ast/expression/lambda_expression.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class boolean_expression;
class call_expression;
class lambda_expression;
class if_expression;
class comparison;
class boolean_constant;
class variable_name;

class none_boolean_expression : public base {
  public:
	using instance = std::shared_ptr<none_boolean_expression>;
	using none_boolean_expression_type =
		std::variant<std::shared_ptr<boolean_expression>,
					 std::shared_ptr<comparison>,
					 std::shared_ptr<call_expression>,
					 std::shared_ptr<lambda_expression>,
					 std::shared_ptr<if_expression>,
					 std::shared_ptr<boolean_constant>,
					 std::shared_ptr<variable_name>>;

	none_boolean_expression(base::constructor_access,
							std::optional<std::string>,
							none_boolean_expression_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const none_boolean_expression&) const -> bool;
	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::optional<std::string> unary_boolean_operator;
	none_boolean_expression_type value;

  private:
	explicit none_boolean_expression(std::optional<std::string>,
									 none_boolean_expression_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
