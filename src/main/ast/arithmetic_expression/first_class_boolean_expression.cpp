/// \file

#include "ast/arithmetic_expression/first_class_boolean_expression.hpp"

namespace bpy::ast {

first_class_boolean_expression::first_class_boolean_expression(
	base::constructor_access /*unnamed*/,
	none_boolean_expression::instance _value,
	rhs_expr_type _binary_operator)
	: first_class_boolean_expression{std::move(_value),
									 std::move(_binary_operator)}
{
}

first_class_boolean_expression::first_class_boolean_expression(
	none_boolean_expression::instance _value, rhs_expr_type _binary_operator)
	: base{RuleName::FirstClassBooleanExpression}
	, value{std::move(_value)}
	, binary_operator{std::move(_binary_operator)}

{
}

/**
 * creates a representation of a FirstClassBooleanExpression Bython Rule.  It can, but dont have to contain a rhs
 * @param ptr the PEG AST class to transform
 * @return an FirstClassBooleanExpression instance
 */
auto first_class_boolean_expression::create(
	const std::shared_ptr<peg::Ast>& ptr)
	-> first_class_boolean_expression::instance
{
	throw_if_bad_ast(ptr);

	const peg::Ast& ast = *ptr;
	const std::shared_ptr<peg::Ast>& none_arithmetic = ast.nodes.front();

	auto rhs = rhs_expr_type{};
	if (std::size(ast.nodes) == 3) {
		const std::shared_ptr<peg::Ast>& arithmetic_operator = ast.nodes[1];
		const std::shared_ptr<peg::Ast>& righthand_expression = ast.nodes[2];

		rhs = std::make_pair(
			arithmetic_operator->token,
			first_class_boolean_expression::create(righthand_expression));
	}
	else {
		rhs = std::nullopt;
	}

	return std::make_shared<first_class_boolean_expression>(
		base::constructor_access{},
		none_boolean_expression::create(none_arithmetic),
		rhs);
}

/**
 * a statement cant be compared
 * @return always false
 */
auto first_class_boolean_expression::
operator==(const first_class_boolean_expression& other) const -> bool
{
	return false;
}

/**
 * get the dependencis of a None Arithmetic Expression are the dependencioes of its rhs and lhs childs
 * @return a unordered set containing the dependencies
 */
auto first_class_boolean_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {get_lhs()->dependencies()};

		if (has_rhs()) {
			auto rhs_deps = get_rhs()->dependencies();
			base::dependencies_.merge(rhs_deps);
		}
	});
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto first_class_boolean_expression::throw_if_bad_ast(
	const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const std::shared_ptr<peg::Ast>& ast_first_child = ptr->nodes.front();
	if (ast_first_child->original_name != "NoneBooleanExpression") {
		throw std::invalid_argument{
			std::string{"FirstClassBooleanExpression expected "
						"NoneBooleanExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (!(ptr->nodes.size() == 1 || ptr->nodes.size() == 3)) {
		throw std::invalid_argument{
			std::string{"FirstClassBooleanExpression expected to have one "
						"or three children.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		if (ptr->nodes[1]->original_name != "FirstClassBinaryBooleanOperator") {
			throw std::invalid_argument{
				std::string{"FirstClassBooleanExpression expected  "
							"FirstClassBinaryBooleanOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (!ptr->nodes[1]->is_token) {
			throw std::invalid_argument{
				std::string{"FirstClassBooleanExpression expected  "
							"FirstClassBinaryArithmeticOperator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (ptr->nodes[2]->original_name != "FirstClassBooleanExpression") {
			throw std::invalid_argument{
				std::string{"FirstClassBooleanExpression expected  "
							"FirstClassBooleanExpression after Operator\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

auto first_class_boolean_expression::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t boolExprHash = value->hash_impl();

	if (binary_operator) {
		std::size_t operatorHash =
			std::hash<std::string>{}(binary_operator->first);
		boolExprHash =
			boolExprHash ^
			(operatorHash ^ (binary_operator->second->hash_impl() << 1) << 1);
	}
	return ruleHash ^ (boolExprHash << 1);
}

/**
 * get the lhs of the FirstClassBooleanExpression
 * @return the lhs as NoneBooleanExpression
 */
auto first_class_boolean_expression::get_lhs() const
	-> const std::shared_ptr<none_boolean_expression>&
{
	return value;
}

/**
 * checks if this Expression has a rhs
 * @return true if this exists, false otherwise
 */
auto first_class_boolean_expression::has_rhs() const -> bool
{
	return binary_operator != std::nullopt;
}

/**
 * get the rhs of this FirstClassBooleanExpression
 * @return the rhs as FirstClassBooleanExpression
 */
auto first_class_boolean_expression::get_rhs() const
	-> const std::shared_ptr<first_class_boolean_expression>&
{
	return binary_operator->second;
}
} // namespace bpy::ast
