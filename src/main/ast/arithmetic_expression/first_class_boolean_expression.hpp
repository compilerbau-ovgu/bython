#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/arithmetic_expression/none_boolean_expression.hpp"
#include "ast/constant/numeric.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class none_boolean_expression;

class first_class_boolean_expression : public base {
  public:
	using instance = std::shared_ptr<first_class_boolean_expression>;
	using rhs_expr_type = std::optional<
		std::pair<std::string, first_class_boolean_expression::instance>>;

	first_class_boolean_expression(base::constructor_access,
								   std::shared_ptr<none_boolean_expression>,
								   rhs_expr_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const first_class_boolean_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

    auto get_lhs() const -> const std::shared_ptr<none_boolean_expression>&;

    auto has_rhs() const -> bool;

    auto get_rhs() const -> const std::shared_ptr<first_class_boolean_expression>&;

	std::shared_ptr<none_boolean_expression> value;
	rhs_expr_type binary_operator;

  private:
	explicit first_class_boolean_expression(
		std::shared_ptr<none_boolean_expression>, rhs_expr_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
