/// \file

#include "call_expression.hpp"

#include "ast/function/function.hpp"
#include "ast/record/record.hpp"

namespace bpy::ast {

call_expression::call_expression(base::constructor_access /*unnamed*/,
								 call_expr_type call_name,
								 value_callargs callargs_)
	: call_expression{std::move(call_name), std::move(callargs_)}
{
}

call_expression::call_expression(call_expr_type call_name,
								 value_callargs callargs_)

	: base{RuleName::CallExpression}
	, call_name{std::move(call_name)}
	, call_args{std::move(callargs_)}
{
}

/**
 * creates an instance of the Bython CallExpression rule. A Call Expression can be a function name call or a constructor call
 * and contains 0 to n Call Args
 * @param ptr the PEG AST class to transform
 * @return an instance of this rule
 */
auto call_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> call_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	const auto& type = ptr->nodes[0];

	call_expr_type call_types;

	if (type->original_name == "Constructor") {
		call_types = constructor::create(ast.nodes[0]);
	}
	else {
		call_types = function_name::create(ast.nodes[0]);
	}

	std::optional<std::shared_ptr<callargs>> callargs;
	if (const auto& back = ast.nodes.back();
		back->original_name == "CallArgs") {
		callargs = callargs::create(back);
	}
	else {
		callargs = std::nullopt;
	}

	return std::make_shared<call_expression>(
		base::constructor_access{}, call_types, callargs);
}

/**
 * gets the dependencies of a Call Expression. Can be either the dependencies of the function name or the constructor
 * @return an unordered set with the dependecies
 */
auto call_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		// base::dependencies_ = {};
		if (const auto& func =
				std::get_if<function_name::instance>(&call_name)) {
			dependencies_ = {{*func}};
		}
		else if (const auto& ctor =
					 std::get_if<constructor::instance>(&call_name)) {
			auto return_type_deps = (*ctor)->dependencies();
			dependencies_.merge(return_type_deps);
		}

		if (call_args) {
			auto return_type_deps = (*call_args)->dependencies();
			dependencies_.merge(return_type_deps);
		}
	});
	return dependencies_;
}
/**
 * checks the equality of two given Call Expressions.
 * @return always false, cant be equal
 */
auto call_expression::operator==(const call_expression& /*unused*/) const
	-> bool
{
	return false;
}

/**
 * get the call instance, can be either a function or a constructor
 * @return the actual call expression type
 */
auto call_expression::get_callee_var() const -> const call_expr_type&
{
	return call_name;
}

/**
 * returns the call name, either a constructor name or a function name
 * @return the name
 */
auto call_expression::get_callee_name() const -> const std::string&
{
	struct callee_visitor {
		const auto& operator()(const constructor::instance& ctor)
		{
			return ctor->get_name();
		}
		const auto& operator()(const function_name::instance& fname)
		{
			return fname->get_name();
		}
	};

	return std::visit(callee_visitor{}, call_name);
}

/**
 * checks if the given Call Expression is valid.
 * CallExpression := Constructor / "
						"FunctionName
 * @param ptr the PEG AST rule to check
 */
auto call_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.empty() || ptr->nodes.size() > 2) {
		throw std::invalid_argument{
			std::string{"Expected: CallExpression := Constructor / "
						"FunctionName\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto call_expression::hash_impl() const noexcept -> std::size_t
{
	return std::hash<call_expression>{}(*this);
}
} // namespace bpy::ast
