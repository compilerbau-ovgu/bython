/// \file

#include "block_expression.hpp"

namespace bpy::ast {

block_expression::block_expression(
	base::constructor_access /*unnamed*/,
	std::vector<std::shared_ptr<statement>> statements,
	std::optional<std::shared_ptr<expression>> _expression)
	: block_expression{std::move(statements), std::move(_expression)}
{
}

block_expression::block_expression(
	std::vector<std::shared_ptr<statement>> statements,
	std::optional<std::shared_ptr<expression>> _expression)
	: base{RuleName::BlockExpression}
	, statements{std::move(statements)}
	, value_expression{std::move(_expression)}
{
}

/**
 * creates an instance of the Bython BlockExpression rule. A Block Expression can contain 0 to n statements and 0 to n expressions
 * @param ptr the PEG AST class to transform
 * @return an instance of this rule
 */
auto block_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> block_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	std::vector<std::shared_ptr<statement>> statements;
	for (auto& node : ast.nodes) {
		if (node->original_name == "Statement") {
			statements.push_back(statement::create(node));
		}
	}
	std::optional<std::shared_ptr<expression>> expression = std::nullopt;
	if (!ast.nodes.empty()) {
		if (const std::shared_ptr<peg::Ast>& back = ast.nodes.back();
			back->original_name == "Expression") {
			expression = expression::create(back);
		}
	}
	return std::make_shared<block_expression>(
		base::constructor_access{}, statements, expression);
}

/**
 * checks the equality of two given Call Expressions.
 * @return always false, cant be equal
 */
auto block_expression::operator==(const block_expression & /*unused*/) const -> bool
{
	return false;
}

/**
 * the dependencies of a block expression are the dependencies of all child statements and child expressions
 * @return an unordered set containing the dependencies
 */
auto block_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {};
		std::for_each(std::begin(statements),
					  std::end(statements),
					  [&](const auto& statement) {
						  auto statement_deps = statement->dependencies();
						  base::dependencies_.merge(statement_deps);
					  });

		if (value_expression != std::nullopt) {
			auto expression_deps = (*value_expression)->dependencies();
			base::dependencies_.merge(expression_deps);
		}
	});
	return base::dependencies_;
}

auto block_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
    //empty implementation
}

auto block_expression::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t statementHash = 0;
	for (const std::shared_ptr<statement>& statement : statements) {
		statementHash = statementHash ^ (statement->hash_impl() << 1);
	}

	if (value_expression) {
		statementHash =
			statementHash ^ (value_expression->get()->hash_impl() << 1);
	}
	return ruleHash ^ (statementHash << 1);
}

} // namespace bpy::ast
