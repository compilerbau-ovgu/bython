/// \file

#include "ast/expression/lambda_expression.hpp"

#include "ostream_overloaded.hpp"

namespace bpy::ast {

lambda_expression::lambda_expression(
	base::constructor_access /*unnamed*/,
	std::shared_ptr<func_decl_args> _args,
	std::optional<std::shared_ptr<return_type>> _type,
	std::shared_ptr<block_expression> _block)
	: lambda_expression{std::move(_args), std::move(_type), std::move(_block)}
{
}

lambda_expression::lambda_expression(
	std::shared_ptr<func_decl_args> _args,
	std::optional<std::shared_ptr<return_type>> _type,
	std::shared_ptr<block_expression> _block)
	: base{RuleName::LambdaExpression}
	, args{std::move(_args)}
	, type{std::move(_type)}
	, block{std::move(_block)}
{
}

auto lambda_expression::operator==(const lambda_expression& /*unused*/) const
	-> bool
{
	return false;
}

/**
 * ATTENTION: This rule ist not further implemented and not used!
 */
auto lambda_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> lambda_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& args_ast = ast.nodes.front();
	const std::shared_ptr<peg::Ast>& block_ast = ast.nodes.back();

	const std::shared_ptr<peg::Ast>& typename_ast = ast.nodes[1];
	std::optional<std::shared_ptr<return_type>> typename_opt = std::nullopt;
	if (typename_ast->original_name == "ReturnType") {
		typename_opt = return_type::create(typename_ast);
	}
	return std::make_shared<lambda_expression>(
		base::constructor_access{}, func_decl_args::create(args_ast),
		typename_opt, block_expression::create(block_ast));
}

auto lambda_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {};

		auto fda_deps = args->dependencies();
		base::dependencies_.merge(fda_deps);

		if (type != std::nullopt) {
			auto ret_deps = (*type)->dependencies();
			base::dependencies_.merge(ret_deps);
		}

		auto block_deps = block->dependencies();
		base::dependencies_.merge(block_deps);
	});
	return base::dependencies_;
}

std::string lambda_expression::get_type() const
{
	return type.value()->get_name();
}

auto lambda_expression::has_type() const -> bool
{
	return type != std::nullopt;
}

auto lambda_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() > 3 || ptr->nodes.size() < 2) {
		throw std::invalid_argument{
			std::string{
				"LambdaExpression expected to have two or three child."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		const std::shared_ptr<peg::Ast>& typename_ast = ptr->nodes[1];
		if (typename_ast->original_name != "ReturnType") {
			throw std::invalid_argument{
				std::string{"LambdaExpression expected ReturnType after "
							"FuncDeclArgs\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
	const std::shared_ptr<peg::Ast>& var_name_ast = ptr->nodes.front();
	if (var_name_ast->original_name != "FuncDeclArgs") {
		throw std::invalid_argument{
			std::string{"LambdaExpression expected FuncDeclArgs.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& expression_ast = ptr->nodes.back();
	if (expression_ast->original_name != "BlockExpression") {
		throw std::invalid_argument{
			std::string{"LambdaExpression expected BlockExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto lambda_expression::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t lambdaExpressionHash =
		ruleHash ^ ((args->hash_impl() ^ (block->hash_impl() << 1)) << 1);

	if (type) {
		lambdaExpressionHash ^= type->get()->hash_impl();
	}
	return ruleHash ^ (lambdaExpressionHash << 1);
}
} // namespace bpy::ast
