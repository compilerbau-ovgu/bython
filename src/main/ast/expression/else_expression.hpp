#pragma once

#include <variant>

#include "../type_system.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/expression/if_expression.hpp"
#include "base.hpp"

namespace bpy::ast {

class if_expression;
class block_expression;

class else_expression : public base {
  public:
	using instance = std::shared_ptr<else_expression>;
	using else_expression_type =
		std::variant<std::shared_ptr<if_expression>,
					 std::shared_ptr<block_expression>>;

	else_expression(base::constructor_access, else_expression_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const else_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	else_expression_type value;

  private:
	explicit else_expression(else_expression_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
