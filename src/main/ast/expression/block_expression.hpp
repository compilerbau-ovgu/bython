#pragma once

#include <memory>
#include <optional>
#include <vector>

#include "../type_system.hpp"
#include "expression/expression.hpp"
#include "statement/statement.hpp"

namespace bpy::ast {

class statement;
class expression;

class block_expression : public base {
  public:
	using instance = std::shared_ptr<block_expression>;

	block_expression(base::constructor_access,
					 std::vector<std::shared_ptr<statement>>,
					 std::optional<std::shared_ptr<expression>>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const block_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::vector<std::shared_ptr<statement>> statements;
	std::optional<std::shared_ptr<expression>> value_expression;

  private:
	explicit block_expression(std::vector<std::shared_ptr<statement>>,
							  std::optional<std::shared_ptr<expression>>);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
