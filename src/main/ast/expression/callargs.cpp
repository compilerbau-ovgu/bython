/// \file

#include "callargs.hpp"

namespace bpy::ast {

callargs::callargs(base::constructor_access /*unused*/,
				   std::vector<expression::instance> members_)
	: callargs{std::move(members_)}
{
}

callargs::callargs(std::vector<expression::instance> members_)
	: members{std::move(members_)}, base{RuleName::CallArgs}
{
}

/**
 * creates an instance of the Bython CallArgs rule. CallArgs are a Comma seperated list of expressions, so this class is a
 * vector wrapper
 * @param ptr the PEG AST class to transform
 * @return an instance of this rule
 */
auto callargs::create(const std::shared_ptr<peg::Ast>& ptr)
	-> callargs::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	auto args = std::vector<expression::instance>{std::size(ast.nodes)};

	std::transform(std::begin(ast.nodes), std::end(ast.nodes), std::begin(args),
				   [](const auto& node) { return expression::create(node); });

	return std::make_shared<callargs>(base::constructor_access{}, args);
}

/**
 * checks the equality of two given Call Args.
 * @return always false, cant be equal
 */
auto callargs::operator==(const callargs& /*unused*/) const -> bool
{
	return false;
}

/**
 * get the dependencies. Depending on the real Call Arg and the return type
 * @return a unordered set with the dependencies
 */
auto callargs::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		for (const auto& arg : members) {
			auto return_type_deps = arg->dependencies();
			dependencies_.merge(return_type_deps);
		}
	});
	return base::dependencies_;
}

auto callargs::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
    //empty because nothing to check
}

auto callargs::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t callArgsHash = 0;

	for (const std::shared_ptr<expression>& expression : members) {
		callArgsHash = callArgsHash ^ (expression->hash_impl() << 1);
	}
	return ruleHash ^ (callArgsHash << 1);
}

} // namespace bpy::ast
