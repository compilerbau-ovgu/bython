#pragma once

#include <algorithm>
#include <numeric>
#include <string>
#include <unordered_set>
#include <vector>

#include "ast/expression/expression.hpp"
#include "base.hpp"

namespace bpy::ast {

class expression;

//! Callargs
class callargs : public base {
  public:
	using instance = std::shared_ptr<callargs>;

	std::vector<std::shared_ptr<expression>> members;

	callargs(base::constructor_access, std::vector<expression::instance>);

	static auto create(const std::shared_ptr<peg::Ast>&) -> callargs::instance;

	auto operator==(const callargs&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit callargs(std::vector<expression::instance>);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
