#pragma once

#include <variant>

#include "../type_system.hpp"
#include "base.hpp"

namespace bpy::ast {

class arithmetic_expression;
class block_expression;
class boolean_expression;
class call_expression;
class if_expression;
class lambda_expression;
class bstring;

class expression : public base {
  public:
	using instance = std::shared_ptr<expression>;
	using expression_type = std::variant<std::shared_ptr<lambda_expression>,
										 std::shared_ptr<if_expression>,
										 std::shared_ptr<block_expression>,
										 std::shared_ptr<call_expression>,
										 std::shared_ptr<arithmetic_expression>,
										 std::shared_ptr<bstring>,
										 std::shared_ptr<boolean_expression>>;

	expression(base::constructor_access, expression_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	expression_type value;

  private:
	explicit expression(expression_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
