/// \file

#include "ast/expression/if_expression.hpp"

namespace bpy::ast {

if_expression::if_expression(base::constructor_access /*unnamed*/,
							 std::shared_ptr<boolean_expression> _ruling,
							 std::shared_ptr<block_expression> _block,
							 std::shared_ptr<else_expression> _alternative)
	: if_expression{std::move(_ruling), std::move(_block),
					std::move(_alternative)}
{
}

if_expression::if_expression(std::shared_ptr<boolean_expression> _ruling,
							 std::shared_ptr<block_expression> _block,
							 std::shared_ptr<else_expression> _alternative)
	: base{RuleName::IfExpression}
	, ruling{std::move(_ruling)}
	, block{std::move(_block)}
	, alternative{std::move(_alternative)}
{
}

/**
 * creates an instance of the Bython AST rule IfExpression.
 * It is used for the program control flow and contains a BooleanExpression,
 * a BlockExpression and an ElseExpression
 * @param ptr the PEG AST rule to transform
 * @return a instance of the rule
 */
auto if_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> if_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	return std::make_shared<if_expression>(
		base::constructor_access{},
		boolean_expression::create(ast.nodes.front()),
		block_expression::create(ast.nodes[1]),
		else_expression::create(ast.nodes.back()));
}
/**
 * checks equality of IfExpression
 * @return always false, cant be equal
 */
auto if_expression::operator==(const if_expression& /*unused*/) const -> bool
{
	return false;
}

/**
 * get the dependencies of a if expression, depending on the child dependencies of the BooleanExpression,
 * BlockExpression and ElseExpression
 * @return an unordered set witth the dependencies
 */
auto if_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		{
			auto return_type_deps = ruling->dependencies();
			dependencies_.merge(return_type_deps);
		}
		{
			auto return_type_deps = block->dependencies();
			dependencies_.merge(return_type_deps);
		}
		{
			auto return_type_deps = alternative->dependencies();
			dependencies_.merge(return_type_deps);
		}
	});
	return base::dependencies_;
}

/**
 * checks if the rule is valid. This is the case if there are 3 childs in this specific Order:
 * BooleanExpression, BlockExpression, ElseExpression
 * @param ptr the PEG AST to check
 */
auto if_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() != 3) {
		throw std::invalid_argument{
			std::string{"IfExpression expected to have three children."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& var_name_ast = ptr->nodes.front();
	if (var_name_ast->original_name != "BooleanExpression") {
		throw std::invalid_argument{
			std::string{"IfExpression expected BooleanExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& expression_ast = ptr->nodes[1];
	if (expression_ast->original_name != "BlockExpression") {
		throw std::invalid_argument{
			std::string{"IfExpression expected BlockExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& else_ast = ptr->nodes.back();
	if (else_ast->original_name != "ElseExpression") {
		throw std::invalid_argument{
			std::string{"IfExpression expected ElseExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto if_expression::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t rulingHash = ruling->hash_impl();
	std::size_t blockHash = block->hash_impl();
	std::size_t alternativeHash = alternative->hash_impl();
	// hippity hoppity, lets shift these property
	return ruleHash ^
		   (rulingHash ^ (blockHash ^ (alternativeHash << 1) << 1) << 1);
}

/**
 * collect the If Expression conditions
 * @param accum the collected if/elseif conditions
 */
auto if_expression::collect_conditions(
	std::vector<boolean_expression::instance>& accum) const -> void
{
	if (const auto else_if_expr =
			std::get_if<if_expression::instance>(&alternative->value);
		else_if_expr != nullptr) {
		accum.push_back((*else_if_expr)->ruling);
		(*else_if_expr)->collect_conditions(accum);
	}
}

/**
 * gets all BooleanExpression conditions
 * @return a vector of BooleanExpression
 */
auto if_expression::collect_conditions() const
	-> std::vector<boolean_expression::instance>
{
	auto conds = std::vector<boolean_expression::instance>(0);
	conds.push_back(ruling);

	collect_conditions(conds);
	return conds;
}

/**
 * transform a nested blockExpression
 * @param accum the flattened BlockExpression
 */
auto if_expression::collect_bodies(
	std::vector<block_expression::instance>& accum) const -> void
{
	if (const auto else_if_expr =
			std::get_if<if_expression::instance>(&alternative->value);
		else_if_expr != nullptr) {
		accum.push_back((*else_if_expr)->block);
		(*else_if_expr)->collect_bodies(accum);
	}

	else {
		const auto else_expr =
			std::get<block_expression::instance>(alternative->value);
		accum.push_back(else_expr);
	}
}

/**
 * gets all BlockExpression
 * @return a vector of BlockExpressions
 */
auto if_expression::collect_bodies() const
	-> std::vector<block_expression::instance>
{
	auto bodies = std::vector<block_expression::instance>(0);
	bodies.push_back(block);

	collect_bodies(bodies);
	return bodies;
}

} // namespace bpy::ast
