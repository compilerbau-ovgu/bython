#pragma once

#include <functional>
#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/base.hpp"
#include "ast/expression/callargs.hpp"
#include "ast/function/constructor.hpp"
#include "ast/function/function_name.hpp"

// namespace std

namespace bpy::ast {
class function;
class record;

class call_expression : public base {
  public:
	using instance = std::shared_ptr<call_expression>;

	using call_expr_type =
		std::variant<constructor::instance, function_name::instance>;
	using value_callargs = std::optional<std::shared_ptr<callargs>>;

	call_expression(base::constructor_access, call_expr_type, value_callargs);

	auto operator==(const call_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto get_callee_var() const -> const call_expr_type&;
	auto get_callee_name() const -> const std::string&;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	static instance create(const std::shared_ptr<peg::Ast>&);

	value_callargs call_args;

  private:
	explicit call_expression(call_expr_type, value_callargs);

	call_expr_type call_name;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};
} // namespace bpy::ast
// custom specialization of std::hash can be injected in namespace std
namespace std {
template <>
struct hash<bpy::ast::call_expression> {
	using argument_type = bpy::ast::call_expression;
	using result_type = std::size_t;
	result_type operator()(argument_type const& s) const noexcept
	{
		return std::hash<argument_type::call_expr_type>{}(s.get_callee_var());
	}
};
} // namespace std
