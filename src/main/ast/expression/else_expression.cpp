/// \file

#include "else_expression.hpp"

namespace bpy::ast {

else_expression::else_expression(base::constructor_access /*unnamed*/,
								 else_expression_type value_)
	: else_expression{std::move(value_)}
{
}

else_expression::else_expression(else_expression_type value_)
	: base{RuleName::ElseExpression}, value{std::move(value_)}
{
}

/**
 * creates an instance of the Bython ElseExpression rule. An Else Expression is g
 * enerated when an condition of if else is given. Can contain either IfExpression or BlockExpression as a childs
 * @param ptr the PEG AST rule to transform
 * @return an instance of the rule
 */
auto else_expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> else_expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& first_child = ast.nodes.front();
	std::string child_name = first_child->original_name;
	if (child_name == "IfExpression") {
		return std::make_shared<else_expression>(
			base::constructor_access{},
			else_expression_type(if_expression::create(first_child)));
	}
	if (child_name == "BlockExpression") {
		return std::make_shared<else_expression>(
			base::constructor_access{},
			else_expression_type(block_expression::create(first_child)));
	}
	throw std::invalid_argument{
		std::string{"ElseExpression <- 'else' %ows (IfExpression / "
					"BlockExpression)\n"} +
		std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
}

/**
 * checks the equality of two else expressions.
 * @return always false, ElseExpressions cant be equal
 */
auto else_expression::operator==(const else_expression& /*unused*/) const
	-> bool
{
	return false;
}

/**
 * get the dependencies of a ElseExpression which are the dependecies of either the
 * IfExpression or the BlockExpression
 * @return an unordered set containing the dependencies
 */
auto else_expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	struct dep_visitor {
		auto operator()(const if_expression::instance& f)
		{
			return f->dependencies();
		}

		auto operator()(const block_expression::instance& b)
		{
			return b->dependencies();
		}
	};
	std::call_once(dependency_cache_flag,
				   [&]() { dependencies_ = std::visit(dep_visitor{}, value); });
	return base::dependencies_;
}

/**
 * checks if the given ElseExpression is valid. This is the case when
 * "ElseExpression <- 'else' %ows (IfExpression / "
						"BlockExpression) is given
 * @param ptr the PEG AST class to check
 * @throws std::invalid_argument if not valid
 */
auto else_expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() != 1) {
		throw std::invalid_argument{
			std::string{"ElseExpression <- 'else' %ows (IfExpression / "
						"BlockExpression)\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto else_expression::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const if_expression::instance& f)
		{
			return f->hash_impl();
		}

		auto operator()(const block_expression::instance& b)
		{
			return b->hash_impl();
		}
	};
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t elseExpressionHash = std::visit(hash_visitor{}, value);
	return ruleHash ^ (elseExpressionHash << 1);
}
} // namespace bpy::ast
