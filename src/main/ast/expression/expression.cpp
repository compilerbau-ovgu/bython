/// \file

#include "expression.hpp"

#include "ast/arithmetic_expression/arithmetic_expression.hpp"
#include "ast/arithmetic_expression/boolean_expression.hpp"
#include "ast/constant/string.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/expression/call_expression.hpp"
#include "ast/expression/if_expression.hpp"
#include "ast/expression/lambda_expression.hpp"

namespace bpy::ast {

expression::expression(base::constructor_access /*unnamed*/,
					   expression_type value_)
	: expression{std::move(value_)}
{
}

expression::expression(expression_type value_)
	: value{std::move(value_)}, base{RuleName::Expression}
{
}

/**
 * Delegate rule fore creating one of the real Expression instances. This can be
 * @class lambda_expression
 * @class if_expression
 * @class block_expression
 * @class call_expression
 * @class arithmetic_expression
 * @class boolean_expression
 * @class bstring
 * @param ptr the PEG AST class to transform
 * @return an instance of a real expression
 */
auto expression::create(const std::shared_ptr<peg::Ast>& ptr)
	-> expression::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& first_child = ast.nodes.front();
	std::string child_name = first_child->original_name;

	auto expr = expression_type{};

	if (child_name == "LambdaExpression") {
		expr = lambda_expression::create(first_child);
	}
	else if (child_name == "IfExpression") {
		expr = if_expression::create(first_child);
	}
	else if (child_name == "BlockExpression") {
		expr = block_expression::create(first_child);
	}
	else if (child_name == "CallExpression") {
		expr = call_expression::create(first_child);
	}
	else if (child_name == "ArithmeticExpression") {
		expr = arithmetic_expression::create(first_child);
	}
	else if (child_name == "BooleanExpression") {
		expr = boolean_expression::create(first_child);
	}
	else if (child_name == "StringConstant") {
		expr = bstring::create(first_child);
	}

	else {
		throw std::invalid_argument{
			std::string{
				"Expression <- (CallExpression / LambdaExpression / "
				"IfExpression / BlockExpression / ArithmeticExpression / "
				"BooleanExpression)\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}

	return std::make_shared<expression>(base::constructor_access{}, expr);
}

/**
 * checks the equality of an expression. Cant be equals because of the delegate
 * @return always false
 */
auto expression::operator==(const expression & /*unused*/) const -> bool
{
	return false;
}

/**
 * returns the dependencies of an expression, which is the dependencies of the real *expression type.
 * @return an unordered set with the dependencies
 */
auto expression::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	struct dependency_visitor {
		auto operator()(const lambda_expression::instance& l)
		{
			return l->dependencies();
		}
		auto operator()(const if_expression::instance& i)
		{
			return i->dependencies();
		}
		auto operator()(const call_expression::instance& c)
		{
			return c->dependencies();
		}
		auto operator()(const block_expression::instance& b)
		{
			return b->dependencies();
		}
		auto operator()(const arithmetic_expression::instance& a)
		{
			return a->dependencies();
		}
		auto operator()(const boolean_expression::instance& b)
		{
			return b->dependencies();
		}
		auto operator()(const bstring::instance& b)
		{
			return b->dependencies();
		}
	};

	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = std::visit(dependency_visitor{}, value);
	});
	return base::dependencies_;
}

auto expression::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (ptr->nodes.size() != 1) {
		throw std::invalid_argument{
			std::string{"Expression expected to have one child\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto expression::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const lambda_expression::instance& l)
		{
			return l->hash_impl();
		}
		auto operator()(const if_expression::instance& i)
		{
			return i->hash_impl();
		}
		auto operator()(const block_expression::instance& b)
		{
			return b->hash_impl();
		}
		auto operator()(const call_expression::instance& c)
		{
			return c->hash_impl();
		}
		auto operator()(const arithmetic_expression::instance& a)
		{
			return a->hash_impl();
		}
		auto operator()(const bstring::instance& bstring)
		{
			return bstring->hash_impl();
		}
		auto operator()(const boolean_expression::instance& boolexp)
		{
			return boolexp->hash_impl();
		}
	};
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t expressionHash = std::visit(hash_visitor{}, value);
	return ruleHash ^ (expressionHash << 1);
}

} // namespace bpy::ast
