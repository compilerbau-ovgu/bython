#pragma once

#include <optional>
#include <variant>

#include "ast/expression/block_expression.hpp"
#include "ast/function/func_decl_args.hpp"
#include "ast/function/return_type.hpp"
#include "base.hpp"

namespace bpy::ast {

class expression;
class block_expression;

class lambda_expression : public base {
  public:
	using instance = std::shared_ptr<lambda_expression>;

	lambda_expression(base::constructor_access,
					  std::shared_ptr<func_decl_args>,
					  std::optional<std::shared_ptr<return_type>>,
					  std::shared_ptr<block_expression>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const lambda_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	std::string get_type() const;

	auto has_type() const -> bool;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::shared_ptr<func_decl_args> args;
	std::shared_ptr<block_expression> block;

  private:
	explicit lambda_expression(std::shared_ptr<func_decl_args>,
							   std::optional<std::shared_ptr<return_type>>,
							   std::shared_ptr<block_expression>);

	std::optional<std::shared_ptr<return_type>> type;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
