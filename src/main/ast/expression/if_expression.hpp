#pragma once

#include <optional>
#include <variant>

#include "ast/arithmetic_expression/boolean_expression.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/expression/else_expression.hpp"
#include "base.hpp"

namespace bpy::ast {

class else_expression;
class block_expression;

class if_expression : public base {
  public:
	using instance = std::shared_ptr<if_expression>;

	if_expression(base::constructor_access,
				  std::shared_ptr<boolean_expression>,
				  std::shared_ptr<block_expression>,
				  std::shared_ptr<else_expression>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const if_expression&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto collect_bodies() const
		-> std::vector<std::shared_ptr<block_expression>>;

	auto collect_conditions() const
		-> std::vector<std::shared_ptr<boolean_expression>>;

	std::shared_ptr<boolean_expression> ruling;
	std::shared_ptr<block_expression> block;
	std::shared_ptr<else_expression> alternative;

  private:
	explicit if_expression(std::shared_ptr<boolean_expression>,
						   std::shared_ptr<block_expression>,
						   std::shared_ptr<else_expression>);

	auto collect_bodies(std::vector<std::shared_ptr<block_expression>>&) const
		-> void;

	auto
	collect_conditions(std::vector<std::shared_ptr<boolean_expression>>&) const
		-> void;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
