#pragma once

#include <optional>
#include <variant>

#include "ast/arithmetic_expression/boolean_expression.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/statement/else_statement.hpp"
#include "base.hpp"

namespace bpy::ast {

class else_statement;

class if_statement : public base {
  public:
	using instance = std::shared_ptr<if_statement>;

	if_statement(base::constructor_access,
				 std::shared_ptr<boolean_expression>,
				 std::shared_ptr<block_expression>,
				 std::optional<std::shared_ptr<else_statement>>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const if_statement&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::shared_ptr<boolean_expression> ruling;
	std::shared_ptr<block_expression> block;
	std::optional<std::shared_ptr<else_statement>> alternative;

	auto collect_bodies() const
		-> std::vector<std::shared_ptr<block_expression>>;

	auto collect_conditions() const
		-> std::vector<std::shared_ptr<boolean_expression>>;

  private:
	explicit if_statement(std::shared_ptr<boolean_expression>,
						  std::shared_ptr<block_expression>,
						  std::optional<std::shared_ptr<else_statement>>);

	auto collect_bodies(std::vector<std::shared_ptr<block_expression>>&) const
		-> void;

	auto
	collect_conditions(std::vector<std::shared_ptr<boolean_expression>>&) const
		-> void;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
