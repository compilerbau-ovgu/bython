/// \file

#include "ast/statement/let_statement.hpp"

namespace bpy::ast {

let_statement::let_statement(base::constructor_access /*unnamed*/,
							 std::shared_ptr<variable_name> _var_name,
							 std::optional<std::shared_ptr<type_name>> _type,
							 std::shared_ptr<expression> _expression)
	: let_statement{std::move(_var_name), std::move(_type),
					std::move(_expression)}
{
}

let_statement::let_statement(std::shared_ptr<variable_name> _var_name,
							 std::optional<std::shared_ptr<type_name>> _type,
							 std::shared_ptr<expression> _expression)
	: base{RuleName::LetStatement}
	, var_name{std::move(_var_name)}
	, type{std::move(_type)}
	, right_hand{std::move(_expression)}
{
}

/**
 * represents a Bython AST rule of a Let Statement Rule.
 * A Let Statement is a left side assigned variable and start with a 'let', followed by a variable name and optionally
 * by his type  name (for type hinting) and a assignement expression.
 * @param ptr the given PEG AST representation
 * @return a Bython AST statement representation
 */
auto let_statement::create(const std::shared_ptr<peg::Ast>& ptr)
	-> let_statement::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& var_name_ast = ast.nodes.front();
	const std::shared_ptr<peg::Ast>& expression_ast = ast.nodes.back();

	const std::shared_ptr<peg::Ast>& typename_ast = ast.nodes[1];
	std::optional<std::shared_ptr<type_name>> typename_opt = std::nullopt;
	if (typename_ast->original_name == "TypeName") {
		typename_opt = type_name::create(typename_ast);
	}
	return std::make_shared<let_statement>(
		base::constructor_access{}, variable_name::create(var_name_ast),
		typename_opt, expression::create(expression_ast));
}

/**
 * compares two let statement with each other
 * @return always false, let statements cant be equal
 */
auto let_statement::operator==(const let_statement& /*unused*/) const -> bool
{
	return false;
}

/**
 * returns all dependencies of a let statement, usually the dependencies of the right side
 * @return an unordered set of the dependencies
 */
auto let_statement::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag,
				   [&]() { base::dependencies_ = right_hand->dependencies(); });
	return base::dependencies_;
}

/**
 * type hinting is a feature of bython but optional for a statement, so it returns true if a type hint is available
 * @return true if a type hint is available, false otherwise
 */
auto let_statement::has_type_hint() const -> bool
{
	return type != std::nullopt;
}

/**
 * get the variable name of the let statement
 * @return the var name
 */
const std::string& let_statement::get_var_name() const
{
	return var_name->get_name();
}

/**
 * if a type hint is available, get the type name
 * @return the typename if a type hint ist present, otherwise empty
 */
std::string let_statement::get_type_name() const
{
	return type.has_value() ? type.value()->get_name() : "";
}

std::shared_ptr<type_name> let_statement::get_actual_type_name() const
{
	return *type;
}

/**
 * a Let Statement is valid, if it contains at least two childs.
 * @param ptr the Peg AST representation to check
 */
auto let_statement::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() > 3 || ptr->nodes.size() < 2) {
		throw std::invalid_argument{
			std::string{"LetStatement expected to have two or three child."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		const std::shared_ptr<peg::Ast>& typename_ast = ptr->nodes[1];
		if (typename_ast->original_name != "TypeName") {
			throw std::invalid_argument{
				std::string{
					"LetStatement expected TypeName after VariableName\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
	const std::shared_ptr<peg::Ast>& var_name_ast = ptr->nodes.front();
	if (var_name_ast->original_name != "VariableName") {
		throw std::invalid_argument{
			std::string{"LetStatement expected VariableName.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& expression_ast = ptr->nodes.back();
	if (expression_ast->original_name != "Expression") {
		throw std::invalid_argument{
			std::string{"LetStatement expected Expression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * generates a hash from the right hand side and the rule
 * @return a generated has value
 */
auto let_statement::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t letStatementHash =
		ruleHash ^
		(var_name->hash_impl() ^ (right_hand->hash_impl() << 1) << 1);

	if (type) {
		letStatementHash ^= type->get()->hash_impl();
	}
	return letStatementHash;
}
} // namespace bpy::ast
