/// \file

#include "statement.hpp"

namespace bpy::ast {

statement::statement(base::constructor_access /*unnamed*/,
					 statement_type _statements)
	: statement{std::move(_statements)}
{
}

statement::statement(statement_type _statements)
	: base{RuleName::Statement}, statements{std::move(_statements)}
{
}

/**
 * returns a Bython Statemtn AST representation of the given PEG AST representation.
 * A Statement is the abstraction of the following rules: @class if_statement  @class block_expression
 *  @class call_expression  @class let_statement
 * @param ptr the given PEG AST representation
 * @return a Bython AST statement representation
 */
auto statement::create(const std::shared_ptr<peg::Ast>& ptr)
	-> statement::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& first_child = ast.nodes.front();
	std::string child_name = first_child->original_name;

	auto statement_ = statement_type{};

	if (child_name == "IfStatement") {
		statement_ = if_statement::create(first_child);
	}
	else if (child_name == "BlockExpression") {
		statement_ = block_expression::create(first_child);
	}
	else if (child_name == "CallExpression") {
		statement_ = call_expression::create(first_child);
	}
	else if (child_name == "LetStatement") {
		statement_ = let_statement::create(first_child);
	}
	else {
		throw std::invalid_argument{
			std::string{"Statement  <- (IfStatement / BlockExpression / "
						"CallExpression / LetStatement) ';' %ows\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}

	return std::make_shared<statement>(base::constructor_access{}, statement_);
}

/**
 * a statement cant be compared
 * @return always false
 */
auto statement::operator==(const statement & /*unused*/) const -> bool
{
	return false;
}

/**
 * the dependencies of a statement are the dependencies of the child
 * @return
 */
auto statement::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	struct dependency_visitor {
		auto operator()(const if_statement::instance& if_stmt)
		{
			return if_stmt->dependencies();
		}
		auto operator()(const block_expression::instance& block)
		{
			return block->dependencies();
		}
		auto operator()(const call_expression::instance& call)
		{
			return call->dependencies();
		}
		auto operator()(const let_statement::instance& let)
		{
			return let->dependencies();
		}
	};

	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {std::visit(dependency_visitor{}, statements)};
	});
	return base::dependencies_;
} // namespace bpy::ast


/**
 * checks if the given Statement matches the rules. A statement is valid if any of the child AST rules matches
 * @param ptr the given PEG AST representation to check
 * @throws std::invalid_argument if none of the child statements are matching
 */
auto statement::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (ptr->nodes.size() != 1) {
		throw std::invalid_argument{
			std::string{"Statement expected to have one child\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * generate a hash value from the child statement
 * @return a generated hash value
 */
// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto statement::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const if_statement::instance& i) noexcept
		{
			return i->hash_impl();
		}
		auto operator()(const block_expression::instance& b) noexcept
		{
			return b->hash_impl();
		}
		auto operator()(const call_expression::instance& c) noexcept
		{
			return c->hash_impl();
		}

		auto operator()(const let_statement::instance& l) noexcept
		{
			return l->hash_impl();
		}
	};

	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t statementHash = std::visit(hash_visitor{}, statements);
	return ruleHash ^ (statementHash << 1);
}

} // namespace bpy::ast
