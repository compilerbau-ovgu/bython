/// \file

#include "ast/statement/else_statement.hpp"

namespace bpy::ast {

else_statement::else_statement(base::constructor_access /*unnamed*/,
							   else_statement_type value_)
	: else_statement{std::move(value_)}
{
}

else_statement::else_statement(else_statement_type value_)
	: base{RuleName::ElseStatement}, value{std::move(value_)}
{
}

/**
 * creates a Bython AST representation of a nested else if statement.
 * @param ptr the corresponding PEG AST rule class
 * @return the Bython AST shared pointer class
 */
auto else_statement::create(const std::shared_ptr<peg::Ast>& ptr)
	-> else_statement::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& first_child = ast.nodes.front();
	std::string child_name = first_child->original_name;
	if (child_name == "IfStatement") {
		return std::make_shared<else_statement>(
			base::constructor_access{},
			else_statement_type(if_statement::create(first_child)));
	}
	if (child_name == "BlockExpression") {
		return std::make_shared<else_statement>(
			base::constructor_access{},
			else_statement_type(block_expression::create(first_child)));
	}
	throw std::invalid_argument{
		std::string{
			"ElseStatement <- 'else' %ows (IfStatement / BlockExpression)\n"} +
		std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
}

/**
 * compares two else statements with each other
 * @return always false, cant be equal
 */
auto else_statement::operator==(const else_statement& /*unused*/) const -> bool
{
	return false;
}

/**
 * the childs dependencies for this class are: @class if_statement @class block_expression
 * @return a unordered set with the child dependencies
 */
auto else_statement::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	struct dep_visitor {
		auto operator()(const if_statement::instance& f)
		{
			return f->dependencies();
		}

		auto operator()(const block_expression::instance& b)
		{
			return b->dependencies();
		}
	};
	std::call_once(dependency_cache_flag,
				   [&]() { dependencies_ = std::visit(dep_visitor{}, value); });
	return base::dependencies_;
}

/**
 * checks if the PEG AST are valid
 * @param ptr the PEG AST class to check
 */
auto else_statement::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() != 1) {
		throw std::invalid_argument{
			std::string{"ElseStatement <- 'else' %ows (IfStatement / "
						"BlockExpression)\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}
/**
 * returns a hash value from the else statemenmt value
 * @return the generated hash
 */
auto else_statement::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t elseStatementHash = std::hash<else_statement_type>{}(value);
	return ruleHash ^ (elseStatementHash << 1);
}
} // namespace bpy::ast
