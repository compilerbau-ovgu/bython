/// \file

#include "ast/statement/if_statement.hpp"

namespace bpy::ast {

if_statement::if_statement(
	base::constructor_access /*unnamed*/,
	std::shared_ptr<boolean_expression> _ruling,
	std::shared_ptr<block_expression> _block,
	std::optional<std::shared_ptr<else_statement>> _alternative)
	: if_statement{std::move(_ruling), std::move(_block),
				   std::move(_alternative)}
{
}

if_statement::if_statement(
	std::shared_ptr<boolean_expression> _ruling,
	std::shared_ptr<block_expression> _block,
	std::optional<std::shared_ptr<else_statement>> _alternative)
	: base{RuleName::IfStatement}
	, ruling{std::move(_ruling)}
	, block{std::move(_block)}
	, alternative{std::move(_alternative)}
{
}

/**
 * represents a Bython AST rule for a if statement control flow consisting of an if condition, the body and with an
 * optional else
 * @param ptr the PEG AST rule
 * @return a shared pointer with the created Bython AST representation
 */
auto if_statement::create(const std::shared_ptr<peg::Ast>& ptr)
	-> if_statement::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& else_ast = ast.nodes.back();
	std::optional<std::shared_ptr<else_statement>> else_opt = std::nullopt;
	if (else_ast->original_name == "ElseStatement") {
		else_opt = else_statement::create(else_ast);
	}

	return std::make_shared<if_statement>(
		base::constructor_access{},
		boolean_expression::create(ast.nodes.front()),
		block_expression::create(ast.nodes[1]),
		else_opt);
}

/**
 * compares two if statement with each other
 * @return always false, if statements cant be equal
 */
auto if_statement::operator==(const if_statement& /*unused*/) const -> bool
{
	return false;
}

/**
 * get the rule dependencies
 * @return a unordered set with all dependencies
 */
auto if_statement::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		{
			auto return_type_deps = ruling->dependencies();
			dependencies_.merge(return_type_deps);
		}
		{
			auto return_type_deps = block->dependencies();
			dependencies_.merge(return_type_deps);
		}
		{
			if (alternative) {
				auto return_type_deps = (*alternative)->dependencies();
				dependencies_.merge(return_type_deps);
			}
		}
	});
	return base::dependencies_;
}

/**
 * the rule is valid, if it has 2 or 3 childs, representing the condition, the body and the else block
 * @param ptr the PEG AST class to check
 */
auto if_statement::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	if (ptr->nodes.size() > 3 || ptr->nodes.size() < 2) {
		throw std::invalid_argument{
			std::string{"IfStatement expected to have two or three children."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& var_name_ast = ptr->nodes.front();
	if (var_name_ast->original_name != "BooleanExpression") {
		throw std::invalid_argument{
			std::string{"IfStatement expected BooleanExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& expression_ast = ptr->nodes[1];
	if (expression_ast->original_name != "BlockExpression") {
		throw std::invalid_argument{
			std::string{"IfStatement expected BlockExpression.\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		const std::shared_ptr<peg::Ast>& else_ast = ptr->nodes.back();
		if (else_ast->original_name != "ElseStatement") {
			throw std::invalid_argument{
				std::string{"IfStatement expected ElseExpression.\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

/**
 * generates a hash from the rule, the condfition, the body and optional the else
 * @return a generated hash value
 */
auto if_statement::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t rulingHash = ruling->hash_impl();
	std::size_t blockHash = block->hash_impl();
	std::size_t alternativeHash = (*alternative)->hash_impl();
	// hippity hoppity, lets shift these property
	return ruleHash ^
		   (rulingHash ^ (blockHash ^ (alternativeHash << 1) << 1) << 1);
}

/**
 * returns the n conditions from the if statement
 */
auto if_statement::collect_conditions(
	std::vector<boolean_expression::instance>& accum) const -> void
{
	if (alternative == std::nullopt) {
		return;
	}

	const auto& else_stmt = *alternative;

	if (const auto else_if_stmt =
			std::get_if<if_statement::instance>(&else_stmt->value);
		else_if_stmt != nullptr) {
		accum.push_back((*else_if_stmt)->ruling);
		(*else_if_stmt)->collect_conditions(accum);
	}
}
/**
 * returns the n conditions from the if statement
 * @return a vector with all conditions
 */
auto if_statement::collect_conditions() const
	-> std::vector<boolean_expression::instance>
{
	auto conds = std::vector<boolean_expression::instance>(0);
	conds.push_back(ruling);

	collect_conditions(conds);
	return conds;
}


/**
 * returns a vector with all bodies of the if statement
 * @param accum the vector with the previous bodies
 */
auto if_statement::collect_bodies(
	std::vector<block_expression::instance>& accum) const -> void
{
	if (alternative == std::nullopt) {
		return;
	}

	const auto& else_stmt = *alternative;

	if (const auto else_if_stmt =
			std::get_if<if_statement::instance>(&else_stmt->value);
		else_if_stmt != nullptr) {
		accum.push_back((*else_if_stmt)->block);
		(*else_if_stmt)->collect_bodies(accum);
	}

	else {
		const auto else_expr =
			std::get<block_expression::instance>(else_stmt->value);
		accum.push_back(else_expr);
	}
}

/**
 * returns a vector with all bodies of the if statement
 */
auto if_statement::collect_bodies() const
	-> std::vector<block_expression::instance>
{
	auto bodies = std::vector<block_expression::instance>(0);
	bodies.push_back(block);

	collect_bodies(bodies);
	return bodies;
}

} // namespace bpy::ast
