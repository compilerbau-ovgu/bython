#pragma once

#include <variant>

#include "../type_system.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/statement/if_statement.hpp"
#include "base.hpp"

namespace bpy::ast {

class if_statement;
class block_expression;

class else_statement : public base {
  public:
	using instance = std::shared_ptr<else_statement>;
	using else_statement_type = std::variant<std::shared_ptr<if_statement>,
											 std::shared_ptr<block_expression>>;

	else_statement(base::constructor_access, else_statement_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const else_statement&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	else_statement_type value;

  private:
	explicit else_statement(else_statement_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
