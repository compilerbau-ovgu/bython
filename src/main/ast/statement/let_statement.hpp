#pragma once

#include <optional>
#include <variant>

#include "../type_system.hpp"
#include "ast/expression/expression.hpp"
#include "ast/type_name.hpp"
#include "ast/variable_name.hpp"
#include "base.hpp"

namespace bpy::ast {

class expression;

class let_statement : public base {
  public:
	using instance = std::shared_ptr<let_statement>;

	let_statement(base::constructor_access,
				  std::shared_ptr<variable_name>,
				  std::optional<std::shared_ptr<type_name>>,
				  std::shared_ptr<expression>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const let_statement&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto has_type_hint() const -> bool;
	const std::string& get_var_name() const;
	std::string get_type_name() const;
	std::shared_ptr<type_name> get_actual_type_name() const;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::shared_ptr<expression> right_hand;

  private:
	explicit let_statement(std::shared_ptr<variable_name>,
						   std::optional<std::shared_ptr<type_name>>,
						   std::shared_ptr<expression>);

	std::shared_ptr<variable_name> var_name;
	std::optional<std::shared_ptr<type_name>> type;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
