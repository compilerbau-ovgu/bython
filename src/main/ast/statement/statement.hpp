#pragma once

#include "ast/base.hpp"
#include "ast/expression/block_expression.hpp"
#include "ast/expression/call_expression.hpp"
#include "ast/statement/if_statement.hpp"
#include "ast/statement/let_statement.hpp"

namespace bpy::ast {

class block_expression;
class let_statement;
class if_statement;

class statement : public base {
  public:
	using instance = std::shared_ptr<statement>;
	using statement_type = std::variant<std::shared_ptr<if_statement>,
										std::shared_ptr<block_expression>,
										std::shared_ptr<call_expression>,
										std::shared_ptr<let_statement>>;
	statement(base::constructor_access, statement_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const statement&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	statement_type statements;

  private:
	explicit statement(statement_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
