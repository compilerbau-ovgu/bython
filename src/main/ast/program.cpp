/// \file

#include "ast/program.hpp"

namespace bpy::ast {

program::program(base::constructor_access /*unnamed*/,
				 std::vector<program_type> _contents)
	: program{std::move(_contents)}
{
}

program::program(std::vector<program_type> _contents)
	: base{RuleName::Program}, contents{std::move(_contents)}
{
}

/**
 * a program is the top level definition of Bython.
 * It can contain 1 to n "Record" and 1 to n "Function"
 * @param ptr the pointer to the corresponding AST
 * @return a Bython AST representation of the given program
 */
auto program::create(const std::shared_ptr<peg::Ast>& ptr) -> program::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	const std::shared_ptr<peg::Ast>& first_child = ast.nodes.front();
	std::vector<program_type> contents;
	for (auto& node : ast.nodes) {
		if (node->original_name == "Record") {
			contents.emplace_back(record::create(node));
		}
		else {
			contents.emplace_back(function::create(node));
		}
	}
	return std::make_shared<program>(base::constructor_access{}, contents);
}

/**
 * returns alsways @code false, programs cant be equal
 */
auto program::operator==(const program& other) const -> bool
{
	return false;
}

/**
 * the dependencies of a program are the records and the functions.
 * @return a set with all of the dependencies
 */
auto program::dependencies() const -> const std::unordered_set<base::instance>&
{
	struct dependency_visitor {
		auto operator()(const function::instance& f)
		{
			return std::dynamic_pointer_cast<ast::base>(f);
		}
		auto operator()(const record::instance& r)
		{
			return std::dynamic_pointer_cast<ast::base>(r);
		}
	};

	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {};
		std::transform(
			std::begin(contents), std::end(contents),
			std::inserter(base::dependencies_, std::begin(base::dependencies_)),
			[](const auto& content) {
				return std::dynamic_pointer_cast<ast::base>(
					std::visit(dependency_visitor{}, content));
			});
	});
	return base::dependencies_;
}

/**
 * a program is invalid if there are other things then a Function and a Record
 * @param ptr the pointer to the corresponding AST
 */
auto program::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	std::vector<std::shared_ptr<peg::Ast>> nodes = ptr->nodes;
	for (std::shared_ptr<peg::Ast>& node : nodes) {
		if (!(node->original_name == "Function" ||
			  node->original_name == "Record")) {
			throw std::invalid_argument{
				std::string{"Program expected to contain only Functions and "
							"Records\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
}

/**
 * A program Hash consists of the hash values of all his children
 * @return the generated hash
 */
auto program::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const function::instance& f) noexcept
		{
			return f->hash_impl();
		}

		auto operator()(const record::instance& r) noexcept
		{
			return r->hash_impl();
		}
	};

	std::size_t ruleNameHash = std::hash<RuleName>{}(rule_name);
	std::size_t programHash = 0;
	for (const program::program_type& programType : contents) {
		programHash ^= std::visit(hash_visitor{}, programType) << 1;
	}
	return ruleNameHash ^ (programHash << 1);
}

/**
 * get a specific function or record from the program
 * @param name the function/record name
 * @return the instance or null if not present
 */
auto program::get(const base::instance& name) -> base::instance
{
	if (const ast::function_name::instance name_ptr =
			std::dynamic_pointer_cast<ast::function_name>(name)) {
		for (program::program_type programType : contents) {
			if (const ast::function::instance* funciton_ptr =
					std::get_if<ast::function::instance>(&programType)) {
				if ((*funciton_ptr)->get_function_name()->get_name() ==
					name_ptr->get_name()) {
					return *funciton_ptr;
				}
			}
		}
	}
	if (const ast::type_name::instance name_ptr =
			std::dynamic_pointer_cast<ast::type_name>(name)) {
		for (program::program_type programType : contents) {
			if (const ast::record::instance* record_ptr =
					std::get_if<ast::record::instance>(&programType)) {
				if (ast::identical_to()((*record_ptr)->get_type(), name_ptr)) {
					return *record_ptr;
				}
			}
		}
	}
	return nullptr;
}
} // namespace bpy::ast
