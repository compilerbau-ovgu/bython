/// \file

#include "record_member.hpp"

namespace bpy::ast {

record_member::record_member(base::constructor_access /*unused*/,
							 variable_name::instance var_name_,
							 type_name::instance type_name_)
	: record_member{std::move(var_name_), std::move(type_name_)}
{
}

record_member::record_member(variable_name::instance var_name_,
							 type_name::instance type_name_)
	: vname{std::move(var_name_)}
	, tname{std::move(type_name_)}
	, base{RuleName::RecordMember}
{
}

/**
 * represents the Bython AST rule RecordMember. A Record member is composed of a variable name and a type name.
 * @param ptr the corresponding PEG AST rule
 * @return a created Bython AST rule
 */
auto record_member::create(const std::shared_ptr<peg::Ast>& ptr)
	-> record_member::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<record_member>(base::constructor_access{},
										   variable_name::create(ast.nodes[0]),
										   type_name::create(ast.nodes[1]));
}

/**
 * compares two record members with each other
 * @return always false, cant be equal
 */
auto record_member::operator==(const record_member& /*unused*/) const -> bool
{
	return false;
}

/**
 * returns the rule dependencies
 * @return a unordered set with the rule dependencies
 */
auto record_member::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag,
				   [&]() { base::dependencies_ = {tname}; });
	return base::dependencies_;
}

/**
 * convenient method for returning the record member var name
 * @return the variable name
 */
const std ::string& record_member::get_var_name() const
{
	return vname->get_name();
}

/**
 * convenient method for returning the record member type name
 * @return the type name
 */
const std ::string& record_member::get_type_name() const
{
	return tname->get_name();
}

/**
 * checks if the Peg AST rule is valid. A record member is valid if it contains two childre, VariableName and TypeName
 * @param ptr
 */
auto record_member::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;

	if (!(std::size(ast.nodes) != 2 ||
		  ast.nodes[0]->original_name == "VariableName" ||
		  ast.nodes[1]->original_name == "TypeName")) {
		throw std::invalid_argument{
			std::string{"Expected: VariableName ':' TypeName\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * returns a hash value from the record member and its child
 * @return the generated hash
 */
auto record_member::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t variableNameHash = vname->hash_impl();
	std::size_t typeNameHash = tname->hash_impl();
	return ruleHash ^ (variableNameHash ^ (typeNameHash << 1) << 1);
}

} // namespace bpy::ast
