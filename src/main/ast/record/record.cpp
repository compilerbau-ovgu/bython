/// \file

#include "record.hpp"

namespace bpy::ast {
record::record(base::constructor_access /*unused*/,
			   type_name::instance name_,
			   record_body::instance body_)
	: record{std::move(name_), std::move(body_)}
{
}

record::record(type_name::instance name_, record_body::instance body_)
	: type{std::move(name_)}, body{std::move(body_)}, base{RuleName::Record}
{
}

/**
 * represents a Bython AST Record rule. A Record is a struct like component that contains 1 RecordBody and 1 to n record members
 * @param ptr the PEG AST rule to transform
 * @return a instance of this rule
 */
auto record::create(const std::shared_ptr<peg::Ast>& ptr) -> record::instance
{
	throw_if_bad_ast(ptr);
	const auto& ast = *ptr;

	return std::make_shared<record>(base::constructor_access{},
									type_name::create(ast.nodes[0]),
									record_body::create(ast.nodes[1]));
}

/**
 * compares two records with eachother. A record is equal if their types are equal
 * @param other the record to compare to
 * @return true if equal, false otherwise
 */
auto record::operator==(const record& other) const -> bool
{
	return *type == *other.type;
}

/**
 * returns the dependencies of a record depoending on the record body dependencies
 * @return an unordered set with the dependencies
 */
auto record::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag,
				   [&]() { dependencies_ = body->dependencies(); });
	return dependencies_;
}

/**
 * returns the type of this record
 * @return
 */
auto record::get_type() const -> const type_name::instance&
{
	return type;
}

/**
 * returns the type name of this record
 * @return
 */
const std::string& record::get_type_name() const
{
	return type->get_name();
}

/**
 * checks if the record is valid. A record is valid if it contains only the type name and the record body
 * @param ptr the PEG AST Rule to check against
 */
auto record::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	// Record should match TypeName, then RecordBody
	if (!(std::size(ast.nodes) != 2 ||
		  ast.nodes[0]->original_name == "TypeName" ||
		  ast.nodes[1]->original_name == "RecordBody")) {
		throw std::invalid_argument{
			std::string{"Expected: Record := TypeName, RecordBody\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// Must force hash collisions between call_expression and records
auto record::hash_impl() const noexcept -> std::size_t
{
	std::size_t tname = type->hash_impl();
	std::size_t member_count =
		std::hash<std::size_t>{}(std::size(body->members));

	return tname ^ (member_count << 1);
}
} // namespace bpy::ast
