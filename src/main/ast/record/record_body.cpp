/// \file

#include "record_body.hpp"

#include <algorithm>
#include <numeric>

namespace bpy::ast {

record_body::record_body(base::constructor_access /*unused*/,
						 std::vector<record_member::instance> members_)
	: record_body{std::move(members_)}
{
}

record_body::record_body(std::vector<record_member::instance> members_)
	: members{std::move(members_)}, base{RuleName::RecordBody}
{
}

auto record_body::operator==(const record_body & /*unused*/) const -> bool
{
	return false;
}

/**
 * Represents the Bython RecordBody Rule. A Record Body contains 1 to n Member
 * @param ptr the corresponding PEG AST class
 * @return a instance of Recor Body
 */
auto record_body::create(const std::shared_ptr<peg::Ast>& ptr)
	-> record_body::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	auto record_members = decltype(members){std::size(ast.nodes)};

	std::transform(
		std::begin(ast.nodes), std::end(ast.nodes), std::begin(record_members),
		[](const auto& node) { return record_member::create(node); });

	return std::make_shared<record_body>(base::constructor_access{},
										 record_members);
}

/**
 * the dependencies of RecordBody depends on the dependencies of each RecordMember
 * @return a unordered set with all dependencies
 */
auto record_body::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		using value_type = std::unordered_set<base::instance>;

		base::dependencies_ = std::accumulate(
			std::begin(members), std::end(members), value_type{},
			[&](value_type accum, auto member) {
				auto dependencies = member->dependencies();
				accum.insert(std::begin(dependencies), std::end(dependencies));
				return accum;
			});
	});

	return base::dependencies_;
}

/**
 * a record body has to contain at least one RecordMember. There can be nothing else then this class as a Record Body
 * @param ptr the PEG AST class to check
 */
auto record_body::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	// May only contain RecordMember
	if (std::size(ast.nodes) == 0) {
		throw std::invalid_argument{
			std::string{"Expected: RecordBody := RecordMember+\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}

	bool all_record_members = std::all_of(
		std::begin(ast.nodes), std::end(ast.nodes),
		[](const auto& node) { return node->original_name == "RecordMember"; });

	if (!all_record_members) {
		throw std::invalid_argument{
			std::string{"Expected: RecordBody := RecordMember+\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * generates a hash from the rule and its children
 * @return a generated hash value
 */
auto record_body::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t membersHash = 0;
	for (const record_member::instance& member : members) {
		membersHash = membersHash ^ (member->hash_impl() << 1);
	}
	return ruleHash ^ (membersHash << 1);
}

} // namespace bpy::ast
