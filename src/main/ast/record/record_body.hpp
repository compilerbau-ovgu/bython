#pragma once

#include <string>
#include <unordered_set>
#include <vector>

#include "base.hpp"
#include "record_member.hpp"

namespace bpy::ast {
class record_body : public base {
  public:
	using instance = std::shared_ptr<record_body>;

	std::vector<record_member::instance> members;

	record_body(base::constructor_access, std::vector<record_member::instance>);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> record_body::instance;

	auto operator==(const record_body&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit record_body(std::vector<record_member::instance>);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
