#pragma once

#include <peglib.h>
#include <utility>

#include "base.hpp"
#include "record_body.hpp"
#include "type_name.hpp"

namespace bpy::ast {
class record : public base {
  public:
	using instance = std::shared_ptr<record>;

	record_body::instance body;

	record(base::constructor_access,
		   type_name::instance,
		   record_body::instance);

	static auto create(const std::shared_ptr<peg::Ast>&) -> record::instance;

	auto operator==(const record&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto get_type() const -> const type_name::instance&;

	const std::string& get_type_name() const;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	record(type_name::instance, record_body::instance);

	type_name::instance type;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
