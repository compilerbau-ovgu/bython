#pragma once

#include <peglib.h>
#include <string>
#include <unordered_set>
#include <utility>
#include <vector>

#include "base.hpp"
#include "type_name.hpp"
#include "variable_name.hpp"

namespace bpy::ast {
class record_member : public base {
  public:
	using instance = std::shared_ptr<record_member>;

	record_member(constructor_access,
				  variable_name::instance,
				  type_name::instance);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> record_member::instance;

	auto operator==(const record_member&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	const std::string& get_var_name() const;
	const std::string& get_type_name() const;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	record_member(variable_name::instance, type_name::instance);

	variable_name::instance vname;
	type_name::instance tname;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
