#pragma once

#include <string>
#include <unordered_set>

#include "base.hpp"
#include "identifier.hpp"

namespace bpy::ast {

class variable_name : public base {
  public:
	using instance = std::shared_ptr<variable_name>;

	variable_name(base::constructor_access, identifier::instance);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> variable_name::instance;
	auto operator==(const variable_name&) const -> bool;

	const std::string& get_name() const;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto hash_impl() const noexcept -> std::size_t override;

  private:
	explicit variable_name(identifier::instance);

	identifier::instance name;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
