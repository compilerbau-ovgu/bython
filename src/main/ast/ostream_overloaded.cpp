/// \file

#include "ostream_overloaded.hpp"

#include <ostream>
#include <sstream>

#include "arithmetic_expression/arithmetic_expression.hpp"
#include "arithmetic_expression/boolean_expression.hpp"
#include "arithmetic_expression/comparison.hpp"
#include "arithmetic_expression/first_class_arithmetic_expression.hpp"
#include "arithmetic_expression/first_class_boolean_expression.hpp"
#include "arithmetic_expression/none_arithmetic_expression.hpp"
#include "arithmetic_expression/none_boolean_expression.hpp"
#include "base.hpp"
#include "constant/boolean_constant.hpp"
#include "constant/constant.hpp"
#include "constant/floating_point.hpp"
#include "constant/integer.hpp"
#include "constant/numeric.hpp"
#include "constant/string.hpp"
#include "expression/block_expression.hpp"
#include "expression/call_expression.hpp"
#include "expression/callargs.hpp"
#include "expression/else_expression.hpp"
#include "expression/expression.hpp"
#include "expression/if_expression.hpp"
#include "expression/lambda_expression.hpp"
#include "function/constructor.hpp"
#include "function/func_decl_arg.hpp"
#include "function/func_decl_args.hpp"
#include "function/function.hpp"
#include "function/function_name.hpp"
#include "function/return_type.hpp"
#include "identifier.hpp"
#include "ostream_overloaded.hpp"
#include "program.hpp"
#include "record/record.hpp"
#include "record/record_body.hpp"
#include "record/record_member.hpp"
#include "rules.hpp"
#include "statement/else_statement.hpp"
#include "statement/if_statement.hpp"
#include "statement/let_statement.hpp"
#include "statement/statement.hpp"
#include "type_name.hpp"
#include "variable_name.hpp"

namespace bpy::ast {

indentation indentation::operator++()
{
	taps++;
	return *this;
}

indentation indentation::operator--()
{
	taps--;
	return *this;
}

struct ostream_visitor {
	std::ostream& os;
	ostream_visitor(std::ostream& os_) : os{os_}
	{
	}

	template <typename AstClass>
	auto operator()(const std::shared_ptr<AstClass>& ast) -> std::ostream&
	{
		static_assert(std::is_base_of_v<ast::base, AstClass>,
					  "cannot visit class that does not derive from ast::base");
		return os << *ast;
	}
};

auto operator<<(std::ostream& os, const indentation& b) -> std::ostream&
{
	for (int i = 0; i < b.taps; i++) {
		os << "  ";
	}
	return os;
}

indentation indents;

auto operator<<(std::ostream& os, const base::instance& b) -> std::ostream&
{
	if (b->rule_name == RuleName::Record) {
		os << *std::dynamic_pointer_cast<record>(b);
	}

	else if (b->rule_name == RuleName::TypeName) {
		os << *std::dynamic_pointer_cast<type_name>(b);
	}

	else if (b->rule_name == RuleName::FuncDeclArg) {
		os << *std::dynamic_pointer_cast<func_decl_arg>(b);
	}

	else if (b->rule_name == RuleName::Function) {
		os << *std::dynamic_pointer_cast<function>(b);
	}

	else if (b->rule_name == RuleName::FunctionName) {
		os << *std::dynamic_pointer_cast<function_name>(b);
	}

	else {
		os << "unknown rule name: " << rule_name_to_str(b->rule_name)
		   << indents;
	}

	return os;
}

template <typename AstClass>
static auto join(const std::vector<AstClass>& iterable, std::string delim)
	-> std::string
{
	std::stringstream s;
	const auto one_off_last = std::end(iterable) - 1;

	const bool reindent = delim.back() == '\n';

	auto it = std::begin(iterable);
	if (std::size(iterable) > 1) {
		while (it != one_off_last) {
			if (reindent) {
				s << indents;
			}
			s << **it++ << delim;
		}
	}

	if (reindent) {
		s << indents;
	}

	s << **it;
	return s.str();
}

auto operator<<(std::ostream& os, const program& obj) -> std::ostream&
{
	for (auto it = obj.contents.begin(); it != obj.contents.end() - 1; it++) {
		if (std::holds_alternative<std::shared_ptr<function>>(*it)) {
			const auto& func = std::get<std::shared_ptr<function>>(*it);
			os << *func << std::endl << indents;
		}
		else if (std::holds_alternative<std::shared_ptr<record>>(*it)) {
			const auto& rec = std::get<std::shared_ptr<record>>(*it);
			os << *rec << std::endl << indents;
		}
	}
	auto end = *(obj.contents.end() - 1);
	if (std::holds_alternative<std::shared_ptr<function>>(end)) {
		const auto& func = std::get<std::shared_ptr<function>>(end);
		os << *func;
	}
	else if (std::holds_alternative<std::shared_ptr<record>>(end)) {
		const auto& rec = std::get<std::shared_ptr<record>>(end);
		os << *rec;
	}
	os << std::endl;
	return os;
}

auto operator<<(std::ostream& os, const function& obj) -> std::ostream&
{
	os << "func " << obj.get_function_name()->get_name() << " = ";
	if (obj.args) {
		os << **obj.args;
	}
	if (obj.has_type()) {
		os << " -> " << obj.get_return_type_name();
	}
	os << " " << *obj.block;
	return os;
}

auto operator<<(std::ostream& os, const function_name& obj) -> std::ostream&
{
	os << obj.get_name();
	return os;
}

auto operator<<(std::ostream& os, const func_decl_args& obj) -> std::ostream&
{
	if (obj.args != std::nullopt) {
		os << join(*obj.args, ", ");
	}
	return os;
}

auto operator<<(std::ostream& os, const func_decl_arg& obj) -> std::ostream&
{
	os << obj.get_var_name() << ": " << obj.get_type()->get_name();
	return os;
}

auto operator<<(std::ostream& os, const return_type& obj) -> std::ostream&
{
	os << obj.get_name();
	return os;
}

auto operator<<(std::ostream& os, const block_expression& obj) -> std::ostream&
{
	os << "{\n";
	++indents;
	for (const auto& statement : obj.statements) {
		os << indents << *statement << ";\n";
	}

	if (obj.value_expression) {
		os << indents << **obj.value_expression;
	}

	--indents;
	os << std::endl;
	os << indents << "}";
	return os;
}

auto operator<<(std::ostream& os, const statement& obj) -> std::ostream&
{
	std::visit(ostream_visitor{os}, obj.statements);
	return os;
}

auto operator<<(std::ostream& os, const comparison& comp) -> std::ostream&
{
	os << *comp.left << " " << comp.binary_operator << " " << *comp.right;
	return os;
}

auto operator<<(std::ostream& os, const arithmetic_expression& arithm)
	-> std::ostream&
{
	os << *arithm.get_lhs();
	if (arithm.has_rhs()) {
		os << " " << arithm.get_op() << " " << *arithm.get_rhs();
	}

	return os;
}

auto operator<<(std::ostream& os,
				const first_class_arithmetic_expression& arithm)
	-> std::ostream&
{
	os << *arithm.value;
	if (arithm.binary_operator != std::nullopt) {
		os << " " << arithm.binary_operator->first << " "
		   << *arithm.binary_operator->second;
	}

	return os;
}

auto operator<<(std::ostream& os, const none_arithmetic_expression& arithm)
	-> std::ostream&
{
	if (arithm.unary_arithmetic_operator != std::nullopt) {
		os << *arithm.unary_arithmetic_operator;
	}
	std::visit(ostream_visitor{os}, arithm.value);
	return os;
}

auto operator<<(std::ostream& os, const boolean_expression& boolean)
	-> std::ostream&
{
	os << *boolean.get_lhs();
	if (boolean.has_rhs()) {
		os << " " << boolean.get_op() << " " << *boolean.get_rhs();
	}

	return os;
}

auto operator<<(std::ostream& os, const boolean_constant& boolean)
	-> std::ostream&
{
	os << (boolean.value ? "true" : "false");
	return os;
}

auto operator<<(std::ostream& os, const first_class_boolean_expression& boolean)
	-> std::ostream&
{
	os << *boolean.value;
	if (boolean.binary_operator != std::nullopt) {
		os << " " << boolean.binary_operator->first << " "
		   << *boolean.binary_operator->second;
	}

	return os;
}

auto operator<<(std::ostream& os, const none_boolean_expression& boolean)
	-> std::ostream&
{
	if (boolean.unary_boolean_operator != std::nullopt) {
		os << *boolean.unary_boolean_operator;
	}
	std::visit(ostream_visitor{os}, boolean.value);
	return os;
}

auto operator<<(std::ostream& os, const constructor& ctor) -> std::ostream&
{
	os << ctor.get_name() << "::new";
	return os;
}

auto operator<<(std::ostream& os, const call_expression& call) -> std::ostream&
{
	os << call.get_callee_name();
	os << "(";
	if (call.call_args != std::nullopt) {
		os << **call.call_args;
	}
	os << ")";
	return os;
}

auto operator<<(std::ostream& os, const callargs& args) -> std::ostream&
{
	return os << join(args.members, ", ");
}

auto operator<<(std::ostream& os, const expression& obj) -> std::ostream&
{
	std::visit(ostream_visitor{os}, obj.value);
	return os;
}

auto operator<<(std::ostream& os, const lambda_expression& lambda)
	-> std::ostream&
{
	os << *lambda.args;
	if (lambda.has_type()) {
		os << " -> " << lambda.get_type() << " ";
	}
	os << *lambda.block;

	return os;
}

auto operator<<(std::ostream& os, const let_statement& let) -> std::ostream&
{
	os << "let " << let.get_var_name();
	if (let.has_type_hint()) {
		os << ": " << let.get_type_name();
	}
	os << " = " << *let.right_hand;
	return os;
}

auto operator<<(std::ostream& os, const if_statement& ifs) -> std::ostream&
{
	os << "if " << *ifs.ruling << " " << *ifs.block;
	if (ifs.alternative != std::nullopt) {
		os << **ifs.alternative;
	}
	return os;
}

auto operator<<(std::ostream& os, const else_statement& else_s) -> std::ostream&
{
	os << " else ";
	std::visit(ostream_visitor{os}, else_s.value);
	return os;
}

auto operator<<(std::ostream& os, const if_expression& ife) -> std::ostream&
{
	os << "if " << *ife.ruling << " " << *ife.block;
	os << *ife.alternative;
	return os;
}

auto operator<<(std::ostream& os, const else_expression& else_e)
	-> std::ostream&
{
	os << " else ";
	std::visit(ostream_visitor{os}, else_e.value);
	return os;
}

auto operator<<(std::ostream& os, const identifier& id) -> std::ostream&
{
	os << id.ident;
	return os;
}

auto operator<<(std::ostream& os, const record& record) -> std::ostream&
{
	os << "record " << record.get_type_name() << " " << *record.body;
	return os;
}

auto operator<<(std::ostream& os, const record_body& body) -> std::ostream&
{
	os << '{' << std::endl;
	++indents;

	os << join(body.members, ",\n");

	--indents;
	os << std::endl << "}" << std::endl << indents;
	return os;
}

auto operator<<(std::ostream& os, const record_member& member) -> std::ostream&
{
	os << member.get_var_name() << ": " << member.get_type_name();
	return os;
}

auto operator<<(std::ostream& os, const type_name& tn) -> std::ostream&
{
	os << tn.get_name();
	if (tn.has_subtypes()) {
		os << "[";
		os << join(tn.get_subtypes(), ", ");
		os << "]";
	}

	return os;
}

auto operator<<(std::ostream& os, const variable_name& vn) -> std::ostream&
{
	os << vn.get_name();
	return os;
}

auto operator<<(std::ostream& os, const floating_point& fp) -> std::ostream&
{
	return os << fp.value;
}

auto operator<<(std::ostream& os, const integer& i) -> std::ostream&
{
	return os << i.value;
}

auto operator<<(std::ostream& os, const bstring& s) -> std::ostream&
{
	return os << "'" << s.chars << "'";
}

auto operator<<(std::ostream& os, const numeric& n) -> std::ostream&
{
	std::visit(ostream_visitor{os}, n.value);
	return os;
}

auto operator<<(std::ostream& os, const constant& c) -> std::ostream&
{
	std::visit(ostream_visitor{os}, c.value);
	return os;
}

} // namespace bpy::ast
