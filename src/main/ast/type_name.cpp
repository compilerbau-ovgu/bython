/// \file

#include "type_name.hpp"

namespace bpy::ast {
type_name::type_name(base::constructor_access /*unused*/,
					 identifier::instance name_,
					 std::vector<type_name::instance> subtypes_)
	: type_name{std::move(name_), std::move(subtypes_)}
{
}

type_name::type_name(identifier::instance name_,
					 std::vector<type_name::instance> subtypes_)
	: name{std::move(name_)}
	, subtypes{std::move(subtypes_)}
	, base{RuleName::TypeName}
{
}

/**
 * Creates a new representation of the grammatic rule "TypeName".
 * A Type is used for the non primitives, like e.g. a Record. A TypenName can contain 0 to N Subtypes
 *
 * @param ptr the pointer to the corresponding AST
 * @return a shared pointer with the created TypeName and its subtypes
 */
auto type_name::create(const std::shared_ptr<peg::Ast>& ptr)
	-> type_name::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	auto subtypes = std::vector<type_name::instance>{};

	// Will equal 1 when there is only the outer type name
	// Will be greater than 1 when there are subtypes
	// std::cout << ast. << "\n";
	if (std::size(ast.nodes) > 1) {
		const auto& subtype_begin = std::begin(ast.nodes) + 1;
		const auto& subtype_end = std::end(ast.nodes);

		// Reserve space for std::transform
		subtypes.resize(std::size(ast.nodes) - 1);
		std::transform(subtype_begin,
					   subtype_end,
					   std::begin(subtypes),
					   type_name::create);
	}

	return std::make_shared<type_name>(
		base::constructor_access{}, identifier::create(ast.nodes[0]), subtypes);
}

/**
 * checks the equality of two types. A type is equal, if all of his subtypes are equal.
 * @param other the TypeName to check against
 * @return true if equal, false if not
 */
auto type_name::operator==(const type_name& other) const -> bool
{
	return *name == *other.name &&
		   std::equal(
			   std::begin(subtypes),
			   std::end(subtypes),
			   std::begin(other.subtypes),
			   [](const auto& lhs, const auto& rhs) { return *lhs == *rhs; });
}

/**
 * A type name depends on its subtypes, and their subtypes, etc
 * @return the list with all containing dependencies
 */
auto type_name::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		// A type name depends on its subtypes, and their subtypes, etc.

		// 1. gather subtypes that are depended on directly
		std::unordered_set<base::instance> dependencies{subtypes.begin(),
														subtypes.end()};

		// 2. add their dependencies
		for (const auto& subtype : subtypes) {
			auto dep_copy = subtype->dependencies();
			dependencies.merge(dep_copy);
		}

		base::dependencies_ = dependencies;
	});

	return base::dependencies_;
}

/**
 * a check for subtypes
 * @return true if this specific type has subtypes, false if not
 */
auto type_name::has_subtypes() const -> bool
{
	return std::size(subtypes) != 0;
}

/**
 * returns a specific subtype
 * @param index
 * @return the subtype for a given index
 */
auto type_name::get_subtype(std::size_t index) const
	-> const type_name::instance&
{
	return subtypes.at(index);
}

/**
 * returns the identrifcier of the type name
 */
const std::string& type_name::get_name() const
{
	return name->ident;
}

/**
 *
 * @return all subtypes of this TypeName
 */
auto type_name::get_subtypes() const -> const std::vector<type_name::instance>&
{
	return subtypes;
}

/**
 * checks the requirements for a correct rule
 * @param ptr the pointer to the corresponding AST
 */
auto type_name::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	if (ast.nodes.empty() || ast.nodes[0]->original_name != "Identifier") {
		throw std::invalid_argument{
			std::string{"Expected: TypeName := Identifier\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * this hash function includes the member of the class itselfs and all of his childs and subtypes
 * @return a generated hash value
 */
auto type_name::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t childHash = name->hash_impl();

	// we have to hash all over the subtypes
	if (!subtypes.empty()) {
		std::size_t subtypesHash = 0;
		for (const type_name::instance& subtype : subtypes) {
			subtypesHash = subtypesHash ^ (subtype->hash_impl() << 1);
		}

		std::size_t firstHash = ruleHash ^ (childHash << 1);
		return firstHash ^ (subtypesHash << 1);
	}

	return ruleHash ^ (childHash << 1);
}
} // namespace bpy::ast
