#pragma once

#include <string>
#include <unordered_set>

#include "base.hpp"

namespace bpy::ast {
class identifier : public base {
  public:
	using instance = std::shared_ptr<identifier>;

	identifier(base::constructor_access, std::string);

	static auto create(const std::shared_ptr<peg::Ast>&) -> instance;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto hash_impl() const noexcept -> std::size_t override;

	auto operator==(const identifier& rhs) const -> bool;

	std::string ident;

  private:
	explicit identifier(std::string);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};

} // namespace bpy::ast
// namespace bpy::ast
namespace std {
template <>
struct hash<bpy::ast::identifier> {
	using argument_type = bpy::ast::identifier;
	using result_type = std::size_t;
	result_type operator()(argument_type const& s) const noexcept
	{
		return std::hash<std::string>{}(s.ident);
	}
};
} // namespace std
