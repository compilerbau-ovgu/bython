/// \file

#include "base.hpp"

#include "arithmetic_expression/arithmetic_expression.hpp"
#include "arithmetic_expression/boolean_expression.hpp"
#include "arithmetic_expression/comparison.hpp"
#include "arithmetic_expression/first_class_arithmetic_expression.hpp"
#include "arithmetic_expression/first_class_boolean_expression.hpp"
#include "arithmetic_expression/none_arithmetic_expression.hpp"
#include "arithmetic_expression/none_boolean_expression.hpp"
#include "constant/boolean_constant.hpp"
#include "constant/constant.hpp"
#include "constant/floating_point.hpp"
#include "constant/integer.hpp"
#include "constant/numeric.hpp"
#include "constant/string.hpp"
#include "expression/block_expression.hpp"
#include "expression/call_expression.hpp"
#include "expression/callargs.hpp"
#include "expression/else_expression.hpp"
#include "expression/if_expression.hpp"
#include "expression/lambda_expression.hpp"
#include "function/constructor.hpp"
#include "function/func_decl_arg.hpp"
#include "function/func_decl_args.hpp"
#include "function/function.hpp"
#include "function/function_name.hpp"
#include "function/return_type.hpp"
#include "ostream_overloaded.hpp"
#include "program.hpp"
#include "record/record.hpp"
#include "record/record_body.hpp"
#include "record/record_member.hpp"
#include "rules.hpp"
#include "statement/else_statement.hpp"
#include "statement/if_statement.hpp"
#include "statement/let_statement.hpp"
#include "statement/statement.hpp"
#include "type_name.hpp"
#include "variable_name.hpp"

namespace bpy::ast {
base::base(RuleName name) : rule_name{name}
{
}

/**
 *
 * @tparam AstClass The class that the instances lhs and rhs are
 * to be casted to
 * @param lhs An AST instance
 * @param rhs An AST instance
 * @return
 */
template <typename AstClass>
static auto comparator(const ast::base::instance& lhs,
					   const ast::base::instance& rhs) -> bool
{
	static_assert(std::is_base_of_v<base, AstClass>,
				  "Cannot compare class that does not derive from "
				  "bpy::ast::base");

	return (*std::dynamic_pointer_cast<AstClass>(lhs)) ==
		   (*std::dynamic_pointer_cast<AstClass>(rhs));
}

/**
 * Check if two AST instances reference the same entity.
 *
 * Identical memory addresses -> true
 * Identical rule names and operator== returning true -> true
 *
 * NOTE: AST classes such as arithmetic_expression have been implemented so that
 * NOTE: their operator== will always return false, as in this case, every
 * NOTE: arithmetic expression is unique and marking these as identical would
 * NOTE: lead to incorrect codegen
 *
 * @param lhs
 * @param rhs
 * @return true if both AST instances are identical
 */
auto identical_to::operator()(const ast::base::instance& lhs,
							  const ast::base::instance& rhs) const -> bool
{
	// std::cerr << lhs << " == " << rhs << "\n";

	// Naive check: same AST classes passed twice are logically identical
	if (lhs == rhs) {
		return true;
	}
	// Naive check: different rule cannot have same content
	if (lhs->rule_name != rhs->rule_name) {
		return false;
	}

	// Both rules have same name
	switch (lhs->rule_name) {
		case RuleName::Identifier:
			return comparator<ast::identifier>(lhs, rhs);

		case RuleName::Program:
			return comparator<ast::program>(lhs, rhs);

		case RuleName::TypeName:
			return comparator<ast::type_name>(lhs, rhs);

		case RuleName::VariableName:
			return comparator<ast::variable_name>(lhs, rhs);

		case RuleName::ArithmeticExpression:
			return comparator<ast::arithmetic_expression>(lhs, rhs);

		case RuleName::BooleanExpression:
			return comparator<ast::boolean_expression>(lhs, rhs);

		case RuleName::Comparison:
			return comparator<ast::comparison>(lhs, rhs);

		case RuleName::FirstClassArithmeticExpression:
			return comparator<ast::first_class_arithmetic_expression>(lhs, rhs);

		case RuleName::FirstClassBooleanExpression:
			return comparator<ast::first_class_boolean_expression>(lhs, rhs);

		case RuleName::NoneArithmeticExpression:
			return comparator<ast::none_arithmetic_expression>(lhs, rhs);

		case RuleName::NoneBooleanExpression:
			return comparator<ast::none_boolean_expression>(lhs, rhs);

		case RuleName::BooleanConstant:
			return comparator<ast::boolean_constant>(lhs, rhs);

		case RuleName::FloatConstant:
			return comparator<ast::floating_point>(lhs, rhs);

		case RuleName::IntegerConstant:
			return comparator<ast::integer>(lhs, rhs);

		case RuleName::NumericConstant:
			return comparator<ast::numeric>(lhs, rhs);

		case RuleName::StringConstant:
			return comparator<ast::bstring>(lhs, rhs);

		case RuleName::BlockExpression:
			return comparator<ast::block_expression>(lhs, rhs);

		case RuleName::CallExpression:
			return comparator<ast::call_expression>(lhs, rhs);

		case RuleName::CallArgs:
			return comparator<ast::callargs>(lhs, rhs);

		case RuleName::ElseExpression:
			return comparator<ast::else_expression>(lhs, rhs);

		case RuleName::IfExpression:
			return comparator<ast::if_expression>(lhs, rhs);

		case RuleName::LambdaExpression:
			return comparator<ast::lambda_expression>(lhs, rhs);

		case RuleName::Constructor:
			return comparator<ast::constructor>(lhs, rhs);

		case RuleName::FuncDeclArg:
			return comparator<ast::func_decl_arg>(lhs, rhs);

		case RuleName::FuncDeclArgs:
			return comparator<ast::func_decl_args>(lhs, rhs);

		case RuleName::Function:
			return comparator<ast::function>(lhs, rhs);

		case RuleName::FunctionName:
			return comparator<ast::function_name>(lhs, rhs);

		case RuleName::ReturnType:
			return comparator<ast::return_type>(lhs, rhs);

		case RuleName::Record:
			return comparator<ast::record>(lhs, rhs);

		case RuleName::RecordBody:
			return comparator<ast::record_body>(lhs, rhs);

		case RuleName::RecordMember:
			return comparator<ast::record_member>(lhs, rhs);

		case RuleName::ElseStatement:
			return comparator<ast::else_statement>(lhs, rhs);

		case RuleName::IfStatement:
			return comparator<ast::if_statement>(lhs, rhs);

		case RuleName::LetStatement:
			return comparator<ast::let_statement>(lhs, rhs);

		case RuleName::Statement:
			return comparator<ast::statement>(lhs, rhs);

		case RuleName::Expression:
			return comparator<ast::expression>(lhs, rhs);

		case RuleName::Constant:
			return comparator<ast::constant>(lhs, rhs);
	}

	throw std::invalid_argument{std::string{"Unable to find rule: "} +
								rule_name_to_str(lhs->rule_name)};
}

template <typename AstClass>
static auto hasher(const ast::base::instance& ast) noexcept -> std::size_t
{
	return std::dynamic_pointer_cast<AstClass>(ast)->hash_impl();
}

auto hash_by::operator()(const ast::base::instance& ast) const -> std::size_t
{
	switch (ast->rule_name) {
		case RuleName::Identifier:
			return hasher<ast::identifier>(ast);

		case RuleName::Program:
			return hasher<ast::program>(ast);

		case RuleName::TypeName:
			return hasher<ast::type_name>(ast);

		case RuleName::VariableName:
			return hasher<ast::variable_name>(ast);

		case RuleName::ArithmeticExpression:
			return hasher<ast::arithmetic_expression>(ast);

		case RuleName::BooleanExpression:
			return hasher<ast::boolean_expression>(ast);

		case RuleName::Comparison:
			return hasher<ast::comparison>(ast);

		case RuleName::FirstClassArithmeticExpression:
			return hasher<ast::first_class_arithmetic_expression>(ast);

		case RuleName::FirstClassBooleanExpression:
			return hasher<ast::first_class_boolean_expression>(ast);

		case RuleName::NoneArithmeticExpression:
			return hasher<ast::none_arithmetic_expression>(ast);

		case RuleName::NoneBooleanExpression:
			return hasher<ast::none_boolean_expression>(ast);

		case RuleName::BooleanConstant:
			return hasher<ast::boolean_constant>(ast);

		case RuleName::FloatConstant:
			return hasher<ast::floating_point>(ast);

		case RuleName::IntegerConstant:
			return hasher<ast::integer>(ast);

		case RuleName::NumericConstant:
			return hasher<ast::numeric>(ast);

		case RuleName::StringConstant:
			return hasher<ast::bstring>(ast);

		case RuleName::BlockExpression:
			return hasher<ast::block_expression>(ast);

		case RuleName::CallExpression:
			return hasher<ast::call_expression>(ast);

		case RuleName::CallArgs:
			return hasher<ast::callargs>(ast);

		case RuleName::ElseExpression:
			return hasher<ast::else_expression>(ast);

		case RuleName::IfExpression:
			return hasher<ast::if_expression>(ast);

		case RuleName::LambdaExpression:
			return hasher<ast::lambda_expression>(ast);

		case RuleName::Constructor:
			return hasher<ast::constructor>(ast);

		case RuleName::FuncDeclArg:
			return hasher<ast::func_decl_arg>(ast);

		case RuleName::FuncDeclArgs:
			return hasher<ast::func_decl_args>(ast);

		case RuleName::Function:
			return hasher<ast::function>(ast);

		case RuleName::FunctionName:
			return hasher<ast::function_name>(ast);

		case RuleName::ReturnType:
			return hasher<ast::return_type>(ast);

		case RuleName::Record:
			return hasher<ast::record>(ast);

		case RuleName::RecordBody:
			return hasher<ast::record_body>(ast);

		case RuleName::RecordMember:
			return hasher<ast::record_member>(ast);

		case RuleName::ElseStatement:
			return hasher<ast::else_statement>(ast);

		case RuleName::IfStatement:
			return hasher<ast::if_statement>(ast);

		case RuleName::LetStatement:
			return hasher<ast::let_statement>(ast);

		case RuleName::Statement:
			return hasher<ast::statement>(ast);

		case RuleName::Expression:
			return hasher<ast::expression>(ast);

		case RuleName::Constant:
			return hasher<ast::constant>(ast);
	}

	throw std::invalid_argument{"Unable to find rule"};
}
} // namespace bpy::ast
