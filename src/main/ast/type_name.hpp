#pragma once

#include <string>
#include <unordered_set>
#include <vector>

#include "base.hpp"
#include "identifier.hpp"

namespace bpy::ast {
class type_name : public base {
  public:
	using instance = std::shared_ptr<type_name>;

	type_name(base::constructor_access /*unused*/,
			  identifier::instance,
			  std::vector<type_name::instance>);

	static auto create(const std::shared_ptr<peg::Ast>&) -> type_name::instance;

	auto operator==(const type_name&) const -> bool;

	auto has_subtypes() const -> bool;

	auto get_subtype(std::size_t) const -> const type_name::instance&;

	auto get_subtypes() const -> const std::vector<type_name::instance>&;

	const std::string& get_name() const;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	type_name(identifier::instance, std::vector<type_name::instance>);

	identifier::instance name;
	std::vector<type_name::instance> subtypes;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
namespace std {
template <>
struct hash<bpy::ast::type_name> {
	using argument_type = bpy::ast::type_name;
	using result_type = std::size_t;
	result_type operator()(argument_type const& s) const noexcept
	{
		return std::hash<std::string>{}(s.get_name());
	}
};
} // namespace std
