/// \file

#include "ast/function/function.hpp"

namespace bpy::ast {

function::function(base::constructor_access /*unnamed*/,
				   std::shared_ptr<function_name> _name,
				   std::optional<std::shared_ptr<func_decl_args>> _args,
				   std::optional<std::shared_ptr<return_type>> _type,
				   std::shared_ptr<block_expression> _block)
	: function{std::move(_name), std::move(_args), std::move(_type),
			   std::move(_block)}
{
}

function::function(std::shared_ptr<function_name> _name,
				   std::optional<std::shared_ptr<func_decl_args>> _args,
				   std::optional<std::shared_ptr<return_type>> _type,
				   std::shared_ptr<block_expression> _block)
	: base{RuleName::Function}
	, name{std::move(_name)}
	, args{std::move(_args)}
	, type{std::move(_type)}
	, block{std::move(_block)}
{
}

/**
 * creates a instance of the Bython AST Function rule. a function can have different appearance:
 * "func" FunctionName "=" BlockExpression
 * "func" FunctionName "=" "->" ReturnType BlockExpression
 * 'func' FunctionName '=' FuncDeclArgs '->' ReturnType BlockExpression
 * @param ptr the corresponding PEG Ast rule
 * @return a instance representation of the Bython AST
 */
auto function::create(const std::shared_ptr<peg::Ast>& ptr)
	-> function::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	auto fname = function_name::create(ast.nodes[0]);

	int fdas_idx = -1;
	auto fdas = std::optional<func_decl_args::instance>{std::nullopt};

	int ret_type_idx = -1;
	auto ret_type = std::optional<return_type::instance>{std::nullopt};

	int block_expr_idx = -1;
	auto block_expr = block_expression::instance{};

	switch (const auto nodes = ast.nodes; std::size(nodes)) {
		case 2:
			// "func" FunctionName "=" BlockExpression
			block_expr_idx = 1;
			break;

		case 3:
			// "func" FunctionName "=" FuncDeclArgs BlockExpression
			if (nodes[1]->original_name == "FuncDeclArgs") {
				fdas_idx = 1;
			}

			// "func" FunctionName "=" "->" ReturnType BlockExpression
			else {
				ret_type_idx = 1;
			}
			block_expr_idx = 2;
			break;

		case 4:
			// 'func' FunctionName '=' FuncDeclArgs '->' ReturnType BlockExpression
			fdas_idx = 1;
			ret_type_idx = 2;
			block_expr_idx = 3;
	}

	if (fdas_idx >= 0) {
		fdas = func_decl_args::create(ast.nodes[fdas_idx]);
	}
	if (ret_type_idx >= 0) {
		ret_type = return_type::create(ast.nodes[ret_type_idx]);
	}
	if (block_expr_idx >= 0) {
		block_expr = block_expression::create(ast.nodes[block_expr_idx]);
	}

	return std::make_shared<function>(
		base::constructor_access{}, fname, fdas, ret_type, block_expr);
}

/**
 * checks the equality of two functions. Functions are equal if there name, their args and their types are equal
 * @param other the function to compare with
 * @return true if equal, false otherwise
 */
auto function::operator==(const function& other) const -> bool
{
	return *name == *other.name && *args == *other.args && *type == *other.type;
}

/**
 * get the dependencies of a function.
 * The dependencies depends on the return type (if present), the arguments (if any) and the body
 * @return an unordered set with the dependencies
 */
auto function::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		base::dependencies_ = {};

		if (type != std::nullopt) {
			auto return_type_deps = (*type)->dependencies();
			base::dependencies_.merge(return_type_deps);
		}

		if (args != std::nullopt) {
			auto args_deps = (*args)->dependencies();
			base::dependencies_.merge(args_deps);
		}

		auto block_deps = block->dependencies();
		base::dependencies_.merge(block_deps);
	});

	return base::dependencies_;
}

/**
 * get the name of this function
 * @return a function name instance
 */
auto function::get_function_name() const -> const function_name::instance&
{
	return name;
}

/**
 * gets the return type name if present
 * @return the type name if present, else an empty string
 */
std::string function::get_return_type_name() const
{
	return has_type() ? (*type)->get_name() : "";
}

/**
 * returns the return type instance
 * @return the return type
 */
auto function::get_return_type() const -> const type_name::instance&
{
	return (*type)->get_type();
}

/**
 * checks if the function has a return type
 * @return true if a return type is present, false otherwise
 */
auto function::has_type() const -> bool
{
	return type != std::nullopt;
}

/**
 * checks if the function declaration is valid.
 * @param ptr the PEG AST to check against
 */
auto function::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (ptr->nodes.size() > 4 || ptr->nodes.size() < 2) {
		throw std::invalid_argument{
			std::string{"Function expected to have two or three child."} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	if (ptr->nodes.size() == 3) {
		const std::shared_ptr<peg::Ast>& typename_ast = ptr->nodes[1];
		if (!(typename_ast->original_name == "FuncDeclArgs" ||
			  typename_ast->original_name == "ReturnType")) {
			throw std::invalid_argument{
				std::string{
					"Function expected FuncDeclArgs or ReturnType after "
					"FunctionName\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
	if (ptr->nodes.size() == 4) {
		if (ptr->nodes[1]->original_name != "FuncDeclArgs") {
			throw std::invalid_argument{
				std::string{"Function expected FuncDeclArgs\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
		if (ptr->nodes[2]->original_name != "ReturnType") {
			throw std::invalid_argument{
				std::string{
					"Function expected ReturnType after FuncDeclArgs\n"} +
				std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
		}
	}
	const std::shared_ptr<peg::Ast>& var_name_ast = ptr->nodes.front();
	if (var_name_ast->original_name != "FunctionName") {
		throw std::invalid_argument{
			std::string{"Function expected FunctionName\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
	const std::shared_ptr<peg::Ast>& expression_ast = ptr->nodes.back();
	if (expression_ast->original_name != "BlockExpression") {
		throw std::invalid_argument{
			std::string{"Function expected BlockExpression\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// Must force hash collisions between call_expression and function
auto function::hash_impl() const noexcept -> std::size_t
{
	std::size_t fname = std::hash<std::string>{}(name->get_name());

	/*std::size_t arg_count = (args == std::nullopt || (*args)->args ==
	   std::nullopt) ? std::hash<std::size_t>{}(0) :
	   std::hash<std::size_t>{}(std::size(*(*args)->args));*/

	return fname; //^ (arg_count << 1);
}

/**
 * checks if the function has any function arguments
 * @return  true if present, false otherwise
 */
auto function::has_args() const -> bool
{
	return args != std::nullopt && *args != nullptr;
}

/**
 * get the arguments of the function
 * @return a vector with FuncDeclArgs
 */
auto function::get_args() const -> std::vector<func_decl_arg::instance>&
{
	return *args->get()->args;
}

/**
 * get the amount of arguments of the function
 * @return
 */
auto function::arg_size() const -> std::size_t
{
	return std::size(get_args());
}
} // namespace bpy::ast
