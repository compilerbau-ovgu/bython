#pragma once

#include <optional>
#include <vector>

#include "base.hpp"
#include "func_decl_arg.hpp"

namespace bpy::ast {
class func_decl_args : public base {
  public:
	using instance = std::shared_ptr<func_decl_args>;

	func_decl_args(base::constructor_access,
				   std::optional<std::vector<func_decl_arg::instance>>);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> func_decl_args::instance;

	auto operator==(const func_decl_args&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	// std::nullopt signifies Void, otherwise assume valid func_decl_args
	std::optional<std::vector<func_decl_arg::instance>> args;

  private:
	explicit func_decl_args(
		std::optional<std::vector<func_decl_arg::instance>>);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
