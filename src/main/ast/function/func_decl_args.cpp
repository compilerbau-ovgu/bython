/// \file

#include "func_decl_args.hpp"

#include <numeric>

namespace bpy::ast {
func_decl_args::func_decl_args(
	base::constructor_access /*unused*/,
	std::optional<std::vector<func_decl_arg::instance>> args_)
	: func_decl_args{std::move(args_)}
{
}

func_decl_args::func_decl_args(
	std::optional<std::vector<func_decl_arg::instance>> args_)
	: args{std::move(args_)}, base{RuleName::FuncDeclArgs}
{
}

/**
 * creates a Bython AST FuncDeclArgs rule instance. Contains basically only a vecctor of FuncDeclArg
 * @param ptr the PEG AST class to transform
 * @return an instance of the rule
 */
auto func_decl_args::create(const std::shared_ptr<peg::Ast>& ptr)
	-> func_decl_args::instance
{
	throw_if_bad_ast(ptr);
	const auto& ast = *ptr;

	auto args = std::optional<std::vector<func_decl_arg::instance>>{};

	if (std::size(ast.nodes) == 0) {
		args = std::nullopt;
	}
	else {
		auto children = std::vector<func_decl_arg::instance>{};
		std::transform(std::begin(ast.nodes),
					   std::end(ast.nodes),
					   std::back_inserter(children),
					   func_decl_arg::create);

		args = children;
	}

	return std::make_shared<func_decl_args>(base::constructor_access{}, args);
}

/**
 * checks equality of FuncDeclArgs
 * @return always false, cant be equal
 */
auto func_decl_args::operator==(const func_decl_args & /*unused*/) const -> bool
{
	return false;
}

/**
 * get the dependencies of the FuncDeclArgs. Basically the dependencies of the vector entries
 * @return an unordered set with the dependencies
 */
auto func_decl_args::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	// depends on the types in the parameter list
	std::call_once(dependency_cache_flag, [&]() {
		if (args != std::nullopt) {
			const auto& nonnull_args = *args;
			base::dependencies_ =
				std::accumulate(std::begin(nonnull_args),
								std::end(nonnull_args),
								decltype(base::dependencies_){},

								[](decltype(base::dependencies_) dep_accum,
								   const func_decl_arg::instance& fda) {
									auto deps_copy = fda->dependencies();
									dep_accum.merge(deps_copy);
									return dep_accum;
								});
		}
		// dependencies are empty if args == std::nullopt
	});

	return base::dependencies_;
}

/**
 * checks if the rule is valid. This is true, if the FuncDeclArgs contains only 0 to n FuncDeclArg members
 * @param ptr the PEG AST rule to check against
 */
auto func_decl_args::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;

	// actually has parameters
	bool is_args = std::size(ast.nodes) >= 1;
	bool is_void = std::size(ast.nodes) == 0;

	if (is_args) {
		// all these parameters are FuncDeclArgs accordingly
		bool all_fda = std::all_of(
			std::begin(ast.nodes), std::end(ast.nodes), [](const auto& child) {
				return child->original_name == "FuncDeclArg";
			});
		is_args = is_args && all_fda;
	}

	if (bool correct = is_void ^ is_args; !correct) {
		throw std::invalid_argument{
			std::string{"Expected: FuncDeclArgs <- 'Void' / "
						"CommaSeparatedList(FuncDeclArg))\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto func_decl_args::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t funcArgsHash = 0;
	if (args) {
		for (const auto& funcarg : *args) {
			funcArgsHash ^= funcarg->hash_impl() << 1;
		}
	}
	return ruleHash ^ (funcArgsHash << 1);
}
} // namespace bpy::ast
