#pragma once

#include "base.hpp"
#include "type_name.hpp"

namespace bpy::ast {
class constructor : public base {
  public:
	using instance = std::shared_ptr<constructor>;

	constructor(base::constructor_access, type_name::instance name_);

	static auto create(const std::shared_ptr<peg::Ast>&) -> instance;

	auto operator==(const constructor&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	const std::string& get_name() const;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit constructor(type_name::instance name_);

	type_name::instance name;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
namespace std {
template <>
struct hash<bpy::ast::constructor> {
	using argument_type = bpy::ast::constructor;
	using result_type = std::size_t;
	result_type operator()(argument_type const& s) const noexcept
	{
		return std::hash<std::string>{}(s.get_name());
	}
};
} // namespace std
