/// \file

#include "constructor.hpp"

namespace bpy::ast {
constructor::constructor(base::constructor_access /*unused*/,
						 type_name::instance name_)
	: constructor{std::move(name_)}
{
}

constructor::constructor(type_name::instance name_)
	: name{std::move(name_)}, base{RuleName::Constructor}
{
}

/**
 * creates an instance of the Bython Constructor rule which is used for creating Records.
 * @param ptr the PEG ASt class to transform
 * @return an instance of the Bython AST rule
 */
auto constructor::create(const std::shared_ptr<peg::Ast>& ptr)
	-> constructor::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<constructor>(base::constructor_access{},
										 type_name::create(ast.nodes[0]));
}

/**
 * get the dependencies of a constructor. The dependencies depending on the type name dependencies
 * @return a unordered set with the dependencies
 */
auto constructor::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		std::unordered_set<base::instance> dependencies{{name}};
		base::dependencies_ = dependencies;
	});

	return base::dependencies_;
}

/**
 * checks equality of Constructors. They are equal if their names are equal
 * @return true if equal, false otherwise
 */
auto constructor::operator==(const constructor& other) const -> bool
{
	return *name == *other.name;
}

/**
 * get the name of a constructor
 * @return  the name
 */
const std::string& constructor::get_name() const
{
	return name->get_name();
}

/**
 * checks if the constructor is valid. This is the case if it only contains a TypeName
 * @param ptr
 */
auto constructor::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	bool correct =
		std::size(ast.nodes) == 1 && ast.nodes[0]->original_name == "TypeName";

	if (!correct) {
		throw std::invalid_argument{
			std::string{"Expected: Constructor := TypeName::new\n"} +
			std::string{"Got\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto constructor::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t constructorHash = name->hash_impl();
	return ruleHash ^ (constructorHash << 1);
}
} // namespace bpy::ast
