#pragma once

#include <optional>
#include <variant>

#include "base.hpp"
#include "expression/block_expression.hpp"
#include "function/func_decl_args.hpp"
#include "function/function_name.hpp"
#include "function/return_type.hpp"

namespace bpy::ast {

class expression;
class block_expression;

class function : public base {
  public:
	using instance = std::shared_ptr<function>;

	function(base::constructor_access,
			 std::shared_ptr<function_name>,
			 std::optional<std::shared_ptr<func_decl_args>>,
			 std::optional<std::shared_ptr<return_type>>,
			 std::shared_ptr<block_expression>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const function&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto get_function_name() const -> const function_name::instance&;

	std::string get_return_type_name() const;

	auto has_type() const -> bool;
	auto get_return_type() const -> const type_name::instance&;

	auto has_args() const -> bool;

	auto get_args() const -> std::vector<func_decl_arg::instance>&;

	auto arg_size() const -> std::size_t;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::optional<std::shared_ptr<func_decl_args>> args;
	std::shared_ptr<block_expression> block;

  private:
	explicit function(std::shared_ptr<function_name>,
					  std::optional<std::shared_ptr<func_decl_args>>,
					  std::optional<std::shared_ptr<return_type>>,
					  std::shared_ptr<block_expression>);

	std::shared_ptr<function_name> name;
	std::optional<std::shared_ptr<return_type>> type;

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
