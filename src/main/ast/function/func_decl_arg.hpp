#pragma once

#include <memory>

#include "base.hpp"
#include "type_name.hpp"
#include "variable_name.hpp"

namespace bpy::ast {
class func_decl_arg : public base {
  public:
	using instance = std::shared_ptr<func_decl_arg>;

	func_decl_arg(base::constructor_access,
				  variable_name::instance,
				  type_name::instance);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> func_decl_arg::instance;

	auto operator==(const func_decl_arg&) const -> bool;
	auto hash_impl() const noexcept -> std::size_t override;

	const std::string& get_var_name() const;
	auto get_type() const -> const type_name::instance&;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit func_decl_arg(variable_name::instance, type_name::instance);

	variable_name::instance vname;
	type_name::instance tname;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
