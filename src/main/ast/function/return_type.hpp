#pragma once

#include "base.hpp"
#include "type_name.hpp"

namespace bpy::ast {

class return_type : public base {
  public:
	using instance = std::shared_ptr<return_type>;

	return_type(base::constructor_access, type_name::instance);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> return_type::instance;

	auto operator==(const return_type&) const -> bool;

	auto get_type() const -> const type_name::instance&;
	const std::string& get_name() const;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit return_type(type_name::instance);

	type_name::instance name;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
