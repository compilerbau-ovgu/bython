/// \file

#include "return_type.hpp"

namespace bpy::ast {
return_type::return_type(base::constructor_access /*unused*/,
						 type_name::instance name_)
	: return_type{std::move(name_)}
{
}

return_type::return_type(type_name::instance name_)
	: name{std::move(name_)}, base{RuleName::ReturnType}
{
}

/**
 * represents a Bython AST ReturnType rule instance. A return type is used in functions or Lambda Functions
 * @param ptr the PEG AST class to transform
 * @return a instance of this AST rule
 */
auto return_type::create(const std::shared_ptr<peg::Ast>& ptr)
	-> return_type::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	return std::make_shared<return_type>(base::constructor_access{},
										 type_name::create(ast.nodes[0]));
}

/**
 * get the dependencies of a return type. The dependencies depending on the type name dependencies
 * @return a unordered set with the dependencies
 */
auto return_type::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() {
		std::unordered_set<base::instance> dependencies{{name}};

		base::dependencies_ = dependencies;
		auto name_deps = name->dependencies();
		base::dependencies_.merge(name_deps);
	});

	return base::dependencies_;
}

/**
 * checks if a return type is equal to another. The return types are equal if the name is equal
 * @param other the type to ckeck against
 * @return true if equal, false otherwise
 */
auto return_type::operator==(const return_type& other) const -> bool
{
	return *name == *other.name;
}

/**
 * get the name of this return type
 * @return the name
 */
const std::string& return_type::get_name() const
{
	return name->get_name();
}

/**
 * get the type of the return type
 * @return the return type instance
 */
auto return_type::get_type() const -> const type_name::instance& {
    return name;
}

/**
 * a return type is basically a TypeName and can contain nothing more
 * @param ptr the corresponding PEG AST rule to check against
 */
auto return_type::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	bool correct =
		std::size(ast.nodes) == 1 && ast.nodes[0]->original_name == "TypeName";

	if (!correct) {
		throw std::invalid_argument{
			std::string{"Expected: ReturnType := TypeName\n"} +
			std::string{"Got\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto return_type::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t returnTypeHash = name->hash_impl();
	return ruleHash ^ (returnTypeHash << 1);
}
} // namespace bpy::ast
