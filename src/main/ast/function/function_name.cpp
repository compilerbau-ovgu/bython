/// \file

#include "function_name.hpp"

#include "ostream_overloaded.hpp"

namespace bpy::ast {
function_name::function_name(base::constructor_access /*unused*/,
							 identifier::instance name_)
	: function_name{std::move(name_)}
{
}

function_name::function_name(identifier::instance name_)
	: name{std::move(name_)}, base{RuleName::FunctionName}
{
}

/**
 * creates a new instance of the Bython AST Function Name. A Function name is basically an identifier
 * @param ptr
 * @return
 */
auto function_name::create(const std::shared_ptr<peg::Ast>& ptr)
	-> function_name::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<function_name>(base::constructor_access{},
										   identifier::create(ast.nodes[0]));
}

/**
 * compares two function names with each other. The Function names are equal if the names are equal
 * @param other the function name to check against
 * @return true if equal, false otherwise
 */
auto function_name::operator==(const function_name& other) const -> bool
{
	return *name == *other.name;
}

/**
 * returns a empty set because function names doesnt have deps
 * @return a empty unordered set
 */
auto function_name::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

/**
 * get the function name identifier
 * @return the identifier
 */
auto function_name::get_name() const -> const std::string&
{
	return name->ident;
}

/**
 * Checks if a Function name is valid. A Function name is valid if it only contains a Identifier
 * @param ptr the PEG AST rule to check against
 */
auto function_name::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;

	if (ast.nodes.empty()) {
		throw std::invalid_argument{
			std::string{"Expected: FunctionName := Identifier\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}

	if (ast.nodes[0]->original_name != "Identifier") {
		throw std::invalid_argument{
			std::string{"Expected: FunctionName := Identifier\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto function_name::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t functionNameHash = name->hash_impl();
	return ruleHash ^ (functionNameHash << 1);
}
} // namespace bpy::ast
