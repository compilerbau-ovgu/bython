#pragma once

#include "../base.hpp"
#include "../identifier.hpp"

namespace bpy::ast {
class function_name : public base {
  public:
	using instance = std::shared_ptr<function_name>;

	function_name(base::constructor_access, identifier::instance);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> function_name::instance;

	auto operator==(const function_name&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto get_name() const -> const std::string&;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

  private:
	explicit function_name(identifier::instance);

	identifier::instance name;

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void;
};
} // namespace bpy::ast
namespace std {
template <>
struct hash<bpy::ast::function_name> {
	using argument_type = bpy::ast::function_name;
	using result_type = std::size_t;
	result_type operator()(argument_type const& s) const noexcept
	{
		return std::hash<std::string>{}(s.get_name());
	}
};
} // namespace std
