/// \file

#include "func_decl_arg.hpp"

namespace bpy::ast {
func_decl_arg::func_decl_arg(base::constructor_access /*unused*/,
							 variable_name::instance vname_,
							 type_name::instance tname_)
	: func_decl_arg{std::move(vname_), std::move(tname_)}
{
}

func_decl_arg::func_decl_arg(variable_name::instance vname_,
							 type_name::instance tname_)
	: vname{std::move(vname_)}
	, tname{std::move(tname_)}
	, base{RuleName::FuncDeclArg}
{
}

/**
 * creates an instance of the Bython FuncDeclArg rule which is used in function or constructor paramenters.
 * @param ptr the PEG ASt class to transform
 * @return an instance of the Bython AST rule
 */
auto func_decl_arg::create(const std::shared_ptr<peg::Ast>& ptr)
	-> func_decl_arg::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<func_decl_arg>(base::constructor_access{},
										   variable_name::create(ast.nodes[0]),
										   type_name::create(ast.nodes[1]));
}

/**
 * checks equality of FuncDeclArgs
 * @return always false, cant be equal
 */
auto func_decl_arg::operator==(const func_decl_arg& /*unused*/) const -> bool
{
	return false;
}

/**
 * the dependencies are the dependencies of the type name
 * @return an unordered set with the dependencies
 */
auto func_decl_arg::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	// depends on the variable type
	std::call_once(dependency_cache_flag,
				   [&]() { base::dependencies_ = {tname}; });
	auto tname_deps = tname->dependencies();
	base::dependencies_.merge(tname_deps);
	return base::dependencies_;
}

/**
 * returns the variable name of the argument
 * @return the var name
 */
const std::string& func_decl_arg::get_var_name() const
{
	return vname->get_name();
}

/**
 * get the type of the argument
 * @return the type instance
 */
auto func_decl_arg::get_type() const -> const type_name::instance&
{
	return tname;
}

/**
 * checks if the Function argument is valid. This is the case if it contains a VariableName and a TypeName
 * @param ptr the PEG AST rule to check
 */
auto func_decl_arg::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;
	bool correct = std::size(ast.nodes) == 2 &&
				   ast.nodes[0]->original_name == "VariableName" &&
				   ast.nodes[1]->original_name == "TypeName";

	if (!correct) {
		throw std::invalid_argument{
			std::string{
				"Expected: FuncDeclArg := VariableName ':' TypeName\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto func_decl_arg::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	return ruleHash ^ (vname->hash_impl() ^ (tname->hash_impl() << 1) << 1);
}
} // namespace bpy::ast
