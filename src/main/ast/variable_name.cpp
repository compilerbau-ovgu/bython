/// \file

#include "variable_name.hpp"

#include "identifier.hpp"

namespace bpy::ast {
variable_name::variable_name(base::constructor_access /*unused*/,
							 identifier::instance name_)
	: variable_name{std::move(name_)}
{
}

variable_name::variable_name(identifier::instance name_)
	: name{std::move(name_)}, base{RuleName::VariableName}
{
}

/**
 * Creates a new representation of the grammatic rule "VariableName".
 * A variable is an scoped unique identifier and can contain [a-zA-Z_][a-zA-Z0-9_]* signs,
 * except for reserverd words like 'true' / 'false' / 'if' / 'else' / 'func' / 'new'
 *
 * @param ptr the pointer to the corresponding AST
 * @return a shared pointer with the created variable name
 */
auto variable_name::create(const std::shared_ptr<peg::Ast>& ptr)
	-> variable_name::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<variable_name>(base::constructor_access{},
										   identifier::create(ast.nodes[0]));
}

/**
 * compare a variable name with another. they are equal if their name are equal
 * @param other the variable name to compare to
 * @return true if equal, false otherwise
 */
auto variable_name::operator==(const variable_name& other) const -> bool
{
	return *name == *(other.name);
}

/**
 * Dependencies of the AST Rule
 * @return a set of dependencies (dependent Grammatic rules)
 */
auto variable_name::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

/**
 * convenient method for hiding the complexity of the real identifier
 * @return the variables real name, the identifier
 */
const std::string& variable_name::get_name() const
{
	return name->ident;
}

/**
 * checks the requirements for a correct rule
 * @param ptr the pointer to the corresponding AST
 */
auto variable_name::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;

	if (ast.nodes.empty()) {
		throw std::invalid_argument{
			std::string{"Expected: VariableName := Identifier\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}

	if (ast.nodes[0]->original_name != "Identifier") {
		throw std::invalid_argument{
			std::string{"Expected: VariableName := Identifier\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * this hash function includes the member of the class itselfs and all of his childs
 * @return a generated hash value
 */
auto variable_name::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t childHash = name->hash_impl();
	return ruleHash ^ (childHash << 1);
}
} // namespace bpy::ast
