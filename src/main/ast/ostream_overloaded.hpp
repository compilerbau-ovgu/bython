#pragma once

#include <memory>
#include <ostream>

#include "base.hpp"

namespace bpy::ast {

struct indentation {
	int taps = 0;
	indentation operator++();
	indentation operator--();
};

// forward decls
class identifier;
class ostream_overloaded;
class program;
class record_member;
class record;
class record_body;
class lambda_expression;
class callargs;
class if_expression;
class else_expression;
class block_expression;
class call_expression;
class expression;
class variable_name;
class rules;
class return_type;
class func_decl_args;
class function;
class function_name;
class constructor;
class func_decl_arg;
class none_boolean_expression;
class arithmetic_expression;
class none_arithmetic_expression;
class first_class_arithmetic_expression;
class comparison;
class first_class_boolean_expression;
class boolean_expression;
class type_name;
class boolean_constant;
class constant;
class integer;
class numeric;
class floating_point;
class bstring;
class else_statement;
class statement;
class let_statement;
class if_statement;

auto operator<<(std::ostream&, const indentation&) -> std::ostream&;

auto operator<<(std::ostream&, const base::instance&) -> std::ostream&;

auto operator<<(std::ostream&, const program&) -> std::ostream&;

auto operator<<(std::ostream&, const record&) -> std::ostream&;
auto operator<<(std::ostream&, const record_body&) -> std::ostream&;
auto operator<<(std::ostream&, const record_member&) -> std::ostream&;

auto operator<<(std::ostream&, const function&) -> std::ostream&;
auto operator<<(std::ostream&, const function_name&) -> std::ostream&;
auto operator<<(std::ostream&, const func_decl_args&) -> std::ostream&;
auto operator<<(std::ostream&, const func_decl_arg&) -> std::ostream&;
auto operator<<(std::ostream&, const return_type&) -> std::ostream&;
auto operator<<(std::ostream& os, const callargs&) -> std::ostream&;

auto operator<<(std::ostream&, const arithmetic_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const first_class_arithmetic_expression&)
	-> std::ostream&;
auto operator<<(std::ostream& os, const none_arithmetic_expression&)
	-> std::ostream&;

auto operator<<(std::ostream&, const boolean_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const first_class_boolean_expression&)
	-> std::ostream&;
auto operator<<(std::ostream&, const none_boolean_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const boolean_constant&) -> std::ostream&;

auto operator<<(std::ostream&, const block_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const expression&) -> std::ostream&;
auto operator<<(std::ostream&, const call_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const lambda_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const if_expression&) -> std::ostream&;
auto operator<<(std::ostream&, const else_expression&) -> std::ostream&;

auto operator<<(std::ostream&, const statement&) -> std::ostream&;
auto operator<<(std::ostream&, const if_statement&) -> std::ostream&;
auto operator<<(std::ostream&, const else_statement&) -> std::ostream&;
auto operator<<(std::ostream&, const identifier&) -> std::ostream&;
auto operator<<(std::ostream&, const type_name&) -> std::ostream&;
auto operator<<(std::ostream&, const variable_name&) -> std::ostream&;

auto operator<<(std::ostream&, const floating_point&) -> std::ostream&;
auto operator<<(std::ostream&, const integer&) -> std::ostream&;
auto operator<<(std::ostream&, const bstring&) -> std::ostream&;
auto operator<<(std::ostream&, const numeric&) -> std::ostream&;
auto operator<<(std::ostream&, const constant&) -> std::ostream&;

} // namespace bpy::ast
