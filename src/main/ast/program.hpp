#pragma once

#include <variant>
#include <vector>

#include "../type_system.hpp"
#include "base.hpp"
#include "function/function.hpp"
#include "record/record.hpp"

namespace bpy::ast {

class program : public base {
  public:
	using instance = std::shared_ptr<program>;
	using program_type = std::variant<function::instance, record::instance>;

	program(base::constructor_access, std::vector<program_type>);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const program&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto get(const base::instance& name) -> base::instance;

	std::vector<program_type> contents;

  private:
	explicit program(std::vector<program_type>);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
