project(bython-ast)
set(CMAKE_CXX_STANDARD 17)

set(BYTHON_AST_SOURCE_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/base.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/identifier.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/type_name.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/variable_name.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ostream_overloaded.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/rules.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/program.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/../type_system.cpp
)


add_subdirectory(constant)
add_subdirectory(record)
add_subdirectory(expression)
add_subdirectory(statement)
add_subdirectory(arithmetic_expression)
add_subdirectory(function)

add_library(bython-ast ${BYTHON_AST_SOURCE_FILES})

target_include_directories(bython-ast PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/..
    ${CMAKE_CURRENT_SOURCE_DIR}/record
    ${CMAKE_CURRENT_SOURCE_DIR}/constant
    ${CMAKE_CURRENT_SOURCE_DIR}/statement
    ${CMAKE_CURRENT_SOURCE_DIR}/expression
    ${EXTERNAL_INSTALL_LOCATION}/cpp-peglib
)

if (CLANG_TIDY)
    set_target_properties(
            bython-ast PROPERTIES
            CXX_CLANG_TIDY "${CLANG_TIDY_COMMAND}")
    message(STATUS "Enabled clang-tidy for bython-ast")
endif()
