/// \file

#include "identifier.hpp"

namespace bpy::ast {
identifier::identifier(base::constructor_access /*unused*/,
					   std::string identifier_)
	: identifier{std::move(identifier_)}
{
}

identifier::identifier(std::string identifier_)
	: ident{std::move(identifier_)}, base{RuleName::Identifier}
{
}

/**
 * returns a Bython Identifier AST representation of the given PEG AST representation.
 * An identifier ist used for variable, type or function names.
 * @param ptr the given PEG AST representation
 * @return a Bython AST Representation
 */
auto identifier::create(const std::shared_ptr<peg::Ast>& ptr)
	-> identifier::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	return std::make_shared<identifier>(base::constructor_access{}, ast.token);
}

/**
 * returns rule childs
 * @return always empty because Identifier has none
 */
auto identifier::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

/**
 * Compare two identifier with each other
 * @param rhs the identifier to compare with
 * @return true if their name are equals, false otherwise
 */
auto identifier::operator==(const identifier& rhs) const -> bool
{
	return ident == rhs.ident;
}

/**
 * checks if the given identifier matches the rules. An Identifier has to be [a-zA-Z_][a-zA-Z0-9_]*
 * @param ptr the given PEG AST representation to check
 * @throws std::invalid_argument if the identifier doesnt match the rule
 */
auto identifier::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (!ptr->is_token) {
		throw std::invalid_argument{
			std::string{
				"Expected: Identifier := < [a-zA-Z_][a-zA-Z0-9_]* >\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

/**
 * Generate a hash from the identifiers name
 * @return a generated hash value
 */
auto identifier::hash_impl() const noexcept -> std::size_t
{
	return std::hash<std::string>{}(ident);
}

} // namespace bpy::ast
