/// \file

#include "integer.hpp"

namespace bpy::ast {

integer::integer(base::constructor_access /*unnamed*/, types::BInt value_)
	: integer{value_}
{
}

integer::integer(bpy::types::BInt value_)
	: value{value_}, base{RuleName::IntegerConstant}
{
}

/**
 * creates an instance of the Bython AST IntegerConstant rule. A Integer is a representation of 0 to n numbers [0-9]+
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto integer::create(const std::shared_ptr<peg::Ast>& ptr) -> integer::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;

	return std::make_shared<integer>(base::constructor_access{},
									 types::string2int32(ast.token));
}

/**
 * checks two integers for equality. They are equal if their values are equal
 * @param other the integer to compare to
 * @return true if equals, falser otherwise
 */
auto integer::operator==(const integer& other) const -> bool
{
	return value == other.value;
}

/**
 * the dependencies of a numeric are the base dependencies
 * @return an unordered set with the dependencies
 */
auto integer::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto integer::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (bool correct = ptr->is_token && ptr->original_name == "IntegerConstant";
		!correct) {
		throw std::invalid_argument{
			std::string{"Expected: IntegerConstant <- < [0-9]+ > %ows\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto integer::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t integerHash = std::hash<types::BInt>{}(value);
	return ruleHash ^ (integerHash << 1);
}

} // namespace bpy::ast
