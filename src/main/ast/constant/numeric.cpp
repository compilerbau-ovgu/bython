/// \file

#include "numeric.hpp"

namespace bpy::ast {

numeric::numeric(base::constructor_access /*unnamed*/, numeric_type value_)
	: numeric{std::move(value_)}
{
}

numeric::numeric(numeric_type value_)
	: value{std::move(value_)}, base{RuleName::NumericConstant}
{
}

/**
 * creates an instance of the Bython AST Numeric rule. A numeric is an delegate for the FloatingPoint or Integer rule
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto numeric::create(const std::shared_ptr<peg::Ast>& ptr) -> numeric::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	const auto& type = ptr->nodes[0];

	if (ast.choice == 1) {
		return std::make_shared<numeric>(
			base::constructor_access{},
			numeric_type(integer::create(ast.nodes[0])));
	}
	return std::make_shared<numeric>(
		base::constructor_access{},
		numeric_type(floating_point::create(ast.nodes[0])));
}

/**
 * compare two given numerics for equality.
 * @return always false
 */
auto numeric::operator==(const numeric& other) const -> bool
{
	/*struct equality_visitor {
		auto operator()(const integer::instance& i)
		{
			return i;
		}
		auto operator()(const floating_point::instance& f)
		{
			return f;
		}
	};

	return value.index() == other.index() &&
		   *std::visit(equality_visitor{}, value) ==
			   *std::visit(equality_visitor{}, other.value);*/
	return false;
}

/**
 * the dependencies of a numeric are the base dependencies
 * @return an unordered set with the dependencies
 */
auto numeric::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto numeric::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (ptr->nodes.size() != 1) {
		throw std::invalid_argument{
			std::string{"Expected: NumericConstant := FloatConstant / "
						"IntegerConstant\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto numeric::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const integer::instance& i) noexcept
		{
			return i->hash_impl();
		}
		auto operator()(const floating_point::instance& f) noexcept
		{
			return f->hash_impl();
		}
	};
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t numericHash = std::visit(hash_visitor{}, value);
	return ruleHash ^ (numericHash << 1);
}

} // namespace bpy::ast
