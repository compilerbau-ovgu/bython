#pragma once

#include <variant>

#include "../type_system.hpp"
#include "base.hpp"

namespace bpy::ast {
class boolean_constant : public base {
  public:
	using instance = std::shared_ptr<boolean_constant>;

	boolean_constant(base::constructor_access, types::BBool);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> boolean_constant::instance;

	auto operator==(const boolean_constant&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	types::BBool value;

  private:
	explicit boolean_constant(types::BBool);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
