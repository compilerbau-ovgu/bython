#pragma once

#include "../../type_system.hpp"
#include "../base.hpp"

namespace bpy::ast {

class integer : public base {
  public:
	using instance = std::shared_ptr<integer>;

	// integer() = delete;
	integer(base::constructor_access, types::BInt);

	static auto create(const std::shared_ptr<peg::Ast>&) -> integer::instance;

	auto operator==(const integer&) const -> bool;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto hash_impl() const noexcept -> std::size_t override;

	types::BInt value;

  private:
	explicit integer(types::BInt);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};

} // namespace bpy::ast
