/// \file

#include "constant.hpp"

namespace bpy::ast {

constant::constant(base::constructor_access /*unused*/, constant_vt value_)
	: constant{std::move(value_)}
{
}

constant::constant(constant_vt value_)
	: value{std::move(value_)}, base{RuleName::Constant}
{
}

/**
 * creates an instance of the Bython AST Constant rule. Delegate for either a StringConstant, NumericConstant or BooleanConstant rule
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto constant::create(const std::shared_ptr<peg::Ast>& ptr)
	-> constant::instance
{
	throw_if_bad_ast(ptr);
	const auto& ast = ptr->nodes[0];

	constant_vt value{};
	if (ast->original_name == "NumericConstant") {
		value = numeric::create(ast);
	}
	else if (ast->original_name == "StringConstant") {
		value = bstring::create(ast);
	}
	else {
		value = boolean_constant::create(ast);
	}

	return std::make_shared<constant>(base::constructor_access{}, value);
}

/**
 * compare two given constants for equality.
 * @return always false
 */
auto constant::operator==(const constant& other) const -> bool
{
	/*struct equality_visitor {
		auto operator()(const numeric::instance& numeric)
		{
			return numeric;
		}
		auto operator()(const bstring::instance& string)
		{
			return string;
		}
		auto operator()(const boolean_constant::instance& bconst)
		{
			return bconst;
		}
	};

	return value.index() == other.value.index() &&
		   *std::visit(equality_visitor{}, value) ==
			   *std::visit(equality_visitor{}, other.value);*/
	return false;
}

/**
 * get the dependencies of an Constant, delegates to the child Constant dependencies
 * @return an unordered set containing the dependencies
 */
auto constant::dependencies() const -> const std::unordered_set<base::instance>&
{
	struct dependency_visitor {
		auto operator()(const numeric::instance& numeric) -> decltype(auto)
		{
			return numeric->dependencies();
		}

		auto operator()(const bstring::instance& string) -> decltype(auto)
		{
			return string->dependencies();
		}

		auto operator()(const boolean_constant::instance& boolean)
			-> decltype(auto)
		{
			return boolean->dependencies();
		}
	};

	std::call_once(dependency_cache_flag, [&]() {
		dependencies_ = std::visit(dependency_visitor{}, value);
	});
	return dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto constant::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	const auto& ast = *ptr;

	// Can only be numeric or string
	bool correct = std::size(ast.nodes) == 1;
	const auto& child = ast.nodes[0];

	// Check names of child rules (Numeric / String)
	correct = correct && (child->original_name == "NumericConstant" ||
						  child->original_name == "StringConstant" ||
						  child->original_name == "BooleanConstant");

	if (!correct) {
		throw std::invalid_argument{
			std::string{"Expected: Constant := NumericConstant / "
						"StringConstant / BooleanConstant\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

// variant can never throw an exception, it is always initialised
// NOLINTNEXTLINE(bugprone-exception-escape)
auto constant::hash_impl() const noexcept -> std::size_t
{
	struct hash_visitor {
		auto operator()(const numeric::instance& n) noexcept
		{
			return n->hash_impl();
		}
		auto operator()(const bstring::instance& b) noexcept
		{
			return b->hash_impl();
		}
		auto operator()(const boolean_constant::instance& bc) noexcept
		{
			return bc->hash_impl();
		}
	};
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t constantHash = std::visit(hash_visitor{}, value);
	return ruleHash ^ (constantHash << 1);
}

} // namespace bpy::ast
