#pragma once

#include <string>
#include <variant>

#include "../type_system.hpp"
#include "base.hpp"
#include "floating_point.hpp"
#include "integer.hpp"

namespace bpy::ast {

class numeric : public base {
  public:
	using instance = std::shared_ptr<numeric>;
	using numeric_type =
		std::variant<integer::instance, floating_point::instance>;

	numeric(base::constructor_access, numeric_type);

	static instance create(const std::shared_ptr<peg::Ast>&);

	auto operator==(const numeric&) const -> bool;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto hash_impl() const noexcept -> std::size_t override;

	numeric_type value;

  private:
	explicit numeric(numeric_type);

	static void throw_if_bad_ast(const std::shared_ptr<peg::Ast>&);
};

} // namespace bpy::ast
