/// \file

#include "string.hpp"

namespace bpy::ast {

bstring::bstring(base::constructor_access /*unused*/, std::string value_)
	: bstring{std::move(value_)}
{
}

bstring::bstring(std::string value_)
	: chars{std::move(value_)}, base{RuleName::StringConstant}
{
}

/**
 * creates an instance of the Bython AST String rule. A string is a sequence of chars which are marked with a starting and closing "
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto bstring::create(const std::shared_ptr<peg::Ast>& ptr) -> bstring::instance
{
	throw_if_bad_ast(ptr);
	const auto& ast = *ptr;

	return std::make_shared<bstring>(base::constructor_access{}, ast.token);
}

/**
 * checks the equality of two given strings. Strings are equal if their chars are equal
 * @param other the string to compare with
 * @return true if equal, false otherwise
 */
auto bstring::operator==(const bstring& other) const -> bool
{
	return chars == other.chars;
}

/**
 * empty because no dependencies
 * @return unordered set with dependencies
 */
auto bstring::dependencies() const -> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}

auto bstring::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr) -> void
{
	if (!ptr->is_token) {
		throw std::invalid_argument{
			std::string{
				R"(Expected: StringConstant := '"' (!'"' .)*  '"' / "'" (!"'" .)*  "'")"} +
			std::string{"\nGot:\n"} + peg::ast_to_s(ptr)};
	}
}

auto bstring::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t stringHash = std::hash<std::string>{}(chars);
	return ruleHash ^ (stringHash << 1);
}

} // namespace bpy::ast
