/// \file

#include "boolean_constant.hpp"

namespace bpy::ast {
boolean_constant::boolean_constant(base::constructor_access /*unused*/,
								   types::BBool value_)
	: boolean_constant{value_}
// no need to use std::move because bool is trivially-copyable
{
}

boolean_constant::boolean_constant(types::BBool value_)
	: value{value_}, base{RuleName::BooleanConstant}
{
}

/**
 * creates an instance of the Bython AST BooleanConstant rule. A BooleanConstant is either 'true' or 'false'
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto boolean_constant::create(const std::shared_ptr<peg::Ast>& ptr)
	-> boolean_constant::instance
{
	throw_if_bad_ast(ptr);
	return std::make_shared<boolean_constant>(base::constructor_access{},
											  types::string2bool(ptr->token));
}

/**
 * compare two given constants for equality.
 * @return always false
 */
auto boolean_constant::operator==(const boolean_constant& other) const -> bool
{
	// return value == other.value;
	return false;
}

/**
 * the dependencies of a numeric are the base dependencies
 * @return an unordered set with the dependencies
 */
auto boolean_constant::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });

	return base::dependencies_;
}

/**
 * Throw if AST nodes do not correspond to expected values
 * @param ptr the AST rule to check against
 * @throws std::invalid_argument if the rule not correspond
 */
auto boolean_constant::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
	const auto& ast = *ptr;

	// ast assertions
	bool correct = ast.is_token && std::size(ast.nodes) == 0 &&
				   ast.original_name == "BooleanConstant";
	// contents of the token
	correct = correct && (ast.token == "true" || ast.token == "false");

	if (!correct) {
		throw std::invalid_argument{
			std::string{
				"Expected: BooleanConstant <- ('false'/'true') %ows\n"} +
			std::string{"Got:\n"} + std::string{peg::ast_to_s(ptr)}};
	}
}

auto boolean_constant::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t boolHash = std::hash<types::BBool>{}(value);
	return ruleHash ^ (boolHash << 1);
}
} // namespace bpy::ast
