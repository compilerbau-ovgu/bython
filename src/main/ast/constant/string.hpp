#pragma once

#include <string>

#include "base.hpp"

namespace bpy::ast {
class bstring : public base {
  public:
	using instance = std::shared_ptr<bstring>;

	bstring() = delete;
	bstring(base::constructor_access, std::string);

	static auto create(const std::shared_ptr<peg::Ast>&) -> bstring::instance;

	auto operator==(const bstring&) const -> bool;

	auto hash_impl() const noexcept -> std::size_t override;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	std::string chars;

  private:
	explicit bstring(std::string);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};
} // namespace bpy::ast
