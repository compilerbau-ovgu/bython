#pragma once

#include <base.hpp>
#include <type_system.hpp>

namespace bpy::ast {

class floating_point : public base {
  public:
	using instance = std::shared_ptr<floating_point>;

	floating_point(base::constructor_access, types::BFloat);

	static auto create(const std::shared_ptr<peg::Ast>&)
		-> floating_point::instance;

	auto operator==(const floating_point&) const -> bool;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;
	auto hash_impl() const noexcept -> std::size_t override;

	types::BFloat value;

  private:
	explicit floating_point(types::BFloat);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};

} // namespace bpy::ast
