#pragma once

#include <base.hpp>
#include <variant>

#include "boolean_constant.hpp"
#include "numeric.hpp"
#include "string.hpp"

namespace bpy::ast {

class constant : public base {
  public:
	using instance = std::shared_ptr<constant>;
	using constant_vt = std::variant<bstring::instance,
									 numeric::instance,
									 boolean_constant::instance>;

	constant_vt value;

	constant(base::constructor_access, constant_vt);

	static auto create(const std::shared_ptr<peg::Ast>&) -> constant::instance;

	auto operator==(const constant&) const -> bool;

	auto dependencies() const
		-> const std::unordered_set<base::instance>& override;

	auto hash_impl() const noexcept -> std::size_t override;

  private:
	explicit constant(constant_vt);

	static auto throw_if_bad_ast(const std::shared_ptr<peg::Ast>&) -> void;
};

} // namespace bpy::ast
