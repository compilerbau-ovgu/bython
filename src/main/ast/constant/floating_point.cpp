/// \file

#include "floating_point.hpp"

#include <cmath>

namespace bpy::ast {

floating_point::floating_point(bpy::ast::base::constructor_access /*unused */,
							   bpy::types::BFloat value)
	: floating_point{value}
{
}

floating_point::floating_point(types::BFloat value)
	: value{value}, base{RuleName::FloatConstant}
{
}


auto floating_point::throw_if_bad_ast(const std::shared_ptr<peg::Ast>& ptr)
	-> void
{
    //empty
}

/**
 * the dependencies of a numeric are the base dependencies
 * @return an unordered set with the dependencies
 */
auto floating_point::dependencies() const
	-> const std::unordered_set<base::instance>&
{
	std::call_once(dependency_cache_flag, [&]() { base::dependencies_ = {}; });
	return base::dependencies_;
}


/**
 * creates an instance of the Bython AST FloatConstant rule. A Float is a representation of 0 to n numbers followed by a point and a IntegerConstant
 * [0-9]+ '.' IntegerConstant
 * @param ptr the PEG AST class to transform
 * @return an instance
 */
auto floating_point::create(const std::shared_ptr<peg::Ast>& ptr)
	-> bpy::ast::floating_point::instance
{
	throw_if_bad_ast(ptr);

	const auto& ast = *ptr;
	// an der Stelle wissen wir ja, dass unser floating_point aus nur 2 Integern
	// besteht. Also können wir die hier zusammenbasteln
	return std::make_shared<floating_point>(base::constructor_access{},
											types::string2Float(ast.token));
}

/**
 * checks two floating point values for equality. They are equal if their values are equal
 * @param other the loating point to compare to
 * @return true if equals, falser otherwise
 */
auto floating_point::operator==(const floating_point& other) const -> bool
{
	return std::abs(value - other.value) <
		   std::numeric_limits<types::BFloat>::epsilon();
}

auto floating_point::hash_impl() const noexcept -> std::size_t
{
	std::size_t ruleHash = std::hash<RuleName>{}(rule_name);
	std::size_t floatHash = std::hash<types::BFloat>{}(value);
	return ruleHash ^ (floatHash << 1);
}
} // namespace bpy::ast
