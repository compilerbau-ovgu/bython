#pragma once

#include <peglib.h>
#include <string>

namespace bpy::parsing {

auto parse_bython(const std::string&)
	-> std::shared_ptr<peg::Ast>;

}