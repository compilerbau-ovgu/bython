project(bython-main)
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(ast)
add_subdirectory(compiler)


set(BYTHON_MAIN_SOURCE_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/compiler.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/dependency_graph.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/parser.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/type_system.cpp
)
add_library(bython-main ${BYTHON_MAIN_SOURCE_FILES})
target_link_libraries(bython-main bython-ast bython-compiler)
target_include_directories(bython-main PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/
    ${CMAKE_CURRENT_SOURCE_DIR}/..
    ${CMAKE_CURRENT_SOURCE_DIR}/ast
    ${CMAKE_CURRENT_SOURCE_DIR}/compiler

    ${EXTERNAL_INSTALL_LOCATION}/cpp-peglib
)

if (CLANG_TIDY)
    set_target_properties(
            bython-main PROPERTIES
            CXX_CLANG_TIDY "${CLANG_TIDY_COMMAND}")
    message(STATUS "Enabled clang-tidy for bython-main")
endif()

set(GRAMMAR_PATH ${CMAKE_SOURCE_DIR}/resources/bython.peg)
target_compile_definitions(bython-main PRIVATE -DGRAMMAR_PATH="${GRAMMAR_PATH}")
message(STATUS "Test Grammar set to ${GRAMMAR_PATH}")

add_executable(bython main.cpp)
target_link_libraries(bython bython-main)
target_include_directories(bython PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/
    ${CMAKE_CURRENT_SOURCE_DIR}/ast
    ${CMAKE_CURRENT_SOURCE_DIR}/compiler

    ${EXTERNAL_INSTALL_LOCATION}/cpp-peglib
    ${EXTERNAL_INSTALL_LOCATION}/cxxopts/include
)
