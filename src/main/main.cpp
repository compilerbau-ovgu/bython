/// \file

/**
 * @param argc argument count
 * @param argv argument vector
 * @return exit status
 */

#include <iostream>

#include "compiler.hpp"
#include "cxxopts.hpp"
#include "parser.hpp"

using namespace bpy;

auto main(int argc, char* argv[]) -> int
{
	try {
		cxxopts::Options options(argv[0], "Bython Compiler.");
		options.positional_help("input").show_positional_help();

		options.allow_unrecognised_options().add_options()(
			"h,help", "Print this Message")("i,input", "bython file",
											cxxopts::value<std::string>());
		options.parse_positional({"input"});

		auto result = options.parse(argc, argv);

		if (result.count("help")) {
			std::cerr << options.help({"", "Group"}) << std::endl;
			exit(0);
		}

		std::shared_ptr<peg::Ast> ptr;
		if (!(ptr = parsing::parse_bython(result["input"].as<std::string>()))) {
			std::cerr << "Failed to parse Bython file" << std::endl;
			return EXIT_FAILURE;
		}

		compiler::compile(ptr);
	}

	catch (const cxxopts::OptionException& e) {
		std::cout << "error parsing options: " << e.what() << std::endl;
		exit(1);
	}
	return 0;
}
