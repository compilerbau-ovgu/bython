# Bython

## Build Instructions

```
> git clone git@gitlab.com:compilerbau-ovgu/bython.git
> cd bython
> mkdir build && cd build
> cmake ..
> make all
```

The bython compiler is built to `src/main`
The tests (`bython-gtest`) for the compiler are built in `src/test`
The documentation for the source code is built to `doc`

